<?php

	$viewing_in_english = false;

	require_once 'site-functions.php';
	require_once 'site-config.php';
	# header('Content-Type: application/json;charset=utf-8');

	try {
		$sql = new PDO('mysql:host='.$config_db_host.';dbname='.$config_db_database, $config_db_username, $config_db_password);
		$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	catch(PDOException $e) {
		echo $e;
		exit;
	}

	# create_folder('images');
	# create_folder('images/webmention-avatars');

	$get_posts = sql("SELECT *
					  FROM posts
					 ", Array());



	foreach($get_posts AS $post) {
		$postid = $post['id_unique'];
		$data = file_get_contents('https://webmention.io/api/mentions.jf2?target=https://blog.airikr.me/en/read:'.$postid);
		$data = json_decode($data);


		foreach($data->children AS $entry) {
			if($entry->{'wm-private'} == false) {
				$fileinfo = pathinfo($entry->author->photo);
				$avatar_locally = MD5($entry->author->url).'.'.$fileinfo['extension'];

				$arr_property = [
					'like-of' => 'like',
					'repost-of' => 'repost',
					'in-reply-to' => 'reply'
				];

				$arr_json = [
					'post_id' => $postid,
					'url' => $entry->url,
					'type' => $arr_property[$entry->{'wm-property'}],
					'author' => [
						'name' => $entry->author->name,
						'avatar' => $entry->author->photo,
						'avatar_locally' => $avatar_locally,
						'url' => $entry->author->url
					],
					'webmention' => [
						'received' => strtotime($entry->{'wm-received'}),
						'source' => $entry->{'wm-source'},
						'target' => $entry->{'wm-target'}
					],
					'content' => [
						'html' => (isset($entry->content->html) ? $entry->content->html : null),
						'text' => (isset($entry->content->text) ? $entry->content->text : null)
					]
				];

				if(!file_exists('/var/www/html/airikr.me/subdomains/blog/images/webmention-avatars/'.$avatar_locally)) {
					copy($entry->author->photo, '/var/www/html/airikr.me/subdomains/blog/images/webmention-avatars/'.$avatar_locally);
				}


				insert_data(
					$postid,
					$arr_json['url'],
					$arr_json['author']['name'],
					$arr_json['author']['avatar'],
					$avatar_locally,
					$arr_json['author']['url'],
					$arr_json['webmention']['received'],
					$arr_json['webmention']['source'],
					$arr_json['webmention']['target'],
					(isset($arr_json['content']['html']) ? $arr_json['content']['html'] : null),
					(isset($arr_json['content']['text']) ? $arr_json['content']['text'] : null),
					$arr_property[$entry->{'wm-property'}]
				);
			}

			$arr[] = $arr_json;
		}
	}



	if(!isset($arr)) {
		$arr = [
			'type' => 'notice',
			'message' => 'No webmentions found.'
		];
	}



	function insert_data($p, $u, $an, $aa, $aal, $au, $wmr, $wms, $wmt, $ch, $ct, $t) {
		$check_data = sql("SELECT COUNT(id_post)
						   FROM webmention
						   WHERE id_post = :_id_post
						   AND url = :_url
						  ", Array(
							  '_id_post' => $p,
							  '_url' => $u
						  ), 'count');


		if($check_data == 0) {
			sql("INSERT INTO webmention(
					 id_post,
					 url,
					 author_name,
					 author_avatar,
					 author_avatar_locally,
					 author_url,
					 webmention_received,
					 webmention_source,
					 webmention_target,
					 content_html,
					 content_text,
					 type
				 )

				 VALUES(
					 :_idpost,
					 :_url,
					 :_a_name,
					 :_a_avatar,
					 :_a_avatar_locally,
					 :_a_url,
					 :_wm_received,
					 :_wm_source,
					 :_wm_target,
					 :_c_html,
					 :_c_text,
					 :_type
				 )
				", Array(
					'_idpost' => $p,
					'_url' => $u,
					'_a_name' => $an,
					'_a_avatar' => $aa,
					'_a_avatar_locally' => $aal,
					'_a_url' => $au,
					'_wm_received' => $wmr,
					'_wm_source' => $wms,
					'_wm_target' => $wmt,
					'_c_html' => $ch,
					'_c_text' => $ct,
					'_type' => $t
				), 'insert');



		} else {
			$data = sql("SELECT *
						 FROM webmention
						 WHERE id_post = :_id_post
						 AND url = :_url
						", Array(
							'_id_post' => $p,
							'_url' => $u
						), 'fetch');


			sql("UPDATE webmention
				 SET url = :_url,
				 	 author_name = :_a_name,
					 author_avatar = :_a_avatar,
					 author_avatar_locally = :_a_avatar_locally,
					 author_url = :_a_url,
					 webmention_received = :_wm_received,
					 webmention_source = :_wm_source,
					 webmention_target = :_wm_target,
					 content_html = :_c_html,
					 content_text = :_c_text

				 WHERE id_post = :_idpost
				 AND url = :_url
				", Array(
					'_idpost' => $p,
					'_url' => $u,
					'_a_name' => ($data['author_name'] == $an ? $data['author_name'] : $an),
					'_a_avatar' => ($data['author_avatar'] == $aa ? $data['author_avatar'] : $aa),
					'_a_avatar_locally' => ($data['author_avatar_locally'] == $aal ? $data['author_avatar_locally'] : $aal),
					'_a_url' => ($data['author_url'] == $au ? $data['author_url'] : $au),
					'_wm_received' => ($data['webmention_received'] == $wmr ? $data['webmention_received'] : $wmr),
					'_wm_source' => ($data['webmention_source'] == $wms ? $data['webmention_source'] : $wms),
					'_wm_target' => ($data['webmention_target'] == $wmt ? $data['webmention_target'] : $wmt),
					'_c_html' => ($data['content_html'] == $ch ? $data['content_html'] : $ch),
					'_c_text' => ($data['content_text'] == $ct ? $data['content_text'] : $ct)
				));
		}
	}

	# echo json_encode($arr);

?>
