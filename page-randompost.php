<?php

	require_once 'site-settings.php';



	$id_unique = strip_tags(htmlspecialchars($_GET['idu']));

	$post = sql("SELECT id_unique
				 FROM posts
				 WHERE id_unique != :_idunique
				 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
				 ".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
				 AND timestamp_saved IS NULL
				 ORDER BY RAND()
				", Array(
					'_idunique' => $id_unique
				), 'fetch');


	header("Location: ".url('read:'.$post['id_unique']));
	exit;

?>
