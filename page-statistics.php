<?php

	require_once 'site-header.php';



	$statistics = sql("SELECT (
						   SELECT COUNT(id)
						   FROM posts
						   WHERE is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						   AND timestamp_saved IS NULL
					   ) AS c_posts,
					   (
						   SELECT COUNT(c.id)
						   FROM comments AS c
						   JOIN posts AS p
						   ON p.id = c.id_post
						   WHERE p.is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
					   ) AS c_comments,
					   (
						   SELECT COUNT(id)
						   FROM posts
						   WHERE YEAR(FROM_UNIXTIME(timestamp_published)) = YEAR(NOW())
						   AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						   AND timestamp_saved IS NULL
					   ) AS c_posts_thisyear,
					   (
						   SELECT COUNT(id)
						   FROM posts
						   WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = MONTH(NOW())
						   AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						   AND timestamp_saved IS NULL
					   ) AS c_posts_thismonth,
					   (
						   SELECT COUNT(id)
						   FROM posts
						   WHERE YEARWEEK(FROM_UNIXTIME(timestamp_published), 1) = YEARWEEK(CURDATE(), 1)
						   AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						   AND timestamp_saved IS NULL
					   ) AS c_posts_thisweek,
					   (
						   SELECT COUNT(cover_hash)
						   FROM posts
						   WHERE cover_hash IS NOT NULL
						   AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						   AND timestamp_saved IS NULL
					   ) AS c_posts_withacover
					  ", Array(), 'fetch');

	$monthly = sql("SELECT (
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '1'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_jan,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '2'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_feb,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '3'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_mar,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '4'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_apr,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '5'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_may,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '6'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_jun,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '7'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_jul,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '8'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_aug,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '9'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_sep,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '10'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_oct,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '11'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_nov,
					(
						SELECT COUNT(id)
						FROM posts
						WHERE MONTH(FROM_UNIXTIME(timestamp_published)) = '12'
						AND is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
						AND timestamp_saved IS NULL
					) AS c_posts_dec
				   ", Array(), 'fetch');



	$post_mostread = sql("SELECT id_unique, subject
						  FROM posts
						  WHERE is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
  						  AND timestamp_saved IS NULL
						  LIMIT 1
						 ", Array(), 'fetch');

	$post_mostcomments = sql("SELECT p.id_unique, p.subject, COUNT(c.id_post) AS c_comments
							  FROM posts AS p
							  JOIN comments AS c
							  ON p.id = c.id_post
   							  WHERE p.is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
							  GROUP BY p.id
							  ORDER BY c_comments DESC, RAND()
							  LIMIT 1
							 ", Array(), 'fetch');

	$get_posts = sql("SELECT content_beforebreak, content_afterbreak
					  FROM posts
					  WHERE is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
					  AND timestamp_saved IS NULL
					 ", Array());

	$get_tags = sql("SELECT t.name
					 FROM tags AS t
					 JOIN tags_linked AS tl
					 ON t.id = tl.id_tag
					 JOIN posts AS p
					 ON p.id = tl.id_post
					 WHERE t.is_inenglish IS ".($viewing_in_english == false ? "" : "NOT")." NULL
					 ORDER BY t.name ASC
					 LIMIT 3
					", Array());



	$characters = 0;
	foreach($get_posts AS $post) {
		$characters += strlen($post['content_beforebreak'] . $post['content_afterbreak']);
	}






	/* echo '<div class="sparks">';
		echo '<span class="bar overlapping background">{70,80,60,30,40,50,55,90,78,100,80}</span>';
		echo '<span class="line overlapping foreground">{30,50,40,20,60,80,90,100,100,100,101}</span>';
	echo '</div>'; */


	echo '<section id="statistics">';
		echo '<h1>'.($viewing_in_english == false ? 'Statistik' : 'Statistics').'</h1>';


		echo '<div class="the-data">';
			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Blogginlägg' : 'Blog posts');
				echo '</div>';

				echo '<div class="value">';
					echo $statistics['c_posts'];
				echo '</div>';
			echo '</div>';


			if($config_allowcomments == true) {
				echo '<div class="item short">';
					echo '<div class="label small-text">';
						echo ($viewing_in_english == false ? 'Kommentarer' : 'Comments');
					echo '</div>';

					echo '<div class="value">';
						echo '<a href="'.url('comments').'">';
							echo $statistics['c_comments'];
						echo '</a>';
					echo '</div>';
				echo '</div>';
			}


			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Uppladdade bilder' : 'Uploaded images');
				echo '</div>';

				echo '<div class="value">';
					$fi = new FilesystemIterator('images', FilesystemIterator::SKIP_DOTS);
					echo format_number(iterator_count($fi), 0);
				echo '</div>';
			echo '</div>';
		echo '</div>';



		echo '<h4>'.($viewing_in_english == false ? 'Blogginlägg' : 'Blog posts').'</h4>';

		echo '<div class="the-data">';
			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Det här året' : 'This year');
				echo '</div>';

				echo '<div class="value">';
					echo $statistics['c_posts_thisyear'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Den här månaden' : 'This month');
				echo '</div>';

				echo '<div class="value">';
					echo $statistics['c_posts_thismonth'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Den här veckan' : 'This week');
				echo '</div>';

				echo '<div class="value">';
					echo $statistics['c_posts_thisweek'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item wide">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Antalet tecken' : 'Number of characters');
				echo '</div>';

				echo '<div class="value">';
					if($characters == 0) {
						echo '0';
					} else {
						echo format_number($characters, 0);
						echo ' ('.format_number(($characters * 0.035277778 * 0.01), 0).' meter)';
					}
				echo '</div>';
			echo '</div>';


			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Med omslagsbilder' : 'With covers');
				echo '</div>';

				echo '<div class="value">';
					if($statistics['c_posts_withacover'] == 0) {
						echo '0';
					} else {
						echo format_number($statistics['c_posts_withacover'], 0);
						echo ' ('.format_number((($statistics['c_posts_withacover'] / $statistics['c_posts']) * 100), 1).'%)';
					}
				echo '</div>';
			echo '</div>';


			echo '<div class="item short">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Utan omslagsbilder' : 'Without covers');
				echo '</div>';

				echo '<div class="value">';
					if(($statistics['c_posts'] - $statistics['c_posts_withacover']) == 0) {
						echo '0';
					} else {
						echo format_number(($statistics['c_posts'] - $statistics['c_posts_withacover']), 0);
						echo ' ('.format_number(((($statistics['c_posts'] - $statistics['c_posts_withacover']) / $statistics['c_posts']) * 100), 1).'%)';
					}
				echo '</div>';
			echo '</div>';


			/*
			echo '<div class="item wider">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Populäraste taggarna' : 'Most popular tags');
				echo '</div>';

				echo '<div class="value">';
					$arr_tags = [];
					foreach($get_tags AS $tag) {
						$arr_tags[] = $tag['name'];
					}

					echo join(' & ', array_filter(array_merge(array(join(', ', array_slice($arr_tags, 0, -1))), array_slice($arr_tags, -1)), 'strlen'));
				echo '</div>';
			echo '</div>';
			*/


			echo '<div class="item wider">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Mest läst' : 'Most read');
				echo '</div>';

				echo '<div class="value">';
					if($statistics['c_posts'] == 0) {
						echo '-';
					} else {
						echo '<a href="'.url('read:'.$post_mostread['id_unique']).'">';
							echo $post_mostread['subject'];
						echo '</a>';
						# echo ' ('.$post_mostread['readers'].')';
					}
				echo '</div>';
			echo '</div>';


			if($config_allowcomments == true) {
				echo '<div class="item wider">';
					echo '<div class="label small-text">';
						echo ($viewing_in_english == false ? 'Mest kommenterad' : 'Most commented');
					echo '</div>';

					echo '<div class="value">';
						if(!isset($post_mostcomments['c_comments'])) {
							echo '-';
						} else {
							echo '<a href="'.url('read:'.$post_mostcomments['id_unique']).'">';
								echo $post_mostcomments['subject'];
							echo '</a>';
							echo ' ('.$post_mostcomments['c_comments'].')';
						}
					echo '</div>';
				echo '</div>';
			}


			/*
			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Januari:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_jan'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Februari:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_feb'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Mars:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_mar'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'April:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_apr'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Maj:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_may'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Juni:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_jun'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'July:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_jul'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Augusti:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_aug'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'September:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_sep'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'Oktober:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_oct'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'November:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_nov'];
				echo '</div>';
			echo '</div>';


			echo '<div class="item">';
				echo '<div class="label">';
					echo 'December:';
				echo '</div>';

				echo '<div class="value">';
					echo $monthly['c_posts_dec'];
				echo '</div>';
			echo '</div>';
			*/
		echo '</div>';



		echo '<h4>'.($viewing_in_english == false ? 'Filstorlekar' : 'File sizes').'</h4>';

		echo '<div class="the-data">';

			echo '<div class="item wide">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Uppladdade filer' : 'Uploaded files');
				echo '</div>';

				echo '<div class="value">';
					echo str_replace('.', ',', calculate_filesize(calculate_dirsize('images')));
					echo ' ('.format_number(((calculate_usedsize('images') / calculate_usedsize('.')) * 100), 1).'%)';
				echo '</div>';
			echo '</div>';


			echo '<div class="item wide">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Omslagsbilder' : 'Covers');
				echo '</div>';

				echo '<div class="value">';
					echo str_replace('.', ',', calculate_filesize(calculate_dirsize('covers')));
					echo ' ('.format_number(((calculate_usedsize('covers') / calculate_usedsize('.')) * 100), 1).'%)';
				echo '</div>';
			echo '</div>';


			echo '<div class="item wide">';
				echo '<div class="label small-text">';
					echo ($viewing_in_english == false ? 'Hela webbsidan' : 'The whole website');
				echo '</div>';

				echo '<div class="value">';
					echo str_replace('.', ',', calculate_filesize(calculate_dirsize('.')));
				echo '</div>';
			echo '</div>';
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
