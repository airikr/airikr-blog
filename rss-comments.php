<?php

	require_once 'site-settings.php';
	header('Content-Type: application/feed+json;charset=utf-8');



	$id_post = (!isset($_GET['idp']) ? null : strip_tags(htmlspecialchars($_GET['idp'])));
	$id_comment = (!isset($_GET['idc']) ? null : strip_tags(htmlspecialchars($_GET['idc'])));
	$search = (!isset($_GET['sea']) ? null : strip_tags(htmlspecialchars($_GET['sea'])));
	$name = (!isset($_GET['nam']) ? null : strip_tags(htmlspecialchars($_GET['nam'])));


	if($id_post != null) {
		$postinfo = sql("SELECT id, subject
						 FROM posts
						 WHERE id_unique = :_idpost
						", Array(
							'_idpost' => $id_post
						), 'fetch');

		$user_comment = ($viewing_in_english == false ? 'Det här flödet visar alla kommentarer för inlägget' : 'This feed shows all the comments for the blog post').' \''.$postinfo['subject'].'\'.';
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE id_post = :_idpost
							 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array(
								'_idpost' => (int)$postinfo['id']
							));


	} elseif($id_comment != null) {
		$user_comment = ($viewing_in_english == false ? 'Det här flödet visar alla svarskommentarer för en kommentar.' : 'This feed shows all answers for a comment.');
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE id_comment = :_idcomment
							 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array(
								'_idcomment' => (int)$id_comment
							));

	} elseif($search != null) {
		$user_comment = ($viewing_in_english == false ? 'Det här flödet visar alla kommentarer med sökfrasen' : 'This feed shows all the comments that have the search term').' \''.$search.'\'.';
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE visitor_url LIKE :_url
							 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 OR visitor_comment LIKE :_comment
							 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array(
								'_url' => '%'.$search.'%',
								'_comment' => '%'.$search.'%'
							));

	} elseif($name != null) {
		$user_comment = ($viewing_in_english == false ? 'Det här flödet visar alla kommentarer som skrevs av' : 'This feed shows all the comments that was written by').' \''.$name.'\'';
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE visitor_name LIKE :_name
							 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array(
								'_name' => '%'.str_replace('-', ' ', $name).'%'
							));

	} else {
		$user_comment = ($viewing_in_english == false ? 'Det här glödet visar alla kommentarer som är offentliga.' : 'This feed shows all the comments that are public.');
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array());
	}


	$arr_comments = [];
	foreach($get_comments AS $comment) {
		$post = sql("SELECT id_unique
					 FROM posts
					 WHERE id = :_idpost
					", Array(
						'_idpost' => (int)$comment['id_post']
					), 'fetch');


		$string = [
			'id' => $comment['id'],
			'url' => $og_url.'/read:'.$post['id_unique'].'#'.(int)$comment['id'],
			'external_url' => $og_url.'/read:'.$post['id_unique'],
			'title' => trim($comment['visitor_name']),
			'image' => null,
			'banner_image' => null,
			'summary' => $comment['visitor_comment'],
			'content_html' => $comment['visitor_comment'],
			'content_text' => strip_tags($comment['visitor_comment']),
			'tags' => null,
			'date_published' => date('Y-m-d\TH:i:s', $comment['timestamp_published']),
			'date_modified' => ($comment['timestamp_edited'] == null ? null : date('Y-m-d\TH:i:s', $comment['timestamp_edited']))
		];

		$arr_comments[] = $string;
	}


	$json = [
		'version' => 'https://jsonfeed.org/version/1.1',
		'title' => $og_title.' _ '.($viewing_in_english == false ? 'Kommentarer' : 'Comments'),
		'icon' => $og_image,
		'home_page_url' => $og_url,
		'feed_url' => $og_url.'/rss/comments',
		'description' => $og_description,
		'user_comment' => $user_comment,
		'expired' => false,
		'language' => ($viewing_in_english == false ? 'sv' : 'en'),
		'items' => $arr_comments
	];



	echo json_encode($json);

?>
