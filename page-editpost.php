<?php

	require_once 'site-header.php';



	$id_unique = strip_tags(htmlspecialchars($_GET['idu']));

	$postid = sql("SELECT id
				   FROM posts
				   WHERE id_unique = :_idunique
				  ", Array(
					  '_idunique' => $id_unique
				  ), 'fetch');

	$c_edits = sql("SELECT COUNT(id)
					FROM posts
					WHERE id = :_idpost
 					AND timestamp_saved IS NULL
 					AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
				   ", Array(
					   '_idpost' => (int)$postid['id']
				   ), 'count');


	if($c_edits != 0) {
		$post = sql("SELECT content_beforebreak,
							content_afterbreak

					 FROM posts
					 WHERE id = :_idpost
  					 AND timestamp_saved IS NULL
  					 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
					 ORDER BY timestamp_edited DESC
					 LIMIT 1
					", Array(
						'_idpost' => (int)$postid['id']
					), 'fetch');
	}







	echo '<section id="edit-post">';
		if($c_edits == 0) {
			echo '<div class="message color-red">';
				echo ($viewing_in_english == false ? 'Kunde inte hitta inlägget' : 'Couldn\'t find the blog post');
			echo '</div>';


		} else {
			echo '<h1>'.($viewing_in_english == false ? 'Rätta ett inlägg' : 'Make a correction to a post').'</h1>';

			echo '<div class="content">';
				if($viewing_in_english == false) {
					echo '<p>Den här sidan finns för att du ska kunna rätta något stavfel eller föreslå en säker källa för något som jag har yttrat mig om. Du kan även kartlägga platser till '.link_('OpenStreetMap', 'https://openstreetmap.org').', till exempel ändra "Skoghall" till "'.link_('Skoghall', 'https://www.openstreetmap.org/#map=15/59.3241/13.4702').'".</p>';
					echo '<p>När du har gjort dina ändringar, tryck på knappen och jag kommer att granska dina ändringar så fort jag kan. Om jag tycker att dina ändringar är korrekta, så publicerar jag dom.</p>';

				} else {
					echo '<p>This page exists for you to let you fix spelling errors or propose a valid source or something like that.</p>';
					echo '<p>When you have made your changes, press the Send button and I will review your changes whenever I can. If I see your changes as valid, I will publish them.</p>';
				}
			echo '</div>';


			echo '<div id="message"></div>';
			echo '<form action="javascript:void(0)" method="POST">';
				echo '<input type="hidden" name="hidden-idpost" value="'.(int)$postid['id'].'">';

				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? '' : '');
					echo '</div>';

					echo '<div class="field">';
						echo '<textarea name="field-content" tabindex="1">';
							echo $post['content_beforebreak'];
							echo ($post['content_afterbreak'] == null ? null : "\r\n----\r\n".str_replace('<br />', '', $post['content_afterbreak']));
						echo '</textarea>';
					echo '</div>';
				echo '</div>';

				echo '<div class="button">';
					echo '<input type="submit" name="button-send" value="'.($viewing_in_english == false ? 'Skicka' : 'Send').'" tabindex="2">';

					echo '<div class="links small-text">';
						echo '<a href="'.url('read:'.$id_unique).'" id="goback">';
							echo ($viewing_in_english == false ? 'Visa inlägget' : 'View the post');
						echo '</a>';
					echo '</div>';
				echo '</div>';
			echo '</form>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
