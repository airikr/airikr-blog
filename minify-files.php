<?php

	require_once 'site-settings.php';
	require_once 'vendor/autoload.php';



	$minify_css = new \Bissolli\PhpMinifier\Minifier;
	$minify_css->addCssFile('css/desktop.css');
	$minify_css->minifyCss()->outputCss('css/desktop.min.css');

	$minify_themedark = new \Bissolli\PhpMinifier\Minifier;
	$minify_themedark->addCssFile('css/theme-dark.css');
	$minify_themedark->minifyCss()->outputCss('css/theme-dark.min.css');

	$minify_themelight = new \Bissolli\PhpMinifier\Minifier;
	$minify_themelight->addCssFile('css/theme-light.css');
	$minify_themelight->minifyCss()->outputCss('css/theme-light.min.css');

	$minify_themelight = new \Bissolli\PhpMinifier\Minifier;
	$minify_themelight->addCssFile('css/noscript.css');
	$minify_themelight->minifyCss()->outputCss('css/noscript.min.css');

	$minify_themelight = new \Bissolli\PhpMinifier\Minifier;
	$minify_themelight->addJsFile('js/main.js');
	$minify_themelight->minifyJs()->outputJs('js/main.min.js');

	die('The files has been minified.');

?>
