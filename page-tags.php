<?php

	require_once 'site-header.php';



	$get_tags = sql("SELECT *
					 FROM tags
					 WHERE is_inenglish IS ".($viewing_in_english == false ? '' : "NOT")." NULL
					 ORDER BY name ASC
					", Array());

	/* $arr_words = [
		'horny'
	];

	foreach($arr_words AS $word) {
		sql("INSERT INTO blacklist(data_word) VALUES(:_word)", Array('_word' => $word), 'insert');
	} */







	echo '<section id="tags">';
		echo '<h1>'.($viewing_in_english == false ? 'Taggar' : 'Tags').'</h1>';


		foreach($get_tags AS $tag) {
			$c_posts = sql("SELECT COUNT(id_tag)
							FROM tags_linked
							WHERE id_tag = :_idtag
						   ", Array(
							   '_idtag' => (int)$tag['id']
						   ), 'count');


			echo '<div class="tag">';
				echo '<div class="rss">';
					echo link_(svgicon('linkicon-rss'), url('rss/tag:'.str_replace(' ', '-', $tag['name'])));
				echo '</div>';

				echo '<div class="name">';
					echo '<a href="'.url('tag:'.str_replace(' ', '-', $tag['name'])).'">';
						echo $tag['name'];
					echo '</a>';
				echo '</div>';

				echo '<div class="amount">';
					echo format_number($c_posts);
				echo '</div>';
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
