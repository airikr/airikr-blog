<?php

	if(isset($_GET['ver'])) {
		require_once 'site-settings.php';

		$field_secret = strip_tags(htmlspecialchars($_POST['field-secret']));
		$field_totp = strip_tags(htmlspecialchars($_POST['field-totp']));
		$field_username = strip_tags(htmlspecialchars($_POST['field-username']));
		$field_email = strip_tags(htmlspecialchars($_POST['field-email']));

		$ga = new PHPGangsta_GoogleAuthenticator();
		$result = $ga->verifyCode($field_secret, $field_totp, 2);

		$ncrypt = new mukto90\Ncrypt;
		$ncrypt->set_secret_key($password[0]);
		$ncrypt->set_secret_iv($password[0]);
		$ncrypt->set_cipher('AES-256-CBC');


		if($result) {
			$totp = [
				'secret' => $field_secret
			];

			sql("INSERT INTO users(
					 twofactorcode,
					 username,
					 email,
					 email_password
				 )

				 VALUES(
					 :_twofactorcode,
					 :_username,
					 :_email,
					 :_email_password
				 )
				", Array(
					'_twofactorcode' => $field_secret,
					'_username' => trim($field_username),
					'_email' => trim($ncrypt->encrypt($field_email)),
					'_email_password' => password_hash($password[0], PASSWORD_BCRYPT)
				), 'insert');

			session_unset();
			session_destroy();
		}

		header("Location: ".url('login'));
		exit;







	} else {

		require_once 'site-header.php';



		$ga = new PHPGangsta_GoogleAuthenticator();
		$secret = $ga->createSecret();

		$barcode = new \Com\Tecnick\Barcode\Barcode();
		$barcodeObj = $barcode->getBarcodeObj('QRCODE,H', 'otpauth://totp/Airikr Blog?secret='.$secret, -5, -5, 'black', array(-1, -1, -1, -1))->setBackgroundColor('#ffffff');
		$imageData = $barcodeObj->getPngData();
		file_put_contents('images/qrcode.png', $imageData);








		echo '<section id="config">';
			echo '<h2>'.($viewing_in_english == false ? 'Skapa en ny användare' : 'Create a new user').'</h2>';

			echo '<form action="'.url('verify-totp').'" method="POST" autocomplete="off">';

				echo '<div class="code">';
					echo '<div class="secret">';
						echo '<input type="text" name="field-secret" value="'.$secret.'" readonly>';
					echo '</div>';

					echo '<div class="qrcode">';
						echo '<img src="'.url('images/qrcode.png', true).'">';
					echo '</div>';
				echo '</div>';


				echo '<div class="fields">';
					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Vad är engångskoden?' : 'What is the one-time code?');
						echo '</div>';

						echo '<div class="field">';
							echo '<input type="text" name="field-totp" maxlength="6" tabindex="1" required>';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Användarnamn' : 'Username');
						echo '</div>';

						echo '<div class="field">';
							echo '<input type="text" name="field-username" tabindex="2" required>';
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'E-postadress' : 'Email address');
						echo '</div>';

						echo '<div class="field">';
							echo '<input type="email" name="field-email" tabindex="3" required>';
						echo '</div>';
					echo '</div>';

					echo '<div class="button">';
						echo '<input type="submit" name="button-verify" tabindex="4" value="'.($viewing_in_english == false ? 'Verifiera' : 'Verify').'">';
					echo '</div>';
				echo '</div>';

			echo '</form>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
