<?php

	require_once 'site-settings.php';
	header('Content-Type: application/feed+json;charset=utf-8');



	$tagname = strip_tags(htmlspecialchars($_GET['tag']));
	$get_posts = sql("SELECT *, p.id AS id_post
					  FROM posts p
					  INNER JOIN tags_linked tl
					  ON tl.id_post = p.id
					  INNER JOIN tags t
					  ON tl.id_tag = t.id
					  WHERE t.name = :_name
					  AND p.timestamp_saved IS NULL
					  AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
					  ORDER BY p.timestamp_published DESC
					 ", Array(
						 '_name' => str_replace('-', ' ', $tagname)
					 ));


	$arr_posts = [];
	foreach($get_posts AS $post) {
		$string = [
			'id' => $post['id'],
			'url' => $og_url.'/read:'.$post['id_unique'],
			'title' => $post['subject'],
			'image' => (empty($post['cover_hash']) ? null : $og_url.'/cover:'.$post['cover_hash']),
			'banner_image' => (empty($post['cover_hash']) ? null : $og_url.'/cover:'.$post['cover_hash']),
			'summary' => $Parsedown->text($post['content_beforebreak']),
			'content_html' => $Parsedown->text($post['content_beforebreak']) . ($post['content_afterbreak'] == null ? '' : $Parsedown->text($post['content_afterbreak'])),
			'content_text' => strip_tags($post['content_beforebreak'] . ($post['content_afterbreak'] == null ? '' : $post['content_afterbreak'])),
			'tags' => (empty($arr_tags) ? null : $arr_tags),
			'date_published' => date('Y-m-d\TH:i:s', $post['timestamp_published']),
			'date_modified' => date('Y-m-d\TH:i:s', $post['timestamp_edited'])
		];

		$arr_posts[] = $string;
	}


	$json = [
		'version' => 'https://jsonfeed.org/version/1.1',
		'title' => $og_title,
		'icon' => $og_image,
		'home_page_url' => $og_url,
		'feed_url' => $og_url.'/feed:json',
		'description' => $og_description,
		'user_comment' => ($viewing_in_english == false ? 'Det här flödet visar alla blogginlägg som Erik har publicerat.' : 'This feed lists all blog posts that Erik has published.'),
		'expired' => false,
		'language' => ($viewing_in_english == false ? 'sv' : 'en'),
		'items' => $arr_posts
	];



	echo json_encode($json);

?>
