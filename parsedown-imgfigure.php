<?php

class FigureExtParsedown extends Parsedown {

    // Matches Markdown image definition
    private $MarkdownImageRegex = "~^!\[.*?\]\(.*?\)$~";

    public function __construct () {
        // Add blockFigure to non-exclusive handlers for text starting with !
        $this->BlockTypes['!'][] = 'Figure';
    }

    protected function blockFigure($Line) {
        // If line does not match image def, don't handle it
        if (1 !== preg_match($this->MarkdownImageRegex, $Line['text'])) {
            return;
        }

        $InlineImage = $this->inlineImage($Line);
        if (!isset($InlineImage)) {
            return;
        }

        $FigureBlock = array(
            'element' => array(
                'name' => 'figure',
                'handler' => 'elements',
                'text' => array(
                    $InlineImage['element']
                )
            ),
        );

        // Add figcaption if title set
        if (!empty($InlineImage['element']['attributes']['title'])) {

            $InlineFigcaption = array(
                'element' => array(
                    'name' => 'figcaption',
                    'text' => $InlineImage['element']['attributes']['title']
                ),
            );

            $FigureBlock['element']['text'][] = $InlineFigcaption['element'];
        }

        return $FigureBlock;
    }
}
