<?php

	require_once 'site-settings.php';
	header('Content-Type: application/feed+json;charset=utf-8');



	$id_comment = strip_tags(htmlspecialchars($_GET['idc']));
	$get_comments = sql("SELECT *
						 FROM comments
						 WHERE id_comment = :_idcomment
						 ORDER BY timestamp_published DESC
						", Array(
							'_idcomment' => (int)$id_comment
						));


	$arr_comments = [];
	foreach($get_comments AS $comment) {
		$post = sql("SELECT id_unique
					 FROM posts
					 WHERE id = :_idpost
					", Array(
						'_idpost' => (int)$comment['id_post']
					), 'fetch');


		$string = [
			'id' => $comment['id'],
			'url' => $og_url.'/read:'.$post['id_unique'].'#'.(int)$comment['id'],
			'external_url' => $og_url.'/read:'.$post['id_unique'],
			'title' => $comment['visitor_name'],
			'image' => null,
			'banner_image' => null,
			'summary' => $comment['visitor_comment'],
			'content_html' => $comment['visitor_comment'],
			'content_text' => strip_tags($comment['visitor_comment']),
			'tags' => null,
			'date_published' => date('Y-m-d\TH:i:s', $comment['timestamp_published']),
			'date_modified' => ($comment['timestamp_edited'] == null ? null : date('Y-m-d\TH:i:s', $comment['timestamp_edited']))
		];

		$arr_comments[] = $string;
	}


	$json = [
		'version' => 'https://jsonfeed.org/version/1.1',
		'title' => $og_title,
		'icon' => $og_image,
		'home_page_url' => $og_url,
		'feed_url' => $og_url.'/comment-feed:json/id:'.$id_comment,
		'description' => $og_description,
		'user_comment' => ($viewing_in_english == false ? 'Det här flödet visar alla svarskommentarer för en huvudkommentar.' : 'This feed lists all comments that have answered a comment.'),
		'expired' => false,
		'language' => ($viewing_in_english == false ? 'sv' : 'en'),
		'items' => $arr_comments
	];



	echo json_encode($json);

?>
