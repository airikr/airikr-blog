<?php

	require_once 'site-settings.php';

	sql("UPDATE users
		 SET timestamp_lastactive = null
		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$session_id
		));

	session_unset();
	session_destroy();

	header("Location: ".url(''));
	exit;

?>
