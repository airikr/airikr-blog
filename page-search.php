<?php

	if(isset($_GET['go'])) {
		require_once 'site-settings.php';

		$form_string = strip_tags(htmlspecialchars($_POST['field-string']));

		header("Location: ".url('search:'.str_replace(' ', '-', strtolower($form_string))));
		exit;







	} else {

		require_once 'site-header.php';







		echo '<section id="search">';
			echo '<h1>'.($viewing_in_english == false ? 'Sök' : 'Search').'</h1>';

			if($viewing_in_english == false) {
				echo '<p>För närvarande kan du endast söka efter enkla ord, så som "facebook", "integritet", "affär", eller liknande som finns i blogginläggen. Den här funktionen kommer att bli rikligt förbättrad så småningom.</p>';
			} else {
				echo '<p>Currently, you can only search for simple words like "facebook", "privacy", "store", or something similar within the content of the blog posts. This feature will be greatly improved in due time.</p>';
			}


			echo '<div id="message"></div>';
			echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate>';
				echo '<input type="search" name="field-string" tabindex="1">';
				echo '<input type="submit" name="button-search" value="'.($viewing_in_english == false ? 'Sök' : 'Search').'" tabindex="2">';
			echo '</form>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
