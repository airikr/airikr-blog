-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 14, 2023 at 06:42 PM
-- Server version: 10.11.4-MariaDB
-- PHP Version: 8.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airikr-blog`
--
CREATE DATABASE IF NOT EXISTS `airikr-blog` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `airikr-blog`;

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL,
  `data_word` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_answer` int(11) DEFAULT NULL,
  `visitor_name` varchar(20) NOT NULL,
  `visitor_url` tinytext DEFAULT NULL,
  `visitor_comment` text NOT NULL,
  `visitor_ipaddress` text DEFAULT NULL,
  `comment_password` text NOT NULL,
  `comment_identity` text DEFAULT NULL,
  `is_inenglish` tinyint(4) DEFAULT NULL,
  `timestamp_published` varchar(20) NOT NULL,
  `timestamp_edited` varchar(20) DEFAULT NULL,
  `timestamp_accepted` tinyint(4) DEFAULT NULL,
  `timestamp_denied` tinyint(4) DEFAULT NULL,
  `is_public` tinyint(4) DEFAULT NULL,
  `is_denied` tinyint(4) DEFAULT NULL,
  `is_spam` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `file_name_md5` varchar(32) NOT NULL,
  `file_extension` varchar(4) NOT NULL,
  `field_alt` varchar(200) NOT NULL,
  `timestamp_uploaded` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `id_unique` varchar(13) NOT NULL,
  `id_post` int(11) DEFAULT NULL,
  `subject` varchar(80) NOT NULL,
  `cover_hash` varchar(32) DEFAULT NULL,
  `cover_url` text DEFAULT NULL,
  `cover_owner` text DEFAULT NULL,
  `cover_owner_url` text DEFAULT NULL,
  `content_beforebreak` text NOT NULL,
  `content_meta` varchar(180) DEFAULT NULL,
  `content_afterbreak` text DEFAULT NULL,
  `is_inenglish` varchar(2) DEFAULT NULL,
  `hasbeencorrected` tinyint(4) DEFAULT NULL,
  `readers` smallint(6) DEFAULT NULL,
  `timestamp_saved` tinytext DEFAULT NULL,
  `timestamp_published` tinytext DEFAULT NULL,
  `timestamp_edited` tinytext DEFAULT NULL,
  `is_pinned` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `public_edits`
--

DROP TABLE IF EXISTS `public_edits`;
CREATE TABLE `public_edits` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `content` text NOT NULL,
  `timestamp_sent` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `is_inenglish` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags_linked`
--

DROP TABLE IF EXISTS `tags_linked`;
CREATE TABLE `tags_linked` (
  `id` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` tinytext NOT NULL,
  `password` text DEFAULT NULL,
  `info_name` varchar(50) DEFAULT NULL,
  `info_url` text DEFAULT NULL,
  `info_email` text DEFAULT NULL,
  `twofactorcode` tinytext DEFAULT NULL,
  `is_admin` tinyint(4) DEFAULT NULL,
  `is_guest` tinyint(4) DEFAULT NULL,
  `timestamp_lastlogin` varchar(20) DEFAULT NULL,
  `timestamp_lastactive` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `data_ipaddress` text NOT NULL,
  `data_useragent` text NOT NULL,
  `data_page` tinytext NOT NULL,
  `data_page_get` text DEFAULT NULL,
  `timestamp_allowed` varchar(20) DEFAULT NULL,
  `timestamp_visited` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webmention`
--

DROP TABLE IF EXISTS `webmention`;
CREATE TABLE `webmention` (
  `id` int(11) NOT NULL,
  `id_post` varchar(20) NOT NULL,
  `url` text NOT NULL,
  `author_name` text NOT NULL,
  `author_avatar` text NOT NULL,
  `author_avatar_locally` text NOT NULL,
  `author_url` text NOT NULL,
  `webmention_received` varchar(15) NOT NULL,
  `webmention_source` text NOT NULL,
  `webmention_target` text NOT NULL,
  `content_html` text DEFAULT NULL,
  `content_text` text DEFAULT NULL,
  `type` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blacklist`
--
ALTER TABLE `blacklist`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `public_edits`
--
ALTER TABLE `public_edits`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tags_linked`
--
ALTER TABLE `tags_linked`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `webmention`
--
ALTER TABLE `webmention`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `public_edits`
--
ALTER TABLE `public_edits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags_linked`
--
ALTER TABLE `tags_linked`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `webmention`
--
ALTER TABLE `webmention`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
