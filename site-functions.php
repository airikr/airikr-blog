<?php

	use ChrisUllyott\FileSize;
	use WideImage\WideImage;
	use Astrotomic\Twemoji\Twemoji;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;



	function url($string, $pure = false, $change_theme = false) {
		global $dir, $theme, $viewing_in_english, $host;

		$dir_ = '/'.((strpos($host, '192.168') !== false OR $host == 'localhost' OR $host == '8888') ? 'blog/' : '');
		$language_ = ($viewing_in_english == true ? 'en' : null);
		$pos = strrpos($_SERVER['REQUEST_URI'], '/');
		$after_last_slash = ($pos === false ? null : substr($_SERVER['REQUEST_URI'], $pos + 1));
		$check_string_sv = (((empty($string) OR $string == 'light' OR ($string == 'light' AND $theme == 'light'))) ? '' : '/');
		$check_string_en = (($language_ == 'en' AND $string == 'light' OR $theme == 'light' OR $string == '') ? '' : '/');


		if($pure == false) {
			$string_ = null;

			if($change_theme == true) {
				if($language_ == 'en') {
					$string_ .= 'en';
					$string_ .= ($theme == 'light' ? $check_string_en : ',light'.$check_string_en);
					$string_ .= (($after_last_slash == 'en' OR $after_last_slash == 'en,light') ? '' : '/'.$after_last_slash);

				} else {
					$string_ .= ($theme == 'light' ? '' : 'light'.$check_string_en);
					$string_ .= ($after_last_slash == 'light' ? '' : $after_last_slash);
				}

				return $dir_ . $string_;


			} else {
				if($language_ == 'en') {
					$string_ .= 'en';
					$string_ .= ($theme == 'light' ? ',light'.$check_string_sv : $check_string_sv);
					$string_ .= $string;

				} else {
					$string_ .= ($theme == 'light' ? 'light'.$check_string_sv : '');
					$string_ .= $string;
				}

				return $dir_ . $string_;
			}

		} else {
			return $dir_ . $string;
		}
	}



	function email_content($content) {
		$structure = '<html>';
		$structure .= '<head>';
		$structure .= '<style type="text/css">';
			$structure .= 'html, body { background-color: #0f0f0f; color: #c3c4c5; font-family: "Arial", sans-serif; font-size: 14px; margin: 0; padding: 15px 20px; }';
			$structure .= 'h4 { font-size: 22px; margin: 0 0 10px 0; }';
			$structure .= 'p { line-height: 23px; margin: 20px 0; max-width: 700px; }';
		$structure .= '</style>';
		$structure .= '</head>';
		$structure .= '<body>';

			$structure .= $content;

		$structure .= '</body>';
		$structure .= '</html>';

		return $structure;
	}


	function send_email($to, $subject, $content_html, $content_nohtml) {
		global $config_smtp_server, $config_smtp_port, $config_smtp_username, $config_smtp_password, $config_mailer_email, $config_mailer_name;

		$mail = new PHPMailer(true);

		try {
			$mail->SMTPDebug = 0;
			$mail->isSMTP();
			$mail->Host = $config_smtp_server;
			$mail->SMTPAuth = true;
			$mail->Username = $config_smtp_username;
			$mail->Password = $config_smtp_password;
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
			$mail->Port = $config_smtp_port;

			$mail->setFrom($config_mailer_email, $config_mailer_name);
			$mail->addAddress($to);
			$mail->addCC($to);
			$mail->addBCC($to);

			$mail->isHTML(false);
			$mail->Subject = utf8_decode($subject);
			$mail->Body = email_content(utf8_decode($content_html));
			$mail->AltBody = utf8_decode($content_nohtml);

			$mail->send();
			return 'mail-sent';

		} catch (Exception $e) {
			return 'mail-not-sent';
		}
	}



	function get_toot($id) {
		global $config_mastodoninstance;
		global $viewing_in_english;

		$url = @file_get_contents('https://'.$config_mastodoninstance.'/api/v1/statuses/'.$id[1]);
		$json = json_decode($url);

		$content = '<div class="toot">';
			$content .= '<div class="mastodon-logo">'.svgicon('mastodon').'</div>';

			if($json) {
				$content .= '<div class="avatar" style="background-image: url('.$json->account->avatar.');"></div>';
				$content .= '<div class="content">';
					$content .= '<div class="info">';
						$content .= '<div>';
							$content .= '<div class="displayname">'.$json->account->display_name.'</div>';
							$content .= '<div class="username">@'.$json->account->username.'</div>';
						$content .= '</div>';

						$content .= '<div class="created">';
							$content .= '<a href="'.$json->url.'" target="_blank" rel="nofollow">';
								$content .= date('Y-m-d, H:i', strtotime($json->created_at));
							$content .= '</a>';
						$content .= '</div>';
					$content .= '</div>';


					$content .= '<div class="text">';
						$content .= $json->content;
					$content .= '</div>';

					if($json->media_attachments != null) {
						$content .= '<div class="media">';
							foreach($json->media_attachments AS $media) {
								$content .= '<div class="item">'.link_('<figure><img src="'.$media->preview_url.'"></figure>', $media->url).'</div>';
							}
						$content .= '</div>';
					}

					$content .= '<div class="statistics no-select small-text">';
						$content .= '<div>';
							$content .= svgicon('toot-replies');
							$content .= $json->replies_count;
						$content .= '</div>';

						$content .= '<div>';
							$content .= svgicon('toot-favourites');
							$content .= $json->favourites_count;
						$content .= '</div>';
					$content .= '</div>';
				$content .= '</div>';


			} else {
				$content .= '<div class="error">';
					$content .= ($viewing_in_english == false ? 'Kunde inte hämta toot\'en.' : 'Was not able to fetch the toot.');
				$content .= '</div>';
			}
		$content .= '</div>';

		return $content;
	}

	function checkaftertoots($string) {
		return preg_replace_callback("/\[toot=([0-9]+)\]/", "get_toot", $string);
	}



	function manage_image($file, $height, $width, $size) {
		$img = new Imagick($file);
		$img->setCompression(Imagick::COMPRESSION_JPEG);
		$img->setCompressionQuality(20);
		$img->stripImage();

		if($height > $size OR $width > $size) {
			$img->resizeImage(
				min($img->getImageWidth(),  $size),
				min($img->getImageHeight(), $size),
				imagick::FILTER_CATROM,
				1,
				true
			);
		}

		switch ($img->getImageOrientation()) {
			case Imagick::ORIENTATION_TOPLEFT:
				break;
			case Imagick::ORIENTATION_TOPRIGHT:
				$img->flopImage();
				break;
			case Imagick::ORIENTATION_BOTTOMRIGHT:
				$img->rotateImage("#000", 180);
				break;
			case Imagick::ORIENTATION_BOTTOMLEFT:
				$img->flopImage();
				$img->rotateImage("#000", 180);
				break;
			case Imagick::ORIENTATION_LEFTTOP:
				$img->flopImage();
				$img->rotateImage("#000", -90);
				break;
			case Imagick::ORIENTATION_RIGHTTOP:
				$img->rotateImage("#000", 90);
				break;
			case Imagick::ORIENTATION_RIGHTBOTTOM:
				$img->flopImage();
				$img->rotateImage("#000", 90);
				break;
			case Imagick::ORIENTATION_LEFTBOTTOM:
				$img->rotateImage("#000", -90);
				break;
			default:
				break;
		}

		$img->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
		$img->setInterlaceScheme(3);
		$img->writeImage($file);
		$img->clear();
		$img->destroy();
	}



	function page_humanreadable($page, $page_get = null) {
		global $viewing_in_english;

		$rss_comments = null;
		$post_subject = null;
		$page_number = null;
		$page_privacy = null;
		$language = (($viewing_in_english == true OR strpos($page_get, 'lau=') === false) ? null : 'en');
		$rssfeed_name = (strpos($page_get, 'nam=') !== false ? true : false);

		if($page_get != null) {
			if(strpos($page_get, 'idu') !== false) {
				preg_match('([a-z0-9]{13})', $page_get, $matches);
				$post = sql("SELECT id_unique, subject
							 FROM posts
							 WHERE id_unique = :_idunique
							", Array(
								'_idunique' => $matches[0]
							), 'fetch');

				$post_subject = '"<a href="'.url('read:'.$post['id_unique']).'">'.$post['subject'].'</a>"';


			} elseif(strpos($page_get, 'idp') !== false) {
				$post = sql("SELECT id_unique, subject
							 FROM posts
							 WHERE id = :_idpost
							", Array(
								'_idpost' => str_replace('idp=', '', $page_get)
							), 'fetch');

				$post_subject = '"<a href="'.url('read:'.$post['id_unique']).'">'.$post['subject'].'</a>"';


			} elseif(strpos($page_get, 'pgn') !== false) {
				$page_number = str_replace('pgn=', '', $page_get);


			} elseif(strpos($page_get, 'pag') !== false) {
				if($page_get != 'pag=') {
					preg_match('(collects|uses|duration|access|rights|stored)', $page_get, $matches);
					$page_privacy = $matches[0];
				}

				$arr_privacy = [
					'collects' => ($viewing_in_english == false ? 'Insamling' : 'Collects'),
					'uses' => ($viewing_in_english == false ? 'Behandling' : 'Uses'),
					'duration' => ($viewing_in_english == false ? 'Varaktighet' : 'Duration'),
					'access' => ($viewing_in_english == false ? 'Åtkomst' : 'Access'),
					'rights' => ($viewing_in_english == false ? 'Rättigheter' : 'Rights'),
					'stored' => ($viewing_in_english == false ? 'Lagring' : 'Stored')
				];
			}
		}


		if($page == 'rss-comments.php') {
			$rss_comments = ($viewing_in_english == false ? 'Visade RSS-flödet' : 'Listed the RSS feed');

			if($rssfeed_name == true) {
				$rss_comments .= ' '.($viewing_in_english == false ? 'från en person' : 'from a person');
			} elseif(strpos($page_get, 'idc') !== false) {
				$rss_comments .= ' '.($viewing_in_english == false ? 'för en kommentar' : 'for a comment');
			} elseif(strpos($page_get, 'idu') !== false) {
				$rss_comments .= ' '.($viewing_in_english == false ? 'för '.$post_subject : 'for a blog post');
			} else {
				$rss_comments .= ' '.($viewing_in_english == false ? 'för alla kommentarer' : 'for all comments');
			}
		}



		$arr = [
			'index.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Hem'.($page_number == null ? '' : ' (sida '.$page_number.')') : 'Home (page '.$page_number.')'),
			'page-yourdata.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Din data' : 'Your data'),
			'page-tags.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Taggar' : 'Tags'),
			'page-privacy.php' => 'Integritetspolicy'.($page_privacy == null ? '' : ': '.$arr_privacy[$page_privacy]),
			'page-privacy-en.php' => ($viewing_in_english == false ? '<div class="mark language no-select">EN</div>' : '').'Privacy policy'.($page_privacy == null ? '' : ': '.$arr_privacy[$page_privacy]),
			'page-license.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Licens' : 'License'),
			'admin-login.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Logga in' : 'Login'),
			'page-comments.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Kommentarer' : 'Comments'),
			'page-search.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Sök' : 'Search'),
			'page-statistics.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Statistik' : 'Statistics'),
			'page-read.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Läste '.($post_subject == null ? '' : $post_subject) : 'Read a blog post'),
			'page-randompost.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>') . ($viewing_in_english == false ? 'Gick till ett slumpmässigt inlägg' : 'Went to a random blog post'),
			'rss-comments.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark rss no-select">RSS</div>'.$rss_comments,
			'rss.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark rss no-select">RSS</div>'.($viewing_in_english == false ? 'Visade RSS-flödet för alla inlägg' : 'Showed the RSS feed for all the blog posts'),
			'error.php' => '404',

			'analytics.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">GET</div>'.($viewing_in_english == false ? 'Du godkände datainsamlingen' : 'You allowed the website to collect data'),
			'list-posts.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">GET</div>'.($viewing_in_english == false ? 'Hämtade alla inlägg för sida '.$page_number : 'Listed all posts for the current page'),
			'list-comments.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">GET</div>'.($viewing_in_english == false ? 'Listade kommentarerna för '.$post_subject : 'Listed all comments for a blog post'),
			'manage-comment.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">POST</div>'.($viewing_in_english == false ? 'Hanterade en kommentar' : 'Managed a comment'),
			'comment-save.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">GET</div>'.($viewing_in_english == false ? 'Sparade en kommentar för '.$post_subject : 'Saved a comment'),
			'comment-send.php' => ($language == null ? '' : '<div class="mark language no-select">EN</div>').'<div class="mark type no-select">GET</div>'.($viewing_in_english == false ? 'Skickade en kommentar för '.$post_subject : 'Sent a comment')
		];

		return $arr[$page];
	}



	function link_($string, $link, $class = null) {
		return '<a href="'.$link.'" target="_blank"'.($class == null ? '' : ' class="'.$class.'"').' rel="noopener nofollow">'.$string.'</a>';
	}

	function shorten_text($string, $length = 150) {
		return mb_substr($string, 0, $length, 'utf8') . (strlen($string) > $length ? '...' : '');
	}

	function age($year, $month, $day, $detailed = false) {
		$diff = date_diff(date_create($year.'-'.$month.'-'.$day), date_create('today'));
		return $diff->y;
	}

	function stripos_array($haystack, $needles){
		foreach($needles as $needle) {
			if(($res = stripos($haystack, $needle)) !== false) {
				return $res;
			}
		}

		return false;
	}

	function format_number($string, $decimals = '1') {
		global $viewing_in_english;

		$seperator = ($viewing_in_english == false ? ',' : '.');
		$string = number_format($string, $decimals, ',', ' ');
		$explode = explode(',', $string);

		return $explode[0] . (!isset($explode[1]) ? null : ($explode[1] == 0 ? null : $seperator . $explode[1]));
	}

	function endecrypt($string, $encrypt = true) {
		global $ncrypt;
		return ($encrypt == true ? $ncrypt->encrypt($string) : $ncrypt->decrypt($string));
	}

	function highlight_word($title, $searched_word) {
		return preg_replace('#('.$searched_word.')#i', '<mark>\1</mark>', $title);
	}

	function do_supports_webp() {
		return strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false ? true : false;
	}

	function unlink_($string) {
		if(file_exists($string)) {
			unlink($string);
		}
	}

	function isvalid_md5($md5 = '') {
		return preg_match('/^[a-f0-9]{32}$/', $md5);
	}

	function create_folder($string) {
		if(!file_exists($string)) {
			$oldmask = umask(0);
			mkdir($string, 0777);
			umask($oldmask);
		}
	}

	function convert_mb($string) {
		$array = [
			'\u00e5' => 'å',
			'\u00e4' => 'ä',
			'\u00f6' => 'ö'
		];

		return strtr($string, $array);
	}

	function emoji_to_unicode($emoji) {
		if(mb_ord($emoji) < 256) return $emoji;
		$emoji = mb_convert_encoding($emoji, 'UTF-32', 'UTF-8');
		$unicode = strtoupper(preg_replace('/^[0]{3}/', '', bin2hex($emoji)));
		return $unicode;
	}

	function emoji_tag($emoji) {
		return '<i class="emoji" style="background-image: url('.Twemoji::emoji($emoji)->base(url('', true))->url().');"></i>';
	}

	function emoji_convert($string) {
		$out = null;
		for($i = 0; $i < mb_strlen($string); $i++) {
			$out .= emoji_to_unicode(mb_substr($string, $i, 1));
		}

		$emojis = Array(
			'1F44D1F3FB' => emoji_tag('👍'),
			'1F601' => emoji_tag('😁'),
			'1F603' => emoji_tag('😃'),
			'1F604' => emoji_tag('😄'),
			'1F606' => emoji_tag('😆'),
			'1F642' => emoji_tag('🙂'),
			'1F917' => emoji_tag('🤗'),
			'1F60A' => emoji_tag('😊'),
			'1F616' => emoji_tag('😖'),
			'1F612' => emoji_tag('😒'),
			'1F632' => emoji_tag('😲'),
			'1F928' => emoji_tag('🤨'),
			'1F4F8' => emoji_tag('📸'),
			'1F615' => emoji_tag('😕'),
			'1F602' => emoji_tag('😂'),
			'1F605' => emoji_tag('😅'),
			'1F613' => emoji_tag('😓'),

			':unamused:' => emoji_tag('😒'),
			':innocent:' => emoji_tag('😇'),
			':blush:' => emoji_tag('😊'),
			':joy:' => emoji_tag('😂'),
			':cry:' => emoji_tag('😭'),
			':sweat:' => emoji_tag('😅'),
			':yum:' => emoji_tag('😋'),
			':flushed:' => emoji_tag('😳'),

			':)' => emoji_tag('🙂'),
			'^^' => emoji_tag('😄'),
			':P' => emoji_tag('😛'),
			'xD' => emoji_tag('😆'),
			':D' => emoji_tag('😃'),
			':(' => emoji_tag('🙁'),
			# ';)' => emoji_tag('😉'),
			# ':/' => emoji_tag('😕'),
			'-.-' => emoji_tag('😑'),
			'&lt;3' => emoji_tag('♥'),
			'(Y)' => emoji_tag('👍'),
			'02026' => '...'
		);

		foreach($emojis AS $icon => $image) {
			$icon = preg_quote($icon);
			$text = preg_replace("~\b$icon\b~", $image, $string);
		}

		return strtr($out, $emojis);
	}

	function replace_br($data) {
		$data = preg_replace('#(?:<br\s*/?>\s*?){2,}#', '</p><p>', $data);
		return '<p>'.convert_mb($data).'</p>';
	}

	function replace_p($data) {
		$data = preg_replace('/<p>(.*)<\/p>/', '${1}<br/><br/>', $data);
		return convert_mb($data);
	}

	function calculate_dirsize($path) {
		$bytestotal = 0;
		$path = realpath($path);
		if($path !== false AND $path != '' AND file_exists($path)) {
			foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) AS $object) {
				$bytestotal += $object->getSize();
			}
		}

		return $bytestotal;
	}

	function calculate_filesize($string, $format = null) {
		$size = new FileSize($string);

		if($format == null) {
			return $size->asAuto();
		} else {
			return $size->as($format);
		}
	}

	function calculate_usedsize($dir) {
		$size = 0;
		foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each)
			$size += is_file($each) ? filesize($each) : calculate_usedsize($each);
		return $size;
	}



	function date_($timestamp, $format) {
		global $viewing_in_english;

		$months = Array(
			1 => ($viewing_in_english == false ? 'Januari' : 'January'),
			2 => 'Februari',
			3 => 'Mars',
			4 => 'April',
			5 => 'Maj',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Augusti',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'December'
		);


		if($format == 'short') {
			return date('Y-m-d, H:i', $timestamp);

		} elseif($format == 'full') {
			if($viewing_in_english == false) {
				return date('j', $timestamp).' '.mb_strtolower($months[date('n', $timestamp)]).', '.date('Y', $timestamp).', kl. '.date('H:i', $timestamp);
			} else {
				return date('F jS Y, g:i a (H:i)', $timestamp);
			}
		}
	}



	function compress_image($destination, $size) {
		$file_info = pathinfo($destination);
		$img = new Imagick();
		$img->readImage($destination);
		$img->setCompression(Imagick::COMPRESSION_JPEG);
		$img->setCompressionQuality(20);
		$img->stripImage();

		$img->resizeImage(
			min($img->getImageWidth(),  $size),
			min($img->getImageHeight(), $size),
			imagick::FILTER_CATROM, 1, true
		);

		$img->setInterlaceScheme(3);
		$img->writeImage($file_info['dirname'].'/'.$file_info['filename'].'.jpg');
		$img->destroy();
	}



	function svgicon($string) {
		global $theme;

		$array_icons = Array(
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 3a9 9 0 0 1 9 9h-2a7 7 0 0 0-7-7V3z"/></svg>' => 'loading',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="64" height="64"><path fill="none" d="M0 0h24v24H0z"/><path d="M3 8l6.003-6h10.995C20.55 2 21 2.455 21 2.992v18.016a.993.993 0 0 1-.993.992H3.993A1 1 0 0 1 3 20.993V8zm7-4.5L4.5 9H10V3.5z"/></svg>' => 'no-posts',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-1-5h2v2h-2v-2zm0-8h2v6h-2V7z"/></svg>' => 'outside-paging',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="64" height="64"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 3h14l2.707 2.707a1 1 0 0 1 .293.707V20a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm3 1v5h9V4H7zm-1 8v7h12v-7H6zm7-7h2v3h-2V5z"/></svg>' => 'nothing-saved',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.784 14l.42-4H4V8h4.415l.525-5h2.011l-.525 5h3.989l.525-5h2.011l-.525 5H20v2h-3.784l-.42 4H20v2h-4.415l-.525 5h-2.011l.525-5H9.585l-.525 5H7.049l.525-5H4v-2h3.784zm2.011 0h3.99l.42-4h-3.99l-.42 4z"/></svg>' => 'nothing-tagged',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.784 14l.42-4H4V8h4.415l.525-5h2.011l-.525 5h3.989l.525-5h2.011l-.525 5H20v2h-3.784l-.42 4H20v2h-4.415l-.525 5h-2.011l.525-5H9.585l-.525 5H7.049l.525-5H4v-2h3.784zm2.011 0h3.99l.42-4h-3.99l-.42 4z"/></svg>' => 'hashtag',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M21 9v11.993A1 1 0 0 1 20.007 22H3.993A.993.993 0 0 1 3 21.008V2.992C3 2.455 3.447 2 3.998 2H14v6a1 1 0 0 0 1 1h6zm0-2h-5V2.003L21 7z"/></svg>' => 'file',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M21 15.243v5.765a.993.993 0 0 1-.993.992H3.993A1 1 0 0 1 3 20.993V9h6a1 1 0 0 0 1-1V2h10.002c.551 0 .998.455.998.992v3.765l-8.999 9-.006 4.238 4.246.006L21 15.243zm.778-6.435l1.414 1.414L15.414 18l-1.416-.002.002-1.412 7.778-7.778zM3 7l5-4.997V7H3z"/></svg>' => 'file-edit',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7 6V3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1h-3v3c0 .552-.45 1-1.007 1H4.007A1.001 1.001 0 0 1 3 21l.003-14c0-.552.45-1 1.007-1H7zm2 0h8v10h2V4H9v2z"/></svg>' => 'copy',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5.463 4.433A9.961 9.961 0 0 1 12 2c5.523 0 10 4.477 10 10 0 2.136-.67 4.116-1.81 5.74L17 12h3A8 8 0 0 0 6.46 6.228l-.997-1.795zm13.074 15.134A9.961 9.961 0 0 1 12 22C6.477 22 2 17.523 2 12c0-2.136.67-4.116 1.81-5.74L7 12H4a8 8 0 0 0 13.54 5.772l.997 1.795z"/></svg>' => 'reload',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M3 3h18a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm4 12.5v-4l2 2 2-2v4h2v-7h-2l-2 2-2-2H5v7h2zm11-3v-4h-2v4h-2l3 3 3-3h-2z"/></svg>' => 'markdown',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M2 8.994A5.99 5.99 0 0 1 8 3h8c3.313 0 6 2.695 6 5.994V21H8c-3.313 0-6-2.695-6-5.994V8.994zM14 11v2h2v-2h-2zm-6 0v2h2v-2H8z"/></svg>' => 'answering-to',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"/></svg>' => 'title_arrow',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.52 0 10 4.48 10 10s-4.48 10-10 10S2 17.52 2 12 6.48 2 12 2zM6.023 15.416C7.491 17.606 9.695 19 12.16 19c2.464 0 4.669-1.393 6.136-3.584A8.968 8.968 0 0 0 12.16 13a8.968 8.968 0 0 0-6.137 2.416zM12 11a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/></svg>' => 'your-data',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm0 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16zm2 6a1 1 0 0 1 1 1v4h-1.5v4h-3v-4H9v-4a1 1 0 0 1 1-1h4zm-2-5a2 2 0 1 1 0 4 2 2 0 0 1 0-4z"/></svg>' => 'copyright-by',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.523 0 10 4.477 10 10 0 2.4-.846 4.604-2.256 6.328l.034.036-1.414 1.414-.036-.034A9.959 9.959 0 0 1 12 22C6.477 22 2 17.523 2 12S6.477 2 12 2zM4 12a8 8 0 0 0 12.905 6.32l-2.375-2.376A2.51 2.51 0 0 1 14 16h-1v2h-2v-2H8.5v-2H14a.5.5 0 0 0 .09-.992L14 13h-4a2.5 2.5 0 0 1-2.165-3.75L5.679 7.094A7.965 7.965 0 0 0 4 12zm8-8c-1.848 0-3.55.627-4.905 1.68L9.47 8.055A2.51 2.51 0 0 1 10 8h1V6h2v2h2.5v2H10a.5.5 0 0 0-.09.992L10 11h4a2.5 2.5 0 0 1 2.165 3.75l2.156 2.155A8 8 0 0 0 12 4z"/></svg>' => 'copyright-nc',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm0 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16zm4 9v2H8v-2h8zm0-4v2H8V9h8z"/></svg>' => 'copyright-nd',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20"><path fill="none" d="M0 0h24v24H0z"/><path d="M2 9h3v12H2a1 1 0 0 1-1-1V10a1 1 0 0 1 1-1zm5.293-1.293l6.4-6.4a.5.5 0 0 1 .654-.047l.853.64a1.5 1.5 0 0 1 .553 1.57L14.6 8H21a2 2 0 0 1 2 2v2.104a2 2 0 0 1-.15.762l-3.095 7.515a1 1 0 0 1-.925.619H8a1 1 0 0 1-1-1V8.414a1 1 0 0 1 .293-.707z"/></svg>' => 'thumb-up',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20"><path fill="none" d="M0 0h24v24H0z"/><path d="M22 15h-3V3h3a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zm-5.293 1.293l-6.4 6.4a.5.5 0 0 1-.654.047L8.8 22.1a1.5 1.5 0 0 1-.553-1.57L9.4 16H3a2 2 0 0 1-2-2v-2.104a2 2 0 0 1 .15-.762L4.246 3.62A1 1 0 0 1 5.17 3H16a1 1 0 0 1 1 1v11.586a1 1 0 0 1-.293.707z"/></svg>' => 'thumb-down',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M18 3v2h-1v6l2 3v2h-6v7h-2v-7H5v-2l2-3V5H6V3z"/></svg>' => 'pinned',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M16.8 19L14 22.5 11.2 19H6a1 1 0 0 1-1-1V7.103a1 1 0 0 1 1-1h16a1 1 0 0 1 1 1V18a1 1 0 0 1-1 1h-5.2zM2 2h17v2H3v11H1V3a1 1 0 0 1 1-1z"/></svg>' => 'comments',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z"/></svg>' => 'menu',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 10.586l4.95-4.95 1.414 1.414-4.95 4.95 4.95 4.95-1.414 1.414-4.95-4.95-4.95 4.95-1.414-1.414 4.95-4.95-4.95-4.95L7.05 5.636z"/></svg>' => 'close',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M22 12v2H2v-2h2V2.995c0-.55.445-.995.996-.995H15l5 5v5h2zM3 16h2v6H3v-6zm16 0h2v6h-2v-6zm-4 0h2v6h-2v-6zm-4 0h2v6h-2v-6zm-4 0h2v6H7v-6z"/></svg>' => 'delete',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M1.181 12C2.121 6.88 6.608 3 12 3c5.392 0 9.878 3.88 10.819 9-.94 5.12-5.427 9-10.819 9-5.392 0-9.878-3.88-10.819-9zM12 17a5 5 0 1 0 0-10 5 5 0 0 0 0 10zm0-2a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/></svg>' => 'open',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-2.29-2.333A17.9 17.9 0 0 1 8.027 13H4.062a8.008 8.008 0 0 0 5.648 6.667zM10.03 13c.151 2.439.848 4.73 1.97 6.752A15.905 15.905 0 0 0 13.97 13h-3.94zm9.908 0h-3.965a17.9 17.9 0 0 1-1.683 6.667A8.008 8.008 0 0 0 19.938 13zM4.062 11h3.965A17.9 17.9 0 0 1 9.71 4.333 8.008 8.008 0 0 0 4.062 11zm5.969 0h3.938A15.905 15.905 0 0 0 12 4.248 15.905 15.905 0 0 0 10.03 11zm4.259-6.667A17.9 17.9 0 0 1 15.973 11h3.965a8.008 8.008 0 0 0-5.648-6.667z"/></svg>' => 'global',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-1-7v2h2v-2h-2zm0-8v6h2V7h-2z"/></svg>' => 'warning',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72 72" width="24" height="24"><path fill="#EA5A47" d="M59.5,25c0-6.9036-5.5964-12.5-12.5-12.5c-4.7533,0-8.8861,2.6536-11,6.5598 C33.8861,15.1536,29.7533,12.5,25,12.5c-6.9036,0-12.5,5.5964-12.5,12.5c0,2.9699,1.0403,5.6942,2.7703,7.8387l-0.0043,0.0034 L36,58.5397l20.7339-25.6975l-0.0043-0.0034C58.4597,30.6942,59.5,27.9699,59.5,25z"/></svg>' => 'heart',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="64" height="64"><path fill="none" d="M0 0h24v24H0z"/><path d="M6.455 19L2 22.5V4a1 1 0 0 1 1-1h18a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H6.455z"/></svg>' => 'no-comments',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-10 0 1034 1024" width="64" height="64"><path fill="currentColor" d="M857 216l-230 214h111l-85 368h-2l-120 -368h-144l-121 362h-2l-111 -476h-153l185 700h155l117 -363h2l118 363h153l157 -586h113z"/></svg>' => 'no-webmentions',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" id="telegram"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z" fill="rgba(0,136,204,1)"/></svg>' => 'telegram',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" id="mastodon"><path fill="none" d="M0 0h24v24H0z"/><path d="M21.258 13.99c-.274 1.41-2.456 2.955-4.962 3.254-1.306.156-2.593.3-3.965.236-2.243-.103-4.014-.535-4.014-.535 0 .218.014.426.04.62.292 2.215 2.196 2.347 4 2.41 1.82.062 3.44-.45 3.44-.45l.076 1.646s-1.274.684-3.542.81c-1.25.068-2.803-.032-4.612-.51-3.923-1.039-4.598-5.22-4.701-9.464-.031-1.26-.012-2.447-.012-3.44 0-4.34 2.843-5.611 2.843-5.611 1.433-.658 3.892-.935 6.45-.956h.062c2.557.02 5.018.298 6.451.956 0 0 2.843 1.272 2.843 5.61 0 0 .036 3.201-.397 5.424zm-2.956-5.087c0-1.074-.273-1.927-.822-2.558-.567-.631-1.308-.955-2.229-.955-1.065 0-1.871.41-2.405 1.228l-.518.87-.519-.87C11.276 5.8 10.47 5.39 9.405 5.39c-.921 0-1.663.324-2.229.955-.549.631-.822 1.484-.822 2.558v5.253h2.081V9.057c0-1.075.452-1.62 1.357-1.62 1 0 1.501.647 1.501 1.927v2.79h2.07v-2.79c0-1.28.5-1.927 1.5-1.927.905 0 1.358.545 1.358 1.62v5.1h2.08V8.902z"/></svg>' => 'mastodon',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" id="facebook"><path fill="none" d="M0 0h24v24H0z"/><path d="M15.402 21v-6.966h2.333l.349-2.708h-2.682V9.598c0-.784.218-1.319 1.342-1.319h1.434V5.857a19.19 19.19 0 0 0-2.09-.107c-2.067 0-3.482 1.262-3.482 3.58v1.996h-2.338v2.708h2.338V21H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h16a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1h-4.598z"/></svg>' => 'facebook',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" id="messenger"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.634 0 10 4.127 10 9.7 0 5.573-4.366 9.7-10 9.7a10.894 10.894 0 0 1-2.895-.384.8.8 0 0 0-.534.039l-1.984.876a.8.8 0 0 1-1.123-.707l-.055-1.78a.797.797 0 0 0-.268-.57C3.195 17.135 2 14.617 2 11.7 2 6.127 6.367 2 12 2zM5.995 14.537c-.282.447.268.951.689.631l3.155-2.394a.6.6 0 0 1 .723 0l2.337 1.75a1.5 1.5 0 0 0 2.169-.4l2.937-4.66c.282-.448-.268-.952-.689-.633l-3.155 2.396a.6.6 0 0 1-.723 0l-2.337-1.75a1.5 1.5 0 0 0-2.169.4l-2.937 4.66z"/></svg>' => 'messenger',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0H24V24H0z"/><path d="M14 4.5V9c5.523 0 10 4.477 10 10 0 .273-.01.543-.032.81-1.463-2.774-4.33-4.691-7.655-4.805L16 15h-2v4.5L6 12l8-7.5zm-6 0v2.737L2.92 12l5.079 4.761L8 19.5 0 12l8-7.5z"/></svg>' => 'toot-replies',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 18.26l-7.053 3.948 1.575-7.928L.587 8.792l8.027-.952L12 .5l3.386 7.34 8.027.952-5.935 5.488 1.575 7.928z"/></svg>' => 'toot-favourites',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0H24V24H0z"/><path d="M16.5 3C19.538 3 22 5.5 22 9c0 7-7.5 11-10 12.5C9.5 20 2 16 2 9c0-3.5 2.5-6 5.5-6C9.36 3 11 4 12 5c1-1 2.64-2 4.5-2z"/></svg>' => 'webmention-like',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M6 4h15a1 1 0 0 1 1 1v7h-2V6H6v3L1 5l5-4v3zm12 16H3a1 1 0 0 1-1-1v-7h2v6h14v-3l5 4-5 4v-3z"/></svg>' => 'webmention-repost',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5.334 4.545a9.99 9.99 0 0 1 3.542-2.048A3.993 3.993 0 0 0 12 3.999a3.993 3.993 0 0 0 3.124-1.502 9.99 9.99 0 0 1 3.542 2.048 3.993 3.993 0 0 0 .262 3.454 3.993 3.993 0 0 0 2.863 1.955 10.043 10.043 0 0 1 0 4.09c-1.16.178-2.23.86-2.863 1.955a3.993 3.993 0 0 0-.262 3.455 9.99 9.99 0 0 1-3.542 2.047A3.993 3.993 0 0 0 12 20a3.993 3.993 0 0 0-3.124 1.502 9.99 9.99 0 0 1-3.542-2.047 3.993 3.993 0 0 0-.262-3.455 3.993 3.993 0 0 0-2.863-1.954 10.043 10.043 0 0 1 0-4.091 3.993 3.993 0 0 0 2.863-1.955 3.993 3.993 0 0 0 .262-3.454zM13.5 14.597a3 3 0 1 0-3-5.196 3 3 0 0 0 3 5.196z"/></svg>' => 'manage',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M20 20a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-9H1l10.327-9.388a1 1 0 0 1 1.346 0L23 11h-3v9z"/></svg>' => 'linkicon-home',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M18.031 16.617l4.283 4.282-1.415 1.415-4.282-4.283A8.96 8.96 0 0 1 11 20c-4.968 0-9-4.032-9-9s4.032-9 9-9 9 4.032 9 9a8.96 8.96 0 0 1-1.969 5.617zm-2.006-.742A6.977 6.977 0 0 0 18 11c0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7a6.977 6.977 0 0 0 4.875-1.975l.15-.15z"/></svg>' => 'linkicon-search',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11 2.05V13h10.95c-.501 5.053-4.765 9-9.95 9-5.523 0-10-4.477-10-10 0-5.185 3.947-9.449 9-9.95zm2-1.507C18.553 1.02 22.979 5.447 23.457 11H13V.543z"/></svg>' => 'linkicon-statistics',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M8 18h10.237L20 19.385V9h1a1 1 0 0 1 1 1v13.5L17.545 20H9a1 1 0 0 1-1-1v-1zm-2.545-2L1 19.5V4a1 1 0 0 1 1-1h15a1 1 0 0 1 1 1v12H5.455z"/></svg>' => 'linkicon-comments',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M20 22H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h16a1 1 0 0 1 1 1v18a1 1 0 0 1-1 1zM7 6v4h4V6H7zm0 6v2h10v-2H7zm0 4v2h10v-2H7zm6-9v2h4V7h-4z"/></svg>' => 'linkicon-dice',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.784 14l.42-4H4V8h4.415l.525-5h2.011l-.525 5h3.989l.525-5h2.011l-.525 5H20v2h-3.784l-.42 4H20v2h-4.415l-.525 5h-2.011l.525-5H9.585l-.525 5H7.049l.525-5H4v-2h3.784zm2.011 0h3.99l.42-4h-3.99l-.42 4z"/></svg>' => 'linkicon-tag',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M3.783 2.826L12 1l8.217 1.826a1 1 0 0 1 .783.976v9.987a6 6 0 0 1-2.672 4.992L12 23l-6.328-4.219A6 6 0 0 1 3 13.79V3.802a1 1 0 0 1 .783-.976zM12 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5zm-4.473 5h8.946a4.5 4.5 0 0 0-8.946 0z"/></svg>' => 'linkicon-privacy',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 2c5.52 0 10 4.48 10 10s-4.48 10-10 10S2 17.52 2 12 6.48 2 12 2zm0 5c-2.76 0-5 2.24-5 5s2.24 5 5 5c1.82 0 3.413-.973 4.288-2.428l-1.715-1.028A3 3 0 1 1 12 9c1.093 0 2.05.584 2.574 1.457l1.714-1.03A4.999 4.999 0 0 0 12 7z"/></svg>' => 'linkicon-copyright',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M10 11V8l5 4-5 4v-3H1v-2h9zm-7.542 4h2.124A8.003 8.003 0 0 0 20 12 8 8 0 0 0 4.582 9H2.458C3.732 4.943 7.522 2 12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10c-4.478 0-8.268-2.943-9.542-7z"/></svg>' => 'linkicon-login',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.105 15.21A3.001 3.001 0 1 1 5 15.17V8.83a3.001 3.001 0 1 1 2 0V12c.836-.628 1.874-1 3-1h4a3.001 3.001 0 0 0 2.895-2.21 3.001 3.001 0 1 1 2.032.064A5.001 5.001 0 0 1 14 13h-4a3.001 3.001 0 0 0-2.895 2.21z"/></svg>' => 'linkicon-git',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M3 3c9.941 0 18 8.059 18 18h-3c0-8.284-6.716-15-15-15V3zm0 7c6.075 0 11 4.925 11 11h-3a8 8 0 0 0-8-8v-3zm0 7a4 4 0 0 1 4 4H3v-4z"/></svg>' => 'linkicon-rss',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 18a6 6 0 1 1 0-12 6 6 0 0 1 0 12zM11 1h2v3h-2V1zm0 19h2v3h-2v-3zM3.515 4.929l1.414-1.414L7.05 5.636 5.636 7.05 3.515 4.93zM16.95 18.364l1.414-1.414 2.121 2.121-1.414 1.414-2.121-2.121zm2.121-14.85l1.414 1.415-2.121 2.121-1.414-1.414 2.121-2.121zM5.636 16.95l1.414 1.414-2.121 2.121-1.414-1.414 2.121-2.121zM23 11v2h-3v-2h3zM4 11v2H1v-2h3z"/></svg>' => 'linkicon-theme-light',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11.38 2.019a7.5 7.5 0 1 0 10.6 10.6C21.662 17.854 17.316 22 12.001 22 6.477 22 2 17.523 2 12c0-5.315 4.146-9.661 9.38-9.981z"/></svg>' => 'linkicon-theme-dark',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11 11V7h2v4h4v2h-4v4h-2v-4H7v-2h4zm1 11C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16z"/></svg>' => 'linkicon-admin-write',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 3h14l2.707 2.707a1 1 0 0 1 .293.707V20a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm3 1v5h9V4H7zm-1 8v7h12v-7H6zm7-7h2v3h-2V5z"/></svg>' => 'linkicon-admin-saved',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M21 9v11.993A1 1 0 0 1 20.007 22H3.993A.993.993 0 0 1 3 21.008V2.992C3 2.455 3.447 2 3.998 2H14v6a1 1 0 0 0 1 1h6zm0-2h-5V2.003L21 7z"/></svg>' => 'linkicon-admin-files',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5.33 3.271a3.5 3.5 0 0 1 4.472 4.474L20.647 18.59l-2.122 2.121L7.68 9.867a3.5 3.5 0 0 1-4.472-4.474L5.444 7.63a1.5 1.5 0 1 0 2.121-2.121L5.329 3.27zm10.367 1.884l3.182-1.768 1.414 1.414-1.768 3.182-1.768.354-2.12 2.121-1.415-1.414 2.121-2.121.354-1.768zm-7.071 7.778l2.121 2.122-4.95 4.95A1.5 1.5 0 0 1 3.58 17.99l.097-.107 4.95-4.95z"/></svg>' => 'linkicon-admin-settings',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5 11h8v2H5v3l-5-4 5-4v3zm-1 7h2.708a8 8 0 1 0 0-12H4A9.985 9.985 0 0 1 12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10a9.985 9.985 0 0 1-8-4z"/></svg>' => 'linkicon-admin-logout',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 1l8.217 1.826a1 1 0 0 1 .783.976v9.987a6 6 0 0 1-2.672 4.992L12 23l-6.328-4.219A6 6 0 0 1 3 13.79V3.802a1 1 0 0 1 .783-.976L12 1zm0 6a2 2 0 0 0-1 3.732V15h2l.001-4.268A2 2 0 0 0 12 7z"/></svg>' => 'login',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M8 7v4L2 6l6-5v4h5a8 8 0 1 1 0 16H4v-2h9a6 6 0 1 0 0-12H8z"/></svg>' => 'restore',

			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0H24V24H0z"/><path d="M21 18v2H3v-2h18zM17.404 3.904L22 8.5l-4.596 4.596-1.414-1.414L19.172 8.5 15.99 5.318l1.414-1.414zM12 11v2H3v-2h9zm0-7v2H3V4h9z"/></svg>' => 'menu-open',
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0H24V24H0z"/><path d="M21 18v2H3v-2h18zM6.596 3.904L8.01 5.318 4.828 8.5l3.182 3.182-1.414 1.414L2 8.5l4.596-4.596zM21 11v2h-9v-2h9zm0-7v2h-9V4h9z"/></svg>' => 'menu-close'
		);

		return array_search($string, $array_icons);
	}



	function getip() {
		if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
			$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
			$_SERVER['HTTP_CLIENT_IP'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
		}

		$client = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote = $_SERVER['REMOTE_ADDR'] ?? gethostbyname(gethostname());

		if(filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}

		return $ip;
	}



	function sql($query, $array, $method = '') {

		global $sql;


		try {
			$prepare = $sql->prepare($query);

			if($method == 'count') {
				$prepare->execute($array);
				$data = $prepare->fetchColumn();
				return $data;


			} elseif($method == 'fetch') {
				$prepare->execute($array);
				$data = $prepare->fetch(PDO::FETCH_ASSOC);
				return $data;


			} elseif($method == 'insert') {
				foreach($array AS $data => $value) {
					$prepare->bindValue(':'.$data, $value);
				}

				$prepare->execute();
				return $value;


			} else {
				$prepare->execute($array);
				return $prepare;
			}


		} catch(Exception $e) {
			throw($e);
		}

	}

?>
