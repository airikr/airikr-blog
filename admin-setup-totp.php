<?php

	require_once 'site-header.php';



	$c_users = sql("SELECT COUNT(id)
					FROM users
				   ", Array(), 'count');

	if($c_users == 0) {
		sql("INSERT INTO users(
				 username,
				 password,
				 is_admin
			 )

			 VALUES(
				 :_username,
				 :_password,
				 :_isadmin
			 )
			", Array(
				'_username' => 'admin',
				'_password' => password_hash('admin', PASSWORD_BCRYPT),
				'_isadmin' => 1
			), 'insert');

	} else {
		$check_admin = sql("SELECT password
							FROM users
							WHERE username = :_username
						   ", Array(
							   '_username' => 'admin'
						   ), 'fetch');
	}



	$ga = new PHPGangsta_GoogleAuthenticator();
	$secret = $ga->createSecret();

	$barcode = new \Com\Tecnick\Barcode\Barcode();
	$barcodeObj = $barcode->getBarcodeObj('QRCODE,H', 'otpauth://totp/Airikr Blog?secret='.$secret, -5, -5, 'black', array(-1, -1, -1, -1))->setBackgroundColor('#ffffff');
	$image = $barcodeObj->getSvgCode();







	echo '<section id="config">';
		echo '<h2>';
			echo ($viewing_in_english == false ? 'Konfiguration' : 'Configuration');
		echo '</h2>';

		if($viewing_in_english == false) {
			echo '<p>Eftersom det konto du skapade var det första, så är det nu väldigt osäkert. För att kunna använda bloggen, måste du ersätta lösenordet med ett engångslösenord.</p>';
		} else {
			echo '<p>asd</p>';
		}



		echo '<div id="message"></div>';
		echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate="novalidate">';

			echo '<div class="code">';
				echo '<div class="secret">';
					echo '<input type="text" name="field-secret" value="'.$secret.'" readonly>';
				echo '</div>';

				echo '<div class="qrcode">';
					echo $image;
				echo '</div>';
			echo '</div>';


			echo '<div class="fields">';
				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Vad är engångskoden?' : 'What is the one-time code?');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-totp" maxlength="6" placeholder="Krävs" tabindex="1" required>';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Ditt användarnamn' : 'Your username');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-username" placeholder="Krävs" tabindex="2" required>';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Din e-postadress' : 'Your email address');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="email" name="field-email" placeholder="Krävs" tabindex="3" required>';
					echo '</div>';
				echo '</div>';

				echo '<div class="button">';
					echo '<input type="submit" name="button-verify" tabindex="4" value="'.($viewing_in_english == false ? 'Verifiera' : 'Verify').'">';
				echo '</div>';
			echo '</div>';

		echo '</form>';
	echo '</section>';







	require_once 'site-footer.php';

?>
