<?php

	require_once 'site-header.php';







	echo '<section id="copyright">';
		echo '<h1>'.($viewing_in_english == false ? 'Licens' : 'License').'</h1>';


		if($viewing_in_english == false) {
			echo '<p>Allt material på '.$og_url_noprotocol.' (om inget annat nämns) är skapad av '.link_($config_author_name, 'https://airikr.me').' för webbsidan. Du är tillåten att dela vidare innehållet enligt villkoren nedan. Källkoden för '.$og_title.' är öppen och kan hittas på '.link_('Codeberg', 'https://codeberg.org/airikr/airikr-blog').'.</p>';
			echo '<p>Du får <b>dela och vidaredistribuera materialet</b> oavsett medium eller format på följande villkor:</p>';

			echo '<ul>';
				echo '<li>Du måste <b>ge ett korrekt erkännande</b>, <b>länka tillbaka till denna sida</b> och <b>ange vad du har ändrat</b>.</li>';
				echo '<li>Du <b>får inte</b> använda materialet för kommersiella syften.</li>';
				echo '<li>Om du remixar, transformerar, eller bygger vidare på materialet, så får du <b>inte sprida det modifierade materialet</b>.</li>';
			echo '</ul>';

			echo '<div class="icons">';
				echo svgicon('linkicon-copyright');
				echo svgicon('copyright-by');
				echo svgicon('copyright-nc');
				echo svgicon('copyright-nd');
			echo '</div>';


			echo '<h4>Omslagsbilder</h4>';
			echo '<p>Alla omslagsbilder är från '.link_('Unsplash', 'https://unsplash.com').' och fotografens namn står under varje omslagsbild. Läs gärna '.link_('deras licens', 'https://unsplash.com/license').'.</p>';



		} else {
			echo '<p>All material on '.$og_url_noprotocol.' (if nothing else mentions) are created by '.link_($config_author_name, 'https://airikr.me/en').' for the website. You are allowed to distribute the content accordingly to the terms below. The source code for '.$og_title.' is open and can be found at '.link_('Codeberg', 'https://codeberg.org/airikr/airikr-blog').'.</p>';
			echo '<p>You are <b>allowed to copy and redistribute the material</b> regardless of medium or format on the following terms:</p>';

			echo '<ul>';
				echo '<li>You must <b>give appropriate credit</b>, <b>provide a link back to this license page</b>, and <b>indicate what you have changed</b>.</li>';
				echo '<li>You <b>are not allowed</b> to use the material for commercial purposes.</li>';
				echo '<li>If you remix, transform or build upon the material, you are <b>not allowed to distribute the modified material</b>.</li>';
			echo '</ul>';

			echo '<div class="icons">';
				echo svgicon('linkicon-copyright');
				echo svgicon('copyright-by');
				echo svgicon('copyright-nc');
				echo svgicon('copyright-nd');
			echo '</div>';


			echo '<h4>Covers</h4>';
			echo '<p>All covers are from '.link_('Unsplash', 'https://unsplash.com').' and the name of the photograph are written under each cover. Please read '.link_('their license', 'https://unsplash.com/license').'.</p>';

		}
	echo '</section>';







	require_once 'site-footer.php';

?>
