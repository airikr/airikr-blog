$(document).ready(function() {


	debugging = false;
	viewing_in_english = ($('main').data('language') != 'en' ? false : true);
	theme_light = ($('main').data('theme') != 'light' ? false : true);
	current_paging = (!$('section#blog').is(':visible') ? null : $('section#blog').data('paging'));
	current_page = (!$('section#blog').is(':visible') ? null : $('section#blog').data('page'));
	tag = (!$('section#blog').is(':visible') ? null : $('section#blog').data('tag'));
	search = (!$('section#blog').is(':visible') ? null : $('section#blog').data('search'));
	url = window.location.href;
	get_hash = window.location.hash.substr(1);
	folder_name = '/' + ((url.indexOf('localhost') > -1 || url.indexOf('182') > -1) ? $('main').data('localfolder') + '/' : '');
	settings_language = (viewing_in_english == false ? '' : 'en');
	settings_theme = (theme_light == false ? '' : '');
	settings = '';
	prefers_color_scheme = window.matchMedia("(prefers-color-scheme: dark)");

	autosize($('textarea'));



	$('body').on('click', '.minimized', function() {
		$('.minimized').hide();
		$('.maximized').show();
	});

	$('body').on('click', 'a#hide', function() {
		$('.minimized').show();
		$('.maximized').hide();
	});

	$('body').on('click', 'a#allow', function() {
		var page = 'ajax/allow-analytics';

		$('.message-analytics').hide().removeClass('color-red').removeClass('color-green');

		$.ajax({
			url: folder_name + page,
			method: 'GET',
			success: function(s) {

				if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + page); console.log(s); console.groupEnd(); }

				if(s == 'error') {
					$('.message-analytics').show().addClass('color-red').text((viewing_in_english == false ? 'Kunde inte initiera datainsamlingen' : 'Were not able to initialize the analytics'));
					$('.maximized > .links').hide();

					setTimeout(function() {
						$('.message-analytics').hide();
						$('.maximized > .links').show();
					}, 2000);


				} else if(s == 'added') {
					$('.maximized > .links').hide();
					$('.message-analytics').show().addClass('color-green').text((viewing_in_english == false ? 'Tack' : 'Thank you') + ' :)');

					setTimeout(function() {
						$('.maximized').hide();
						if(window.location.pathname.indexOf('your-data') >= 0) {
							window.location.reload();
						}
					}, 1500);
				}

			},

			error: function(e) {
				$('.message-analytics').show().addClass('color-green').text((viewing_in_english == false ? 'Något gick fel' : 'Something went wrong'));
				$('.maximized > .links').hide();
				console.log(e);
			}
		});
	});



	if($('section#your-data').is(':visible')) {
		$('body').on('click', 'a#delete', function() {
			var page = 'ajax/delete-all-data';

			if(confirm((viewing_in_english == false ? 'Är du säker?' : 'Are you sure?'))) {
				$.ajax({
					url: folder_name + page,
					method: 'GET',
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + page); console.log(s); console.groupEnd(); }

						if(s == 'error') {
							//

						} else if(s == 'deleted') {
							$('div#analytics-question').show();
							$('.minimized').show();

							$('.message').show();
							$('.content').hide();
						}

					},

					error: function(e) {
						// $('.message').addClass('color-red').text((viewing_in_english == false ? 'Kunde inte läsa in sidan' : 'Were not able to load the page'));
						console.log(e);
					}
				});
			}
		});
	}



	$('body').on('click', 'a.theme', function() {
		var theme = $(this).data('theme');

		if(typeof(Storage) !== void(0)) {
			localStorage.setItem('theme', theme);

			if(theme == 'light') {
				$('.theme[data-theme="light"]').addClass('hide');
				$('.theme[data-theme="dark"]').removeClass('hide');
				$('link#theme').attr('href', folder_name + 'css/theme-light.css');

			} else if(theme == 'dark') {
				$('.theme[data-theme="light"]').removeClass('hide');
				$('.theme[data-theme="dark"]').addClass('hide');
				$('link#theme').attr('href', folder_name + 'css/theme-dark.css');
			}

		} else {
			alert('Browser not supporting localStorage.');
		}
	});


	if(localStorage.getItem('theme') == 'dark') {
		$('.theme[data-theme="light"]').removeClass('hide');
		$('.theme[data-theme="dark"]').addClass('hide');
		$('link#theme').attr('href', folder_name + 'css/theme-dark.css');

	} else if(localStorage.getItem('theme') == 'light') {
		$('.theme[data-theme="light"]').addClass('hide');
		$('.theme[data-theme="dark"]').removeClass('hide');
		$('link#theme').attr('href', folder_name + 'css/theme-light.css');

	} else {
		prefers_color_scheme.addListener(e => {
			if(e.matches) {
				$('.theme[data-theme="light"]').removeClass('hide');
				$('.theme[data-theme="dark"]').addClass('hide');
				$('link#theme').attr('href', folder_name + 'css/theme-dark.css');

			} else {
				$('.theme[data-theme="light"]').addClass('hide');
				$('.theme[data-theme="dark"]').removeClass('hide');
				$('link#theme').attr('href', folder_name + 'css/theme-light.css');
			}
		});
	}


	$('#file').bind('change', function() {
		var file_input = document.getElementById('file');
		if(file_input.files[0].length != 0) {
			var file_name = file_input.files[0].name;
			var file_size = file_input.files[0].size;
		}

		$('#file-selected').html(file_name + ' (' + convert_filesize(file_size) + ')');
	});







	if($('.aside').is(':visible')) {

		var hammertime = new Hammer.Manager(document, {
			touchAction: 'auto',
			inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
			recognizers: [
				[
					Hammer.Swipe, {
						direction: Hammer.DIRECTION_HORIZONTAL
					}
				]
			]
		});

		hammertime.on('swiperight', function() {
			$('aside').show('slide', { direction: 'left' }, 100);
			$('.dim').fadeIn(200);
			$('.aside.close').show('slide', { direction: 'left' }, 100);
			$('.aside.open').hide();
			$('.change-theme').hide();
		});


		// Open the side menu
		$('body').on('click', '.aside.open', function() {
			$('aside').show('slide', { direction: 'left' }, 100);
			$('.dim').fadeIn(200);
			$('.aside.close').show('slide', { direction: 'left' }, 100);
			$('.aside.open').hide();
			$('.change-theme').hide();
		});

		// Close the side menu
		$('body').on('click', '.aside.close', function() {
			$('aside').hide('slide', { direction: 'left' }, 100);
			$('.dim').fadeOut(200);
			$('.aside.close').hide('slide', { direction: 'left' }, 100);
			$('.change-theme').show();

			setTimeout(function() {
				$('.aside.open').show();
			}, 100);
		});

		// Close the side menu by clicking outside of it
		$(document).on('click', function(e) {
			if(e.target.id == 'dim') {
				$('aside').hide('slide', { direction: 'left' }, 100);
				$('.dim').fadeOut(200);
				$('.aside.close').hide('slide', { direction: 'left' }, 100);
				$('.change-theme').show();

				setTimeout(function() {
					$('.aside.open').show();
				}, 100);
			}
		})

	}







	if($('section#privacy').is(':visible')) {

		var do_exists = (localStorage.getItem('theme') ? true : false);

		$('.answer > span:not(.checking)').hide();


		if(do_exists == false) {
			$('.answer > span').hide();
			$('.answer-no').show();

		} else {
			$('.answer > span').hide();
			$('.answer-yes').show();
		}


		$('body').on('click', 'a#delete', function() {
			localStorage.clear();

			$('.answer > span').hide();
			$('.deleting').show();
			setTimeout(function() {
				$('.answer > span').hide();
				$('.deleted').show();

				if(prefers_color_scheme.matches) {
					$('.theme[data-theme="light"]').css({ 'display': 'block' });
					$('.theme[data-theme="dark"]').hide();
					$('link#theme').attr('href', folder_name + 'css/theme-dark.css');

				} else {
					$('.theme[data-theme="light"]').hide();
					$('.theme[data-theme="dark"]').css({ 'display': 'block' });
					$('link#theme').attr('href', folder_name + 'css/theme-light.css');
				}
			}, 400);
		});

		$('body').on('click', 'a#check', function() {
			var do_exists = (localStorage.getItem('theme') ? true : false);

			$('.answer > span').hide();
			$('.checking').show();

			setTimeout(function() {
				if(do_exists == false) {
					$('.answer > span').hide();
					$('.answer-no').show();

				} else {
					$('.answer > span').hide();
					$('.answer-yes').show();
				}
			}, 400);
		});

	}







	if($('section#blog').is(':visible')) {

		$.ajax({
			url: folder_name + (viewing_in_english == false ? '' : 'en/') + 'load:posts' + (tag == '' ? '' : '/tag:' + tag) + (search == '' ? '' : '/search:' + search) + '/page:' + (current_page == 'saved' ? current_page : current_paging),
			method: 'GET',
			async: true,
			success: function(s) {
				$('section#blog').html(s);
			},
			error: function(e) {
				$('.message').addClass('color-red').text((viewing_in_english == false ? 'Kunde inte läsa in sidan' : 'Were not able to load the page'));
				console.log(e);
			}
		});

	}







	if($('section#read').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Load the comments
		loadcomments($('section#read').data('idpost'));

		// Refresh the list
		$('body').on('click', 'a#reload', function() {
			$('section#read > .list-comments > .message').show();
			$('section#read > .list-comments > .comments').hide();

			setTimeout(function() {
				loadcomments($('section#read').data('idpost'));
				scrollto('.list-comments', 200, 10);
			}, 400);
		});

		// Make the popup draggable
		$('.popup > div > div').draggable({
			containment: 'parent',
			cursor: 'move'
		});

		// Scroll to the comments
		$('body').on('click', 'a#go-to-comments', function() {
			scrollto('.list-comments', 200, 10);
		});



		// Enable the button when reached X characters
		$('body').on('input keyup', 'textarea[name="field-comment"]', function() {
			if($(this).val().length > 10) {
				$('input[name="button-showpopup"]').attr({ 'disabled': false });
			} else {
				$('input[name="button-showpopup"]').attr({ 'disabled': true });
			}
		});



		// Send a comment
		$('body').on('click', 'input[name="button-showpopup"]', function() {

			// Text fields
			var field_url = $('input[name="field-url"]').val();


			$('.dim').show();
			$('.popup').show();
			$('input').blur();

			if(validator.isEmpty(field_url)) {
				$('.url').hide();
			} else {
				$('.url').show();
			}

		});



		// Close the popup
		$('body').on('click', 'a#close', function() {
			$('.dim').hide();
			$('.popup').hide();
			$('.message-popup').hide();
			$('input[id*="check"]').prop({ 'checked': false });
			$('input[name="button-send"]').removeClass('password-warning');
		});

		// Close the popup with the Esc key
		$(document).keyup(function(e) {
			if(e.keyCode == 27) {
				$('.dim').hide();
				$('.popup').hide();
				$('.message-popup').hide();
				$('input[id*="check"]').prop({ 'checked': false });
				$('input[name="button-send"]').removeClass('password-warning');
			}
		})

		// Checking the "I agree ..." checkboxe
		$('input#check-1').on('change', function() {
			if($('.message-popup').is(':visible') && $(this).is(':checked')) {
				$('.message-popup').hide();
			}
		});



		// Answer a comment
		$('body').on('click', '.answer', function() {
			var comment_id = $(this).data('idcomment');
			var answer_id = ($(this).data('idanswer') == '' ? null : $(this).data('idanswer'));
			var id_comment = (answer_id == null ? comment_id : answer_id);

			scrollto('.write-a-comment', 200, 10);

			$('.item').removeClass('highlight');
			$('a#cancel').attr({ 'data-idcomment': comment_id });
			$('.item[data-idcomment="' + id_comment + '"]').addClass('highlight');
			$('input#pw').val('');
			$('a#cancel').show();

			$('.answer').text((viewing_in_english == false ? 'Svara' : 'Answer'));
			$('.answer[data-idcomment="' + comment_id + '"][data-idanswer="' + (answer_id == null ? '' : answer_id) + '"]').text((viewing_in_english == false ? 'Gå till formuläret' : 'Go to the form'));

			$('.write-a-comment > h4').text((viewing_in_english == false ? 'Skriv ett svar till' : 'Write an answer to') + ' ' + $('.name[data-idcomment="' + id_comment + '"]').text());
			$('input[name="hidden-idcomment"]').val(comment_id);
			$('input[name="hidden-idanswer"]').val(answer_id);

			$('a#reload').hide();
			$('div#reload-inactive').show();
		});



		// Manage a comment
		$('body').on('click', '.manage', function() {
			var comment_id = $(this).data('idcomment');

			$('.item').removeClass('highlight');
			$('.manage').show();
			$('.managing').hide();
			$('input#pw').val('');

			$('.manage[data-idcomment="' + comment_id + '"]').hide();
			$('.loading[data-idcomment="' + comment_id + '"]').show();


			// setTimeout(function() {
				$.ajax({
					url: folder_name + 'manage-comment/comment-id:' + comment_id,
					method: 'GET',
					async: true,

					success: function(s) {
						var splitted = s.split('|');
						var name = splitted[0];
						var url = splitted[2];
						var comment = splitted[3];
						var no_ipaddress = (splitted[4] == 'no-ip' ? true : false);

						scrollto('.write-a-comment', 200, 10);
						$('.write-a-comment > h4').text((viewing_in_english == false ? 'Hantera en kommentar' : 'Manage a comment'));
						$('div#message').hide();

						$('form').attr({ 'data-idcomment': comment_id });
						$('input[name="field-name"]').val(name).attr({ 'disabled': (no_ipaddress == true ? 'disabled' : false) });
						$('input[name="field-url"]').val(url).attr({ 'disabled': (no_ipaddress == true ? 'disabled' : false) });
						$('textarea[name="field-comment"]').val(comment).attr({ 'disabled': (no_ipaddress == true ? 'disabled' : false) });
						$('input[name="button-showpopup"]').val((viewing_in_english == false ? 'Spara' : 'Save')).prop({
							'name': 'button-save'
						});

						if(no_ipaddress == true) {
							$('.no-ipaddress').show();
						}

						$('.answer').hide();
						$('.answer-inactive').show();

						$('input#pw').show();
						$('input#pw-readonly').hide();
						$('.checkboxes').show();
						$('a#cancel').show();
						$('.managing[data-idcomment="' + comment_id + '"]').show();
						$('.loading[data-idcomment="' + comment_id + '"]').hide();

						$('a#cancel').attr({ 'data-idcomment': comment_id });
						$('.item[data-idcomment="' + comment_id + '"]').addClass('highlight');
						$('input[name="hidden-idcomment"]').val('');
						$('input[name="button-save"]').attr({ 'disabled': false });

						$('a#reload').hide();
						$('div#reload-inactive').show();
					},

					error: function(e) {
						console.log(e);
					}
				});
			// }, 500);
		});


		// Cancel managing the comment
		$('body').on('click', 'a#cancel', function() {
			var comment_id = $(this).data('idcomment');

			$('.write-a-comment > h4').text((viewing_in_english == false ? 'Kommentera inlägget' : 'Comment this post'));

			$('input[name="field-name"]').val('');
			$('input[name="field-url"]').val('');
			$('textarea[name="field-comment"]').val('');
			$('input[name="button-save"]').val((viewing_in_english == false ? 'Skicka min kommentar' : 'Send my comment')).prop({
				'name': 'button-showpopup'
			});

			$('.answer').text((viewing_in_english == false ? 'Svara' : 'Answer'));

			$('.answer').show();
			$('.answer-inactive').hide();

			$('div#message').hide();
			$('.no-ipaddress').hide();

			$('input#pw').hide().val('');
			$('input#pw-readonly').show();
			$('.checkboxes').hide();
			$('a#cancel').hide();
			$('.managing').hide();
			$('.manage').show();

			$('.item').removeClass('highlight');

			$('a#reload').show();
			$('div#reload-inactive').hide();

			$('input#pw-readonly').val(random_password(20));

			window.history.pushState(null, null, folder_name + settings + 'read:' + $('form').data('idpost'));
		});


		// Go back to the form
		$('body').on('click', '.managing', function() {
			scrollto('.write-a-comment', 200, 10);
		});



		// Manage a comment
		$('body').on('click', 'input[name*="button"]:not([name="button-showpopup"])', function() {

			var form = new FormData($('form')[0]);
			var button_type = ($(this).attr('name') == 'button-send' ? 'comment-send/post:' + $('form').data('idpost') : 'comment-save/post:' + $('form').data('idpost') + '/comment:' + $('form').data('idcomment'));
			var button_type_short = ($(this).attr('name') == 'button-send' ? 'send' : 'save');

			// Text fields
			var field_name = $('input[name="field-name"]').val();
			var field_url = $('input[name="field-url"]').val();
			var field_comment = $('textarea[name="field-comment"]').val();
			var field_password = $('input#pw').val();
			var field_password_readonly = $('input#pw-readonly').val();
			var check_1 = ($('input#check-1').is(':checked') ? true : false);
			var check_2 = ($('input#check-2').is(':checked') ? true : false);

			if(!validator.isEmpty(field_name) || !validator.isEmpty(field_comment) || $('input#pw').is(':visible') && !validator.isEmpty(field_password)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_name) || validator.isEmpty(field_comment) || $('input#pw').is(':visible') && validator.isEmpty(field_password)) {
				if(validator.isEmpty(field_name)) {
					addto_console((viewing_in_english == false ? 'Fältet "Ditt namn" är tomt' : 'The field "Your name" are empty'), 'error');
				} else if(validator.isEmpty(field_password)) {
					addto_console((viewing_in_english == false ? 'Fältet "Kommentarens lösenord" är tomt' : 'The field "The password for this comment" are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');

				// Hide the popup window
				$('.dim').hide();
				$('.popup').hide();
				$('.message-popup').hide();
				$('input[id*="check"]').prop({ 'checked': false });
				$('input[name="button-send"]').removeClass('password-warning');
				$('input#pw').val('');


			} else if(!validator.isEmpty(field_url) && !validator.isURL(field_url, {
				protocols: ['https'],
				require_tld: true,
				require_protocol: true,
				require_host: true,
				require_port: false,
				require_valid_protocol: true
			})) {
				addto_console((viewing_in_english == false ? 'Webbadressen "' + field_url + '" är inte giltig' : 'The URL "' + field_url + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna webbadressen är inte giltig' : 'The entered URL are not valid'), 'error');

				// Hide the popup window
				$('.dim').hide();
				$('.popup').hide();
				$('.message-popup').hide();
				$('input[id*="check"]').prop({ 'checked': false });
				$('input[name="button-send"]').removeClass('password-warning');
				$('input#pw').val('');


			} else if($('.popup').is(':visible') && check_1 == false) {
				$('.message-popup').show().removeClass('color-green').addClass('color-red').text((viewing_in_english == false ? 'Vänligen godkänn datalagringen innan du fortsätter' : 'Please agree with the data logging before you can continue'));


			} else if($('.popup').is(':visible') && check_2 == false && !$('input[name="button-send"]').hasClass('password-warning')) {
				$('.message-popup').show().removeClass('color-green').addClass('color-red').text((viewing_in_english == false ? 'Om du inte sparar lösenordet, så kommer du inte kunna ändra eller ta bort din kommentar. Det här är din sista påminnelse!' : 'If you do not save the password, you will not be able to edit or delete your account. This is your last reminder!'));
				$('input[name="button-send"]').addClass('password-warning');


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + button_type,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + 'ajax/' + button_type); console.log(s); console.groupEnd(); }

						// Hide the popup window
						$('.dim').hide();
						$('.popup').hide();
						$('.message-popup').hide();
						$('input[id*="check"]').prop({ 'checked': false });
						$('input[name="button-send"]').removeClass('password-warning');


						// An internal error occured
						if(s.indexOf('on line') >= 0) {
							addto_console((viewing_in_english == false ? 'Ett internt fel inträffade' : 'An internal error occured'), 'error');
							view_message((viewing_in_english == false ? 'Kunde inte gå vidare. Vänligen försök igen. Om problemet återuppstår, kontakta bloggägaren och berätta för honom hur du gjorde, så att han kan återskapa problemet' : 'Were not able to continue. Please try again. If the error is persisting, please contact the blog owner and tell him how you did so he can re-produce the error.'), 'error');

							$('input#pw').val('');


						// Invalid password
						} else if(s == 'error-invalidpassword') {
							addto_console((viewing_in_english == false ? 'Det angivna lösenordet "' + field_password + '" för kommentaren visade sig vara fel' : 'The password "' + field_password + '" for the comment appeared to be wrong'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett fel lösenord' : 'You have entered wrong password'), 'error');

							$('input#pw').val('');


						// The URL are blacklisted
						} else if(s == 'error-urlblacklisted') {
							addto_console((viewing_in_english == false ? 'Den angivna webbadressen "' + field_url + '" är svartlistad' : 'The URL "' + field_url + '" are blacklisted'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett en webbadress som är svartlistad' : 'You have entered an URL that are blacklisted'), 'error');

							$('input#pw').val('');


						// No data errors occured
						} else {
							if(button_type_short == 'send') {
								view_message((viewing_in_english == false ? 'Tack för din kommentar' : 'Thank you for your comment'));
							} else if(button_type_short == 'save' && check_1 == false) {
								view_message((viewing_in_english == false ? 'Kommentaren har blivit ändrad' : 'The comment has been edited'));
							} else if(button_type_short == 'save' && check_1 == true) {
								view_message((viewing_in_english == false ? 'Kommentaren har blivit borttagen' : 'The comment has been deleted'));
							}

							// Load the comments
							loadcomments($('section#read').data('idpost'));

							if(debugging == false) {
								var comment_id = $(this).data('idcomment');

								$('.write-a-comment > h4').text((viewing_in_english == false ? 'Kommentera inlägget' : 'Comment this post'));

								$('input[name="field-name"]').val('');
								$('input[name="field-url"]').val('');
								$('textarea[name="field-comment"]').val('');
								$('input[name="button-save"]').val((viewing_in_english == false ? 'Skicka min kommentar' : 'Send my comment')).prop({
									'name': 'button-showpopup'
								});

								$('input#pw').hide().val('');
								$('input#pw-readonly').show();
								$('.checkboxes').hide();
								$('a#cancel').hide();
								$('.managing').hide();
								$('.manage').show();

								$('.item[data-idcomment="' + comment_id + '"]').removeClass('highlight');

								$('input#pw-readonly').val(random_password(20));
							}
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#search').is(':visible')) {

		// Do a search
		$('body').on('click', 'input[name="button-search"]', function() {

			var form = new FormData($('form')[0]);

			// Text fields
			var field_string = $('input[name="field-string"]').val();



			if(!validator.isEmpty(field_string)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_string)) {
				addto_console((viewing_in_english == false ? 'Fältet är tomt' : 'The text field are empty'), 'error');
				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');


			} else if(!validator.isLength(field_string, { min: 2 })) {
				addto_console((viewing_in_english == false ? 'Sökordet "' + field_string + '" är för kort' : 'The search string "' + field_string + '" are too short'), 'error');
				view_message((viewing_in_english == false ? 'Sökordet är för kort' : 'The entered string are too short'), 'error');


			// No errors occured
			} else {

				window.location = folder_name + (viewing_in_english == false ? '' : 'en/') + 'search:' + field_string;

			}

		});

	}







	if($('section#settings').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Save the settings
		$('body').on('click', 'input[name="button-save"]', function() {

			var form = new FormData($('form')[0]);

			// Text fields
			var field_name = $('input[name="field-name"]').val();
			var field_url = $('input[name="field-url"]').val();


			if(!validator.isEmpty(field_url) && !validator.isURL(field_url, {
				protocols: ['https'],
				require_tld: true,
				require_protocol: true,
				require_host: true,
				require_port: false,
				require_valid_protocol: true
			})) {
				addto_console((viewing_in_english == false ? 'Webbadressen "' + field_url + '" är inte giltig' : 'The URL "' + field_url + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna webbadressen är inte korrekt' : 'The entered URL are not valid'), 'error');


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + 'user:save-settings',
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + 'ajax/save-settings.php'); console.log(s); console.groupEnd(); }
						view_message((viewing_in_english == false ? 'Dina inställningar har blivit sparade' : 'Your settings has been saved'));

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#write').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}

		if(!$('input[name="button-save-new"]').is(':visible')) {
			$('input,textarea').on('input', function() {
				if($('input[name="button-save"]').is(':visible') == true || $('input[name="button-save-unpublish"]').is(':visible') == true) {
					$('input[name="button-save"]').val((viewing_in_english == false ? 'Spara' : 'Save'));
				}
			});
		}



		// Publish a post
		$('body').on('click', 'input[name*="button"]', function() {

			var hidden_idpost = $('input[name="hidden-postid"]').val();
			var form = new FormData($('form')[0]);
			var current_datetime = new Date();
			var current_time = (current_datetime.getHours() < 10 ? '0' : '') + current_datetime.getHours() + ':' + (current_datetime.getMinutes() < 10 ? '0' : '') + current_datetime.getMinutes() + ':' + (current_datetime.getSeconds() < 10 ? '0' : '') + current_datetime.getSeconds();

			// Text fields
			var field_subject = $('input[name="field-subject"]').val();
			var field_tags = $('input[name="field-tags"]').val();
			var field_publish_date = $('input[name="field-publish-date"]').val();
			var field_publish_hour = $('input[name="field-publish-hour"]').val();
			var field_cover = $('input[name="field-cover"]').val();
			var field_cover_owner = $('input[name="field-cover-owner"]').val();
			var field_cover_owner_url = $('input[name="field-cover-owner-url"]').val();
			var field_content = $('textarea[name="field-content"]').val();
			var check_3 = ($('input#check-3').is(':checked') ? true : false);


			if($(this).attr('name') == 'button-publish') {
				var button_type = 'publish';
			} else if(hidden_idpost != null && $(this).attr('name') == 'button-edit') {
				var button_type = 'edit:' + hidden_idpost;
			} else if(hidden_idpost != null && $(this).attr('name') == 'button-finish') {
				var button_type = 'finish:' + hidden_idpost;
			} else if(hidden_idpost == null && $(this).attr('name') == 'button-save-new') {
				var button_type = 'save';
			} else {
				var button_type = 'save:' + hidden_idpost;
			}


			if(!validator.isEmpty(field_subject) && !validator.isEmpty(field_content)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_subject) || validator.isEmpty(field_content) || validator.isEmpty(field_tags)) {
				if(validator.isEmpty(field_subject)) {
					addto_console((viewing_in_english == false ? 'Fältet "Rubrik" är tomt' : 'The field "Subject" are empty'), 'error');
				} else if(validator.isEmpty(field_content)) {
					addto_console((viewing_in_english == false ? 'Fältet "Innehåll" är tomt' : 'The field "Content" are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');


			} else if(!validator.isEmpty(field_publish_date) && !validator.isDate(field_publish_date, {
				format: 'YYYY-MM-DD',
				strictMode: true,
				delimiters: ['-']
			})) {
				addto_console((viewing_in_english == false ? 'Datumet "' + field_publish + '" är inte giltig' : 'The date "' + field_publish + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Det angivna publiceringsdatumet är inte korrekt' : 'The entered publish date are not valid'), 'error');


			/*
			} else if(!validator.isEmpty(field_publish_hour) && !validator.isDate(field_publish_hour, {
				format: 'YYYY-MM-DD',
				strictMode: true,
				delimiters: ['-']
			})) {
				addto_console((viewing_in_english == false ? 'Datumet "' + field_publish + '" är inte giltig' : 'The date "' + field_publish + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Det angivna publiceringsdatumet är inte korrekt' : 'The entered publish date are not valid'), 'error');
			*/


			} else if(!validator.isEmpty(field_cover) && !validator.isURL(field_cover, {
				protocols: ['https'],
				require_tld: true,
				require_protocol: true,
				require_host: true,
				require_port: false,
				require_valid_protocol: true
			})) {
				addto_console((viewing_in_english == false ? 'Webbadressen "' + field_cover + '" är inte giltig' : 'The URL "' + field_cover + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna webbadressen för omslaget är inte korrekt' : 'The entered URL for the cover are not valid'), 'error');


			} else if(!validator.isEmpty(field_cover_owner_url) && !validator.isURL(field_cover_owner_url, {
				protocols: ['https'],
				require_tld: true,
				require_protocol: true,
				require_host: true,
				require_port: false,
				require_valid_protocol: true
			})) {
				addto_console((viewing_in_english == false ? 'Webbadressen "' + field_cover + '" är inte giltig' : 'The URL "' + field_cover + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna webbadressen för ägarens profil på Unsplash är inte korrekt' : 'The entered URL for the owner\'s profile on Unsplash are not valid'), 'error');



			// No errors occured
			} else {

				if(button_type.indexOf('save') > -1) {
					$('input[name*="save"]').val((viewing_in_english == false ? 'Sparas...' : 'Saving...'));
				}


				$.ajax({
					url: folder_name + 'admin:post-' + button_type,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + 'admin:post-' + button_type); console.log(s); console.groupEnd(); }

						if(s == 'error-invalidpassword') {
							addto_console((viewing_in_english == false ? 'Det angivna lösenordet "' + field_password + '" för kommentaren visade sig vara fel' : 'The password "' + field_password + '" for the comment appeared to be wrong'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett fel lösenord för kommentaren' : 'You have entered wrong password for the comment'), 'error');


						// No data errors occured
						} else {
							if(button_type.includes('save') === true) {
								if($('input[name="button-save"]').is(':visible') == true) {
									var redirect_uri = null;
									$('input[name="button-save"]').val((viewing_in_english == false ? 'Sparades' : 'Saved') + ' ' + current_time);

								} else if($('input[name="button-save-new"]').is(':visible') == true) {
									var redirect_uri = (check_3 == false ? '' : 'en/') + 'edit:' + s;
									view_message((viewing_in_english == false ? 'Inlägget har blivit sparat. Laddar om sidan...' : 'The post has been saved. Reloading the page...'));

								} else if($('input[name="button-save-unpublish"]').is(':visible') == true) {
									var redirect_uri = (check_3 == false ? '' : 'en/') + 'edit:' + $('input[name="hidden-postid"]').val();
									view_message((viewing_in_english == false ? 'Inlägget har blivit avpublicerat och sparat. Laddar om sidan...' : 'The post has been unpublished and saved. Reloading the page...'));
								}


							} else if(button_type.includes('edit') === true) {
								var redirect_uri = (check_3 == false ? '' : 'en/') + 'read:' + $('input[name="hidden-postid"]').val();
								view_message((viewing_in_english == false ? 'Inlägget har blivit uppdaterat. Skickar dig vidare till inlägget...' : 'The post has been updated. Redirecting you to the post...'));


							} else if(button_type == 'publish' || button_type.includes('finish') === true) {
								var redirect_uri = (check_3 == false ? '' : 'en/') + 'read:' + s;
								view_message((viewing_in_english == false ? 'Inlägget publicerades' : 'The post has been published'));
							}


							if(redirect_uri != null) {
								if(debugging == false) {
									setTimeout(function() {
										window.location = folder_name + redirect_uri;
									}, 1000);

								} else {
									$('div#message').append((viewing_in_english == false ? '. <a href="' + folder_name + 'read:' + s + '" class="color-green">Klicka här för att fortsätta</a>' : '. <a href="' + folder_name + '" class="color-green">Click here to continue</a>'));
								}
							}
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#edit-post').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Send the edit
		$('body').on('click', 'input[name="button-send"]', function() {

			var form = new FormData($('form')[0]);

			// Text fields
			var field_content = $('textarea[name="field-content"]').val();


			if(!validator.isEmpty(field_content)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_content)) {
				addto_console((viewing_in_english == false ? 'Fältet "Innehåll" är tomt' : 'The field "Content" are empty'), 'error');
				view_message((viewing_in_english == false ? 'Vänligen lämna inte textfältet tomt' : 'Please do not leave the text field empty'), 'error');



			// No errors occured
			} else {

				$.ajax({
					url: folder_name + 'send-edit',
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + 'send-edit'); console.log(s); console.groupEnd(); }

						if(s == 'error-samecontent') {
							addto_console((viewing_in_english == false ? 'Det gjordes inga ändringar' : 'There\'s no edits done'), 'error');
							view_message((viewing_in_english == false ? 'Du har inte gjort några ändringar än' : 'You haven\'t done any changes'), 'error');


						// No data errors occured
						} else {
							view_message((viewing_in_english == false ? 'Tack för din medverkan' : 'Thank you for your contribution'));
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#login').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Login
		$('body').on('click', 'input[name="button-login"]', function() {

			// Text fields
			var field_username = $('input[name="field-username"]').val();
			var field_password = $('input[name="field-password"]').val();
			var field_totp = $('input[name="field-totp"]').val();

			var form = new FormData($('form')[0]);
			var pagename = 'admin:login';

			$('input[name="field-password"]').val('');


			if(!validator.isEmpty(field_username) && $('input[name="field-password"]').is(':visible') && !validator.isEmpty(field_password)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_username) || $('input[name="field-password"]').is(':visible') && validator.isEmpty(field_password) || $('input[name="field-totp"]').is(':visible') && validator.isEmpty(field_totp)) {
				if(validator.isEmpty(field_username)) {
					addto_console((viewing_in_english == false ? 'Fältet "Användarnamn" är tomt' : 'The field "Username" are empty'), 'error');
				} else if($('input[name="field-password"]').is(':visible') && validator.isEmpty(field_password)) {
					addto_console((viewing_in_english == false ? 'Fältet "Lösenord" är tomt' : 'The field "Password" are empty'), 'error');
				} else if(!$('input[name="field-password"]').is(':visible') && validator.isEmpty(field_totp)) {
					addto_console((viewing_in_english == false ? 'Fältet "Engångskod" är tomt' : 'The field "One-time password" are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + pagename,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + pagename); console.log(s); console.groupEnd(); }

						if(s == 'error-usernotfound') {
							addto_console((viewing_in_english == false ? 'Det angivna användarnamnet "' + field_username + '" kunde inte hittas' : 'The username "' + field_username + '" couldn\'t be found'), 'error');
							view_message((viewing_in_english == false ? 'Kunde inte hitta det användarnamnet' : 'Couldn\'t find that username'), 'error');


						} else if($('input[name="field-password"]').is(':visible') && s == 'error-invalidpassword') {
							addto_console((viewing_in_english == false ? 'Det angivna lösenordet "' + field_password + '" visade sig vara fel' : 'The password "' + field_password + '" appeared to be wrong'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett fel lösenord' : 'You have entered wrong password'), 'error');


						} else if($('input[name="field-totp"]').is(':visible') && !validator.isNumeric(field_totp, {no_symbols: true})) {
							addto_console((viewing_in_english == false ? 'Engångskoden "' + field_totp + '" är inte giltig' : 'The one-time password "' + field_totp + '" were not validated'), 'error');
							view_message((viewing_in_english == false ? 'Engångskoden får endast innehålla siffror' : 'The one-time password must only be in digits'), 'error');


						} else if($('input[name="field-totp"]').is(':visible') && s == 'error-invalidtotp') {
							addto_console((viewing_in_english == false ? 'Den angivna engångskoden "' + field_totp + '" visade sig vara fel' : 'The one-time password "' + field_totp + '" appeared to be wrong'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett fel engångskod' : 'You have entered wrong one-time password'), 'error');


						// No data errors occured
						} else {
							if(debugging == false) {
								view_message((viewing_in_english == false ? 'Du har loggats in' : 'You have been logged in'));
								setTimeout(function() {
									window.location = folder_name + ($('main').attr('data-language') == 'en' ? 'en' : '');
								}, 500);


							} else {
								view_message((viewing_in_english == false ? 'Du har loggats in. <a href="' + folder_name + '" class="color-green">Klicka här för att fortsätta</a>' : 'You have been logged in. <a href="' + folder_name + '" class="color-green">Click here to continue</a>'));
							}
						}


						$('input[name="field-totp"]').val('');

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#restore').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Restore
		$('body').on('click', 'input[name="button-restore"]', function() {

			// Text fields
			var field_email = $('input[name="field-email"]').val();

			var form = new FormData($('form')[0]);
			var pagename = 'admin:restore';


			if(!validator.isEmpty(field_email)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_email)) {
				addto_console((viewing_in_english == false ? 'Fältet "E-postadress" är tomt' : 'The field "Email address" are empty'), 'error');
				view_message((viewing_in_english == false ? 'Vänligen fyll i din e-postadress först' : 'Please enter your email address first'), 'error');


			// No errors occured
			} else if(!validator.isEmail(field_email)) {
				addto_console((viewing_in_english == false ? 'E-postadressen "' + field_email + '" är inte giltig' : 'The email address "' + field_email + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna e-postadressen är inte korrekt' : 'The entered email address are not valid'), 'error');


			// No errors occured
			} else {

				view_message((viewing_in_english == false ? 'Skickar instruktioner till e-postadressen...' : 'Sending instructions to the email address...'), 'info');

				$.ajax({
					url: folder_name + pagename,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + pagename); console.log(s); console.groupEnd(); }

						if(s == 'error-usernotfound') {
							addto_console((viewing_in_english == false ? 'E-postadressen "' + field_email + '" existerar inte' : 'The email address "' + field_email + '" does not exists'), 'error');
							view_message((viewing_in_english == false ? 'Kunde inte hitta något konto som är knutet till den e-postadressen' : 'Couldn\'t find that username'), 'error');


						// No data errors occured
						} else {
							view_message((viewing_in_english == false ? 'Var god bekräfta återställningen i e-postmeddelandet' : 'Please confirm the restore in the email we\'ve sent to you'));
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#config').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}



		// Setup TOTP
		$('body').on('click', 'input[name="button-verify"]', function() {

			// Text fields
			var field_totp = $('input[name="field-totp"]').val();
			var field_username = $('input[name="field-username"]').val();
			var field_email = $('input[name="field-email"]').val();

			var form = new FormData($('form')[0]);
			var pagename = 'settings:setup-totp/go';

			$('input[name="field-totp"]').val('');


			if(!validator.isEmpty(field_totp) && !validator.isEmpty(field_username) && !validator.isEmpty(field_email)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}


			if(validator.isEmpty(field_totp) || validator.isEmpty(field_email)) {
				if(validator.isEmpty(field_totp) && !validator.isEmpty(field_email)) {
					addto_console((viewing_in_english == false ? 'Fältet "Vad är engångskoden?" är tomt' : 'The field "What is the one-time password?" are empty'), 'error');
				} else if(!validator.isEmpty(field_totp) && validator.isEmpty(field_email)) {
					addto_console((viewing_in_english == false ? 'Fältet "Din e-postadress" är tomt' : 'The field "Your email address" are empty'), 'error');
				} else if(validator.isEmpty(field_totp) && validator.isEmpty(field_email)) {
					addto_console((viewing_in_english == false ? 'Alla textfält som krävs är inte ifyllda' : 'All the required text fields are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');


			} else if(!validator.isEmpty(field_totp) && !validator.isNumeric(field_totp, {no_symbols: true})) {
				addto_console((viewing_in_english == false ? 'Engångskoden "' + field_totp + '" är inte giltig' : 'The one-time password "' + field_totp + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Engångskoden får endast innehålla siffror' : 'The one-time password must only be in digits'), 'error');


			} else if(!validator.isEmpty(field_email) && !validator.isEmail(field_email)) {
				addto_console((viewing_in_english == false ? 'E-postadressen "' + field_email + '" är inte giltig' : 'The email address "' + field_email + '" were not validated'), 'error');
				view_message((viewing_in_english == false ? 'Den angivna e-postadressen är inte korrekt' : 'The entered email address are not valid'), 'error');


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + pagename,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + pagename); console.log(s); console.groupEnd(); }

						if(s == 'error-invalidtotp') {
							addto_console((viewing_in_english == false ? 'Den angivna engångskoden "' + field_totp + '" visade sig vara fel' : 'The one-time password "' + field_totp + '" appeared to be wrong'), 'error');
							view_message((viewing_in_english == false ? 'Du har angett fel engångskod' : 'You have entered wrong one-time password'), 'error');


						// No data errors occured
						} else {
							view_message((viewing_in_english == false ? 'Ditt konto har blivit uppdaterat' : 'Your account has been updated'));

							setTimeout(function() {
								window.location = folder_name;
							}, 500);
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}







	if($('section#files').is(':visible')) {

		if($('form').is(':visible')) {
			$('form').attr({
				'novalidate': 'novalidate'
			});
		}


		var clipboard = new ClipboardJS('a#copy');
		clipboard.on('success', function(e) {
			alert((viewing_in_english == false ? 'Kopierad.' : 'Copied.'));
			e.clearSelection();
		});

		clipboard.on('error', function(e) {
			alert((viewing_in_english == false ? 'Något gick fel. Försök igen.' : 'Something went wrong. Try again.'));
		});



		$('input[name="file"]').on('change', function(evt) {
			file_size = convert_filesize(this.files[0].size);
			var file_name = $('input[type="file"]')[0].files[0].name;
			var file_info = file_name.split('.');

			$('input[name="field-name"]').val(file_info[0]);
			$('.extension').text(file_info[1]);
		});


		// Upload the file
		$('body').on('click', 'input[name="button-upload"]', function() {

			// Text fields
			var field_file = $('input[name="file"]').val();
			var list_files = $('input[name="file"]').prop('files');
			var field_name = $('input[name="field-name"]').val();
			var field_alt = $('input[name="field-alt"]').val();

			var form = new FormData($('form')[0]);
			var pagename = 'admin:upload-file';

			if(debugging == false) {
				$('input[name="button-upload"]').val((viewing_in_english == false ? 'Laddar upp...' : 'Uploading...')).attr('disabled', true);
			}

			if(!validator.isEmpty(field_file) && !validator.isEmpty(field_name) && !validator.isEmpty(field_alt)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}



			if(validator.isEmpty(field_file) || validator.isEmpty(field_name) || validator.isEmpty(field_alt)) {
				if(validator.isEmpty(field_file)) {
					addto_console((viewing_in_english == false ? 'Ingen fil har valts' : 'No file has been chosen'), 'error');
				} else if(validator.isEmpty(field_name)) {
					addto_console((viewing_in_english == false ? 'Fältet "Namn" är tomt' : 'The field "Name" are empty'), 'error');
				} else if(validator.isEmpty(field_alt)) {
					addto_console((viewing_in_english == false ? 'Fältet "Alternativ text" är tomt' : 'The field "Alternative text" are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');
				$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + pagename,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + pagename); console.log(s); console.groupEnd(); }

						if(s == 'error-upload-1') {
							addto_console((viewing_in_english == false ? 'Filens storlek (' + file_size + ') överskrider upload_max_filesize()-direktiven' : 'The file size (' + file_size + ') exceeds the upload_max_filesize() directive'), 'error');
							view_message((viewing_in_english == false ? 'Ett internt fel har inträffat' : 'An internal error has occured') + ' (1)', 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						} else if(s == 'error-upload-2') {
							addto_console((viewing_in_english == false ? 'Filens storlek är för stor' : 'The filesize are too big'), 'error');
							view_message((viewing_in_english == false ? 'Filens storlek är för stor' : 'The filesize are too big'), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						} else if(s == 'error-file-exists') {
							addto_console((viewing_in_english == false ? 'Filen har redan laddats upp' : 'The file has already been uploaded'), 'error');
							view_message((viewing_in_english == false ? 'Du har redan laddat upp den här filen' : 'You have already uploaded this file'), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						} else if(s == 'error-filenotuploaded') {
							addto_console((viewing_in_english == false ? 'Kunde inte ladda upp filen' : 'Were not able to upload the file'), 'error');
							view_message((viewing_in_english == false ? 'Filen har inte laddats upp. Vänligen försök igen' : 'The file were not uploaded. Please try again'), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						} else if(s == 'error-filenotuploaded-small') {
							addto_console((viewing_in_english == false ? 'Kunde inte ladda upp filen' : 'Were not able to upload the file'), 'error');
							view_message((viewing_in_english == false ? 'Kunde inte ladda upp filen till /small. Vänligen försök igen' : 'The file were not uploaded. Please try again'), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						} else if(s == 'error-filenotuploaded-large') {
							addto_console((viewing_in_english == false ? 'Kunde inte ladda upp filen' : 'Were not able to upload the file'), 'error');
							view_message((viewing_in_english == false ? 'Kunde inte ladda upp filen till /large' : 'The file were not uploaded. Please try again'), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload')).attr('disabled', false);


						// No data errors occured
						} else {
							addto_console((viewing_in_english == false ? 'Laddade upp ' + s : 'Uploaded the file ' + s), 'error');
							$('input[name="button-upload"]').val((viewing_in_english == false ? 'Ladda upp' : 'Upload'));

							if(debugging == false) {
								view_message((viewing_in_english == false ? 'Filen har blivit uppladdad. Laddar om sidan...' : 'The file has been uploaded. Reloading the page...'));
								setTimeout(function() {
									window.location = folder_name + 'files';
								}, 2000);


							} else {
								view_message((viewing_in_english == false ? 'Filen har laddats upp. <a href="' + folder_name + 'files" class="color-green">Klicka här för att fortsätta</a>' : 'The file has been uploaded. <a href="' + folder_name + 'files" class="color-green">Click here to continue</a>'));
							}
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});


		// Save the changes
		$('body').on('click', 'input[name="button-save"]', function() {

			// Text fields
			var id_file = $('input[name="hidden-idfile"]').val();
			var field_name = $('input[name="field-name"]').val();
			var field_alt = $('input[name="field-alt"]').val();

			var form = new FormData($('form')[0]);
			var pagename = 'admin/edit-file:' + id_file;

			if(debugging == false) {
				$('input[name="button-upload"]').val((viewing_in_english == false ? 'Sparar...' : 'Saving...')).attr('disabled', true);
			}

			if(!validator.isEmpty(field_name) && !validator.isEmpty(field_alt)) {
				addto_console((viewing_in_english == false ? 'Alla textfält som krävs är ifyllda' : 'All required text fields are filled'), 'info');
			}



			if(validator.isEmpty(field_name) || validator.isEmpty(field_alt)) {
				if(validator.isEmpty(field_name)) {
					addto_console((viewing_in_english == false ? 'Fältet "Namn" är tomt' : 'The field "Name" are empty'), 'error');
				} else if(validator.isEmpty(field_alt)) {
					addto_console((viewing_in_english == false ? 'Fältet "Alternativ text" är tomt' : 'The field "Alternative text" are empty'), 'error');
				}

				view_message((viewing_in_english == false ? 'Vänligen fyll i alla textfält som krävs' : 'Please fill all text fields that are required'), 'error');


			// No errors occured
			} else {

				$.ajax({
					url: folder_name + pagename,
					method: 'POST',
					async: true,
					success: function(s) {

						if(debugging == true) { console.group((viewing_in_english == false ? 'Innehållet från' : 'The content from') + ' ' + folder_name + pagename); console.log(s); console.groupEnd(); }

						if(s == 'error-file-exists') {
							addto_console((viewing_in_english == false ? 'En fil med filnamnet "' + field_name + '" finns redan' : 'A file with the filename "' + field_name + '" do already exists'), 'error');
							view_message((viewing_in_english == false ? 'Det finns redan en fil med det filnamnet. Vänligen välj ett annat' : 'That filename are already taken. Please choose another'), 'error');


						// No data errors occured
						} else {
							addto_console((viewing_in_english == false ? 'Ändrade' : 'Edited') + ' "' + field_name + '"', 'info');

							if(debugging == false) {
								view_message((viewing_in_english == false ? 'Dina ändringar har blivit sparade. Går tillbaka till alla filer...' : 'Your changes has been saved. Goes back to all the files...'));
								setTimeout(function() {
									window.location = folder_name + 'files';
								}, 2000);


							} else {
								view_message((viewing_in_english == false ? 'Dina ändringar har blivit sparade. <a href="' + folder_name + 'files" class="color-green">Klicka här för att fortsätta</a>' : 'Your changes has been saved. <a href="' + folder_name + 'files" class="color-green">Click here to continue</a>'));
							}
						}

					},

					data: form,
					cache: false,
					contentType: false,
					processData: false
				});

			}

		});

	}


});





function view_message(string, type) {
	$('div#message').text('').removeClass('color-green').removeClass('color-blue').removeClass('color-red').show().html(string).addClass('color-' + (type == 'error' ? 'red' : (type == 'info' ? 'blue' : 'green')));
	scrollto('div#message', 200, 10);
}

function addto_console(string, type) {
	if(debugging == true) {
		if(type == 'error') {
			console.error(string);
		} else if(type == 'info') {
			console.info(string);
		} else if(type == 'debug') {
			console.debug(string);
		} else if(type == 'warn') {
			console.warn(string);
		} else {
			console.log(string);
		}
	}
}

function scrollto(element, speed, offset) {
	var is_mobile = window.matchMedia("only screen and (max-width: 1030px)").matches;
	$('html,body').stop().animate({
		scrollTop: ($(element).offset().top - (is_mobile ? (offset + 50) : offset))
	}, speed);
}

function convert_filesize(size) {
	var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
	return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

function loadcomments(id_post, id_comment = null) {
	$.ajax({
		url: folder_name + (viewing_in_english == false ? '' : 'en/') + 'list-comments/post-id:' + id_post,
		method: 'GET',
		async: true,

		success: function(s) {
			$('section#read > .list-comments > .message').hide();
			$('section#read > .list-comments > .comments').show().html(s);

			// Go to all comments
			if(url.indexOf('comments') > -1) {
				scrollto('.list-comments', 200, 0);
			}

			// Go to comment
			if(get_hash !== 'undefined' && validator.isInt(get_hash)) {
				scrollto('.item[data-idcomment="' + get_hash + '"]', 200, 5);
				$('.item[data-idcomment="' + get_hash + '"]').addClass('highlight');
			}

			// Go to a comment
			$('body').on('click', '.link-id', function() {
				var id_comment = $(this).data('idcomment');
				window.history.pushState(null, null, folder_name + settings + (viewing_in_english == false ? '' : 'en/') + 'read:' + $('form').data('idpost') + '#' + id_comment);
				scrollto('.item[data-idcomment="' + id_comment + '"]', 200, 15);
				$('.item').removeClass('highlight');
				$('.item[data-idcomment="' + id_comment + '"]').addClass('highlight');
			});
		},

		error: function(e) {
			console.log(e);
		}
	});
}

function random_password(digits) {
	var chars = ["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz","0123456789", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"];
	return [digits].map(function(len, i) { return Array(len).fill(chars[i]).map(function(x) { return x[Math.floor(Math.random() * x.length)] }).join('') }).concat().join('').split('').sort(function(){return 0.5-Math.random()}).join('');
}



/*!
	autosize 5.0.0
	license: MIT
	http://www.jacklmoore.com/autosize
*/
!function(e,t){"function"==typeof define&&define.amd?define(["module","exports"],t):"undefined"!=typeof exports?t(module,exports):(t(t={exports:{}},t.exports),e.autosize=t.exports)}(this,function(e,t){"use strict";var n,o,a="function"==typeof Map?new Map:(n=[],o=[],{has:function(e){return-1<n.indexOf(e)},get:function(e){return o[n.indexOf(e)]},set:function(e,t){-1===n.indexOf(e)&&(n.push(e),o.push(t))},delete:function(e){e=n.indexOf(e);-1<e&&(n.splice(e,1),o.splice(e,1))}}),p=function(e){return new Event(e,{bubbles:!0})};try{new Event("test")}catch(e){p=function(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!1),t}}function r(o){var n,r,i,e,d,t;function l(e){var t=o.style.width;o.style.width="0px",o.offsetWidth,o.style.width=t,o.style.overflowY=e}function s(){var e,t;0!==o.scrollHeight&&(e=function(e){for(var t=[];e&&e.parentNode&&e.parentNode instanceof Element;)e.parentNode.scrollTop&&t.push({node:e.parentNode,scrollTop:e.parentNode.scrollTop}),e=e.parentNode;return t}(o),t=document.documentElement&&document.documentElement.scrollTop,o.style.height="",o.style.height=o.scrollHeight+n+"px",r=o.clientWidth,e.forEach(function(e){e.node.scrollTop=e.scrollTop}),t&&(document.documentElement.scrollTop=t))}function u(){s();var e=Math.round(parseFloat(o.style.height)),t=window.getComputedStyle(o,null),n="content-box"===t.boxSizing?Math.round(parseFloat(t.height)):o.offsetHeight;if(n<e?"hidden"===t.overflowY&&(l("scroll"),s(),n="content-box"===t.boxSizing?Math.round(parseFloat(window.getComputedStyle(o,null).height)):o.offsetHeight):"hidden"!==t.overflowY&&(l("hidden"),s(),n="content-box"===t.boxSizing?Math.round(parseFloat(window.getComputedStyle(o,null).height)):o.offsetHeight),i!==n){i=n;n=p("autosize:resized");try{o.dispatchEvent(n)}catch(e){}}}o&&o.nodeName&&"TEXTAREA"===o.nodeName&&!a.has(o)&&(i=r=n=null,e=function(){o.clientWidth!==r&&u()},d=function(t){window.removeEventListener("resize",e,!1),o.removeEventListener("input",u,!1),o.removeEventListener("keyup",u,!1),o.removeEventListener("autosize:destroy",d,!1),o.removeEventListener("autosize:update",u,!1),Object.keys(t).forEach(function(e){o.style[e]=t[e]}),a.delete(o)}.bind(o,{height:o.style.height,resize:o.style.resize,overflowY:o.style.overflowY,overflowX:o.style.overflowX,wordWrap:o.style.wordWrap}),o.addEventListener("autosize:destroy",d,!1),"onpropertychange"in o&&"oninput"in o&&o.addEventListener("keyup",u,!1),window.addEventListener("resize",e,!1),o.addEventListener("input",u,!1),o.addEventListener("autosize:update",u,!1),o.style.overflowX="hidden",o.style.wordWrap="break-word",a.set(o,{destroy:d,update:u}),"vertical"===(t=window.getComputedStyle(o,null)).resize?o.style.resize="none":"both"===t.resize&&(o.style.resize="horizontal"),n="content-box"===t.boxSizing?-(parseFloat(t.paddingTop)+parseFloat(t.paddingBottom)):parseFloat(t.borderTopWidth)+parseFloat(t.borderBottomWidth),isNaN(n)&&(n=0),u())}function i(e){e=a.get(e);e&&e.destroy()}function d(e){e=a.get(e);e&&e.update()}var l=null;"undefined"==typeof window||"function"!=typeof window.getComputedStyle?((l=function(e){return e}).destroy=function(e){return e},l.update=function(e){return e}):((l=function(e,t){return e&&Array.prototype.forEach.call(e.length?e:[e],r),e}).destroy=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],i),e},l.update=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],d),e}),t.default=l,e.exports=t.default});
