<?php

	use DiffMatchPatch\DiffMatchPatch;


	if(isset($_GET['opt'])) {

		require_once 'site-settings.php';

		$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
		$sent = strip_tags(htmlspecialchars($_GET['tms']));


		if($session == true) {
			$option = strip_tags(htmlspecialchars($_GET['opt']));

			$post = sql("SELECT *
						 FROM posts
						 WHERE id_unique = :_idunique
						", Array(
							'_idunique' => $id_unique
						), 'fetch');

			$edit = sql("SELECT *
						 FROM public_edits
						 WHERE id_post = :_idpost
						 AND timestamp_sent = :_sent
						", Array(
							'_idpost' => (int)$post['id'],
							'_sent' => $sent
						), 'fetch');


			if($option == 'accept') {
				$array_content = explode('----', $edit['content']);
				$content_before = trim($array_content[0]);
				$content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);
				$array_content = explode('----', $form_content);
				$content = trim($form_content);
				$content_before = trim($array_content[0]);
				$content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);

				sql("UPDATE posts
					 SET content_beforebreak = :_content_before,
						 content_afterbreak = :_content_after,
						 timestamp_edited = :_edited

					 WHERE id_unique = :_idunique
					", Array(
						'_idunique' => $id_unique,
						'_content_before' => (strpos($edit['content'], '----') !== false ? $content_before : $edit['content']),
						'_content_after' => (strpos($edit['content'], '----') !== false ? (empty($content_after) ? null : $content_after) : null),
						'_edited' => $edit['timestamp_sent']
					));

				/*
				sql("INSERT INTO posts(
						 id_post,
						 subject,
						 cover_hash,
						 cover_url,
						 cover_owner,
						 cover_owner_url,
						 content_beforebreak,
						 content_afterbreak,
						 hasbeencorrected,
						 timestamp_saved,
						 timestamp_published,
						 timestamp_edited,
						 is_pinned,
						 is_inenglish
					 )

					 VALUES(
						 :_idpost,
						 :_subject,
						 :_coverhash,
						 :_coverurl,
						 :_coverowner,
						 :_coverowner_url,
						 :_content_before,
						 :_content_after,
						 :_hasbeencorrected,
						 :_saved,
						 :_published,
						 :_edited,
						 :_ispinned,
						 :_is_inenglish
					 )
					", Array(
						'_idpost' => (int)$post['id'],
						'_subject' => $post['subject'],
						'_coverhash' => (empty($post['cover_hash']) ? null : $post['cover_hash']),
						'_coverurl' => (empty($post['cover_url']) ? null : $post['cover_url']),
						'_coverowner' => (empty($post['cover_owner']) ? null : $post['cover_owner']),
						'_coverowner_url' => (empty($post['cover_owner_url']) ? null : $post['cover_owner_url']),
						'_content_before' => (strpos($edit['content'], '----') !== false ? $content_before : $edit['content']),
						'_content_after' => (strpos($edit['content'], '----') !== false ? (empty($content_after) ? null : $content_after) : null),
						'_hasbeencorrected' => $post['hasbeencorrected'],
						'_saved' => null,
						'_published' => $post['timestamp_published'],
						'_edited' => $edit['timestamp_sent'],
						'_ispinned' => $post['is_pinned'],
						'_is_inenglish' => $post['is_inenglish']
					), 'insert');
				*/

				sql("DELETE FROM public_edits
					 WHERE id = :_idedit
					", Array(
						'_idedit' => (int)$edit['id']
					));


			} else {
				sql("DELETE FROM public_edits
					 WHERE id = :_idedit
					", Array(
						'_idedit' => (int)$edit['id']
					));
			}
		}

		header("Location: ".url('diff:'.$id_unique.'/'.$sent));
		exit;



	} else {

		require_once 'site-settings.php';

		$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
		$sent = (isset($_GET['tms']) ? strip_tags(htmlspecialchars($_GET['tms'])) : null);

		$c_post = sql("SELECT COUNT(id)
					   FROM posts
					   WHERE id_unique = :_idunique
					  ", Array(
						  '_idunique' => $id_unique
					  ), 'count');


		if($c_post != 0) {
			$post = sql("SELECT *
						 FROM posts
						 WHERE id_unique = :_idunique
						", Array(
							'_idunique' => $id_unique
						), 'fetch');


			if($sent == null) {
				$get_edits = sql("SELECT *
								  FROM posts
								  WHERE id_post = :_idpost
								  ORDER BY timestamp_edited DESC
								 ", Array(
									 '_idpost' => (int)$post['id']
								 ));

			} else {
				$c_edit = sql("SELECT COUNT(id)
							   FROM public_edits
							   WHERE id_post = :_idpost
							   AND timestamp_sent = :_sent
							  ", Array(
								  '_idpost' => (int)$post['id'],
								  '_sent' => $sent
							  ), 'count');

				if($c_edit != 0) {
					$edit = sql("SELECT *
								 FROM public_edits
								 WHERE id_post = :_idpost
								 AND timestamp_sent = :_sent
								", Array(
									'_idpost' => (int)$post['id'],
									'_sent' => $sent
								), 'fetch');

				} else {
					header("Location: ".url('read:'.$id_unique));
					exit;
				}
			}
		}

		$c_removed = 0;
		$c_added = 0;
		$dmp = new DiffMatchPatch();



		if($c_post != 0 OR $c_edit != 0) {

			require_once 'site-header.php';







			echo '<section id="post-diff">';
				if($sent == null) {
					echo '<h2>';
						echo '<a href="'.url('read:'.$id_unique).'">';
							echo $post['subject'];
						echo '</a>';
					echo '</h2>';


					$c_posts = 0;
					foreach($get_edits AS $edit) {
						$c_posts++;
						$content_original = $post['content_beforebreak'] . $post['content_afterbreak'];
						$content_edited = $edit['content_beforebreak'] . $edit['content_afterbreak'];
						# $array_content = explode('----', $content);
						# $content_before = trim($array_content[0]);
						# $content_before = (strpos($edit['content_beforebreak'], '----') !== false ? $content_before : $content);
						# $content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);
						# $content_after = (strpos($edit['content_beforebreak'], '----') !== false ? (empty($content_after) ? null : $content_after) : null);

						$diffs = $dmp->diff_main($content_original, $content_edited, false);


						echo '<details'.($c_posts == 1 ? ' open=""' : '').'>';
							echo '<summary>'.date_(($sent == null ? $edit['timestamp_edited'] : $edit['timestamp_sent']), 'short').'</summary>';

							echo '<div class="content">';
								foreach($diffs AS $diff) {
									if($diff[0] == -1) {
										$c_removed++;
										echo '<div class="removed">'.nl2br($diff[1]).'</div>';

									} elseif($diff[0] == 1) {
										$c_added++;
										echo '<div class="added">'.nl2br($diff[1]).'</div>';

									} else {
										echo nl2br($diff[1]);
									}
								}
							echo '</div>';


							echo '<div class="statistics small-text">';
								echo '<div class="color-red">';
									echo '<b>'.$c_removed.'</b> '.($viewing_in_english == false ? ($c_removed == 1 ? 'borttagen' : 'borttagna') : 'removed');
								echo '</div>';

								echo '<div class="color-green">';
									echo '<b>'.$c_added.'</b> '.($viewing_in_english == false ? ($c_added == 1 ? 'tillagd' : 'tillagda') : 'added');
								echo '</div>';
							echo '</div>';
						echo '</details>';
					}


				} else {
					$content_original = $post['content_beforebreak']."\n----\n".$post['content_afterbreak'];
					$content_edited = $edit['content'];
					$array_content = explode('----', $content_edited);
					$content_before = trim($array_content[0]);
					# $content_before = (strpos($content_edited, '----') !== false ? $content_before : $content_edited);
					$content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);
					# $content_after = (strpos($content_edited, '----') !== false ? (empty($content_after) ? null : $content_after) : null); */

					$diffs = $dmp->diff_main($content_original, $content_before."\n----\n".$content_after, false);


					echo '<h1>'.date_(($sent == null ? $edit['timestamp_edited'] : $edit['timestamp_sent']), 'short').'</h1>';

					echo '<div class="content">';
						foreach($diffs AS $diff) {
							# echo '<pre>'; var_dump($diff); echo '</pre>';
							if($diff[0] == -1) {
								$c_removed++;
								echo '<span class="removed">'.nl2br($diff[1]).'</span>';

							} elseif($diff[0] == 1) {
								$c_added++;
								echo '<span class="added">'.nl2br($diff[1]).'</span>';

							} else {
								echo nl2br($diff[1]);
							}
						}
					echo '</div>';


					echo '<div class="statistics small-text">';
						echo '<div class="color-red">';
							echo '<b>'.$c_removed.'</b> '.($viewing_in_english == false ? ($c_removed == 1 ? 'borttagen' : 'borttagna') : 'removed');
						echo '</div>';

						echo '<div class="color-green">';
							echo '<b>'.$c_added.'</b> '.($viewing_in_english == false ? ($c_added == 1 ? 'tillagd' : 'tillagda') : 'added');
						echo '</div>';
					echo '</div>';
				}


				if($sent != null) {
					echo '<div class="options small-text">';
						echo '<a href="'.url('read:'.$post['id_unique']).'">';
							echo 'Gå tillbaka';
						echo '</a>';


						if($session == true) {
							echo '<a href="'.url('diff:'.$post['id_unique'].'/'.$edit['timestamp_sent'].'/accept').'" id="accept">';
								echo 'Godkänn';
							echo '</a>';

							echo '<a href="'.url('diff:'.$post['id_unique'].'/'.$edit['timestamp_sent'].'/deny').'" id="deny">';
								echo 'Neka';
							echo '</a>';
						}
					echo '</div>';
				}
			echo '</section>';







			require_once 'site-footer.php';

		}

	}

?>
