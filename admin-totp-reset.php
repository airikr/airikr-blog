<?php

	if(isset($_POST['button-login'])) {
		require_once 'site-settings.php';

		$form_totp = strip_tags(htmlspecialchars($_POST['field-totp']));
		$field_username = strip_tags(htmlspecialchars($_POST['field-username']));

		$check_user = sql("SELECT COUNT(id)
						   FROM users
  						   WHERE username = :_username
  						  ", Array(
  							  '_username' => trim($field_username)
						  ), 'count');

		if($check_user != 0) {
			$user = sql("SELECT *
						 FROM users
						 WHERE username = :_username
						", Array(
							'_username' => trim($field_username)
						), 'fetch');
		}



		if($check_user == 0) {
			if($form_totp == '000000') {
				$_SESSION['config'] = true;
				header("Location: ".url('config-totp'));
				exit;
			}


		} else {
			$ga = new PHPGangsta_GoogleAuthenticator();
			$result = $ga->verifyCode($user['twofactorcode'], $form_totp, 2);

			if($result) {
				$_SESSION['loggedin'] = (int)$user['id'];

				sql("UPDATE users
					 SET timestamp_lastlogin = :_lastlogin
					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$user['id'],
						'_lastlogin' => time()
					));

				header("Location: ".url(''));
				exit;

			} else {
				header("Location: ".url('login'));
				exit;
			}
		}


		header("Location: ".url('login'));
		exit;







	} else {

		require_once 'site-header.php';







		echo '<section id="restore">';
			echo '<div class="icon">'.svgicon('restore').'</div>';

			echo '<div id="message"></div>';
			echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate="novalidate">';

				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'E-postadress' : 'Email address');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="email" name="field-email" tabindex="1" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" required>';
					echo '</div>';
				echo '</div>';

				echo '<div class="button">';
					echo '<input type="submit" name="button-restore" tabindex="2" value="'.($viewing_in_english == false ? 'Återställ' : 'Restore').'">';

					echo '<a href="'.url('login').'" class="color-red small-text">';
						echo ($viewing_in_english == false ? 'Avbryt' : 'Cancel');
					echo '</a>';
				echo '</div>';

			echo '</form>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
