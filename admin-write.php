<?php

	if(isset($_GET['del'])) {

		require_once 'site-settings.php';

		$id_unique = strip_tags(htmlspecialchars($_GET['del']));
		$post['cover_hash'] = (isset($_GET['cov']) ? strip_tags(htmlspecialchars($_GET['cov'])) : null);

		sql("DELETE FROM posts
			 WHERE id_unique = :_idunique
			", Array(
				'_idunique' => $id_unique
			));

		if($post['cover_hash'] != null) {
			unlink('images/covers/'.$post['cover_hash'].'.webp');
			unlink('images/covers/'.$post['cover_hash'].'.jpg');
		}


		$viewing_in_english = (!isset($_GET['lau']) ? false : true);
		$is_root = true;
		#require_once 'generate-feed-json.php';
		#require_once 'generate-feed-xml.php';

		header("Location: ".url(''));
		exit;



	} else {

		require_once 'site-header.php';



		$array_markdown = [
			[
				'code' => '----',
				'result' => ($viewing_in_english == false ? 'Sidbrytare' : 'Page breaker')
			],
			[
				'code' => '**'.($viewing_in_english == false ? 'Fet text' : 'Bold text').'**',
				'result' => '<b>'.($viewing_in_english == false ? 'Fet text' : 'Bold text').'</b>'
			],
			[
				'code' => '### '.($viewing_in_english == false ? 'Underrubrik' : 'Subtitle'),
				'result' => ($viewing_in_english == false ? 'Underrubrik' : 'Subtitle')
			],
			[
				'code' => '[text]('.($viewing_in_english == false ? 'link' : 'url').')',
				'result' => '<a href="javascript:void(0)">'.($viewing_in_english == false ? 'Text' : 'Text').'</a>'
			],
			[
				'code' => ($viewing_in_english == false ? '![alternativ text](adress)' : '![alternative text](url)'),
				'result' => ($viewing_in_english == false ? 'Visar en bild' : 'Shows a picture')
			],
			[
				'code' => '`'.($viewing_in_english == false ? 'Kod' : 'Code').'`',
				'result' => '<code>'.($viewing_in_english == false ? 'Kod' : 'Code').'</code>'
			],
			[
				'code' => '1. Text',
				'result' => ($viewing_in_english == false ? 'Numrerad lista' : 'Numerical list')
			],
			[
				'code' => '* Text',
				'result' => ($viewing_in_english == false ? 'Punktlista' : 'Dot list')
			],
			[
				'code' => ($viewing_in_english == false ? 'Källa [^siffra]' : 'Source [^digit]'),
				'result' => ($viewing_in_english == false ? 'Källa' : 'Source').'<sup>1</sup>'
			],
			[
				'code' => ($viewing_in_english == false ? '[^siffra]: Text' : '[^digit]: Text'),
				'result' => '1. '.($viewing_in_english == false ? 'Text' : 'Text')
			],
			[
				'code' => ($viewing_in_english == false ? '*[ord]: Några ord' : '*[word]: Some words'),
				'result' => ($viewing_in_english == false ? '<abbr title="Ange ordet någonstans i inlägget">ord</abbr>' : '<abbr title="Define the word in the post">word</abbr>')
			]
		];



		$editing = false;
		$error = null;

		if(isset($_GET['idu'])) {
			$editing = true;
			$id_unique = strip_tags(htmlspecialchars($_GET['idu']));

			$post_exists = sql("SELECT COUNT(id_unique)
								FROM posts
								WHERE id_unique = :_idunique
							   ", Array(
								   '_idunique' => $id_unique
							   ), 'count');

			if($post_exists != 0) {
				$postid = sql("SELECT id
							   FROM posts
							   WHERE id_unique = :_idunique
							  ", Array(
								  '_idunique' => $id_unique
							  ), 'fetch');

				$c_edits = sql("SELECT COUNT(id_post)
								FROM posts
								WHERE id_post = :_idpost
							   ", Array(
								   '_idpost' => (int)$postid['id']
							   ), 'count');


				if($c_edits != 0) {
					$post = sql("SELECT *
								 FROM posts
								 WHERE id_post = :_idpost
								 ORDER BY timestamp_edited DESC
								 LIMIT 1
								", Array(
									'_idpost' => (int)$postid['id']
								), 'fetch');

					$id_post = (int)$post['id_post'];


				} else {
					$post = sql("SELECT *
								 FROM posts
								 WHERE id_unique = :_idunique
								", Array(
									'_idunique' => $id_unique
								), 'fetch');

					$id_post = (int)$post['id'];
				}


				$tag_exists = sql("SELECT COUNT(t.name)
								   FROM tags AS t
								   JOIN tags_linked AS tl
								   ON t.id = tl.id_tag
								   WHERE tl.id_post = :_idpost
								  ", Array(
									  '_idpost' => (int)$id_post
								  ), 'count');


				if($tag_exists != 0) {
					$get_tags = sql("SELECT t.name
									 FROM tags AS t
									 JOIN tags_linked AS tl
									 ON t.id = tl.id_tag
									 WHERE tl.id_post = :_idpost
									", Array(
										'_idpost' => (int)$id_post
									));

					foreach($get_tags AS $tag) {
						$tags[] = $tag['name'];
					}
				}
			}


			if($post_exists == 0) {
				$error .= '<div class="message color-red">';
					$error .= ($viewing_in_english == false ? 'Kunde inte hitta inlägget' : 'Couldn\'t find the post');
				$error .= '</div>';

			} else {
				$notswedish = ($post['is_inenglish'] == null ? false : true);

				if($viewing_in_english == true AND $post['is_inenglish'] == null OR $viewing_in_english == false AND $post['is_inenglish'] != null) {
					$error .= '<div class="message color-red">';
						$error .= ($viewing_in_english == false ? 'Inlägget är i ett annat språk än visningsspråket.' : 'The post are in a different language than the chosen language.');
					$error .= '</div>';
				}
			}
		}





		echo '<section id="write">';
			if($error != null) {
				echo $error;

			} else {
				/* echo '<h2>';
					if($viewing_in_english == false) {
						echo ($editing == false ? 'Skriv ett nytt inlägg' : 'Hantera ett inlägg');
					} else {
						echo ($editing == false ? 'Write a new post' : 'Manage a post');
					}
				echo '</h2>'; */


				echo '<div id="message"></div>';
				echo '<form action="javascript:void(0)" method="POST" autocomplete="off">';

					if($editing == true) {
						echo '<input type="hidden" name="hidden-postid" value="'.$id_unique.'">';
					}


					echo '<div class="item subject">';
						echo '<div class="field">';
							echo '<input type="text" name="field-subject" tabindex="1" placeholder="'.($viewing_in_english == false ? 'Rubrik (krävs)' : 'Subject (required)').'"';
							echo ($editing == false ? '' : ' value="'.$post['subject'].'"');
							echo ' required>';
						echo '</div>';
					echo '</div>';


					echo '<div class="side-by-side">';
						echo '<div class="item tags">';
							echo '<div class="label">';
								echo ($viewing_in_english == false ? 'Taggar' : 'Tags');
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="text" name="field-tags" placeholder="'.($viewing_in_english == false ? 'Krävs. Separera med komma (,)' : 'Required. Seperate with comma (,)').'" tabindex="2"';
								echo ($editing == false ? '' : ($tag_exists == 0 ? '' : ' value="'.implode(',', $tags).'"'));
								echo ' required>';
							echo '</div>';
						echo '</div>';


						echo '<div class="item publish">';
							echo '<div class="label">';
								echo ($viewing_in_english == false ? 'Publicerad' : 'Published');
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="text" name="field-publish-date" placeholder="'.date('Y-m-d').'" tabindex="3"';
								echo ($editing == false ? '' : ' value="'.date('Y-m-d', $post['timestamp_published']).'"');
								echo '>';

								echo '<input type="text" name="field-publish-time" placeholder="'.date('H:i').'" tabindex="3"';
								echo ($editing == false ? '' : ' value="'.date('H:i', $post['timestamp_published']).'"');
								echo '>';
							echo '</div>';
						echo '</div>';
					echo '</div>';


					echo '<div class="item withdesc">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Omslagsbild från' : 'Cover from').' '.link_('Unsplash', 'https://unsplash.com/s/photos/nature?orientation=landscape');
						echo '</div>';

						echo '<div class="fields">';
							echo '<input type="url" name="field-cover" tabindex="4" placeholder="'.($viewing_in_english == false ? 'Direktlänk till bilden (valfritt)' : 'Direct link to the image (valfritt)').'"';
							echo ($editing == false ? '' : ' value="'.$post['cover_url'].'"');
							echo '>';

							echo '<input type="text" name="field-cover-owner" tabindex="5" placeholder="'.($viewing_in_english == false ? 'Fotografens namn' : 'Owner\'s name').'"';
							echo ($editing == false ? '' : ' value="'.$post['cover_owner'].'"');
							echo '>';

							echo '<input type="url" name="field-cover-owner-url" tabindex="6" placeholder="'.($viewing_in_english == false ? 'Webbadress till ägaren' : 'URL to the owner').'"';
							echo ($editing == false ? '' : ' value="'.$post['cover_owner_url'].'"');
							echo '>';
						echo '</div>';
					echo '</div>';


					echo '<div class="item content">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Innehåll' : 'Content');
						echo '</div>';

						echo '<div class="field">';
							echo '<textarea name="field-content" tabindex="7" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" required>';
								echo ($editing == false ? '' : str_replace('<br />', '', $post['content_beforebreak']) . ($post['content_afterbreak'] == null ? null : "\r\n----\r\n".str_replace('<br />', '', $post['content_afterbreak'])));
							echo '</textarea>';
						echo '</div>';
					echo '</div>';


					echo '<div class="item content-meta">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Innehåll för metatagg' : 'Content for the meta tag');
						echo '</div>';

						echo '<div class="field">';
							echo '<textarea name="field-content-meta" tabindex="8" maxlength="180" placeholder="'.($viewing_in_english == false ? 'Detta innehåll visas när man delar länken till inlägget, inga taggar är tillåtna (valfritt)' : 'The content will be visible when you share the link to the post, no tags are allowed (optional)').'" required>';
								echo ($editing == false ? '' : $post['content_meta']);
							echo '</textarea>';
						echo '</div>';
					echo '</div>';


					echo '<div class="checkboxes">';
						echo '<div class="pretty p-default p-round p-thick">';
							echo '<input type="checkbox" name="check-1" id="check-1"';
							echo ($editing == false ? '' : ($post['hasbeencorrected'] == true ? ' checked' : ''));
							echo '>';

							echo '<div class="state p-success-o">';
								echo '<label for="check-1">';
									echo ($viewing_in_english == false ? 'Inlägget har lästs igenom och korrigerats' : 'The post has been reviewed and corrected');
								echo '</label>';
							echo '</div>';
						echo '</div>';


						echo '<div class="pretty p-default p-round p-thick">';
							echo '<input type="checkbox" name="check-2" id="check-2"';
							echo ($editing == false ? '' : ($post['is_pinned'] == true ? ' checked' : ''));
							echo '>';

							echo '<div class="state p-primary-o">';
								echo '<label for="check-2">';
									echo ($viewing_in_english == false ? 'Fäst inlägget överst' : 'Pin the post');
								echo '</label>';
							echo '</div>';
						echo '</div>';


						echo '<div class="pretty p-default p-round p-thick">';
							echo '<input type="checkbox" name="check-3" id="check-3"';
							echo ($editing == false ? '' : ($viewing_in_english == false ? '' : ' checked'));
							echo ($viewing_in_english == false ? '' : ' checked');
							echo '>';

							echo '<div class="state p-primary-o">';
								echo '<label for="check-3">';
									echo ($viewing_in_english == false ? 'Det här inlägget är på engelska' : 'This post is in English');
								echo '</label>';
							echo '</div>';
						echo '</div>';
					echo '</div>';



					echo '<div class="button">';
						$publish = ($viewing_in_english == false ? 'Publicera' : 'Publish');
						$save = ($viewing_in_english == false ? 'Spara' : 'Save');
						$save_unpublish = ($viewing_in_english == false ? 'Spara och avpublicera' : 'Save & unpublish');

						echo '<input type="submit" name="button-'.($editing == false ? 'publish' : ($post['timestamp_saved'] != null ? 'finish' : 'edit').'').'" tabindex="10" value="'.($editing == false ? $publish : ($post['timestamp_saved'] == null ? $save : $publish)).'">';
						echo '<input type="submit" name="button-save'.($editing == false ? '-new' : ($post['timestamp_saved'] == null ? '-unpublish' : '')).'" tabindex="9" value="'.($editing == false ? $save : ($post['timestamp_saved'] == null ? $save_unpublish : $save)).'">';

						if($editing == true) {
							echo '<div class="links">';
								echo '<a href="'.url('delete:'.$id_unique . ($post['cover_hash'] == null ? '' : '/cover:'.$post['cover_hash'])).'" id="delete" onClick="return confirm(\''.($viewing_in_english == false ? 'Inlägget kommer att permanent raderas från servern.' : 'The post will be permanently deleted from the server.').'\')">';
									echo ($viewing_in_english == false ? 'Ta bort' : 'Delete');
								echo '</a>';

								echo '<a href="'.url('read:'.$id_unique).'" id="goback">';
									echo ($viewing_in_english == false ? 'Visa inlägget' : 'View the post');
								echo '</a>';
							echo '</div>';
						}
					echo '</div>';

				echo '</form>';



				echo '<div class="markdown-sheet">';
					foreach($array_markdown AS $markdown) {
						echo '<div class="item">';
							echo '<div>'.$markdown['code'].'</div>';
							echo '<div>'.$markdown['result'].'</div>';
						echo '</div>';
					}
				echo '</div>';

			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
