<?php

	require_once 'site-settings.php';

	$id_unique = null;
	$reading_post = false;

	if(isset($_GET['idu'])) {
		$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
		$post_do_exists = false;

		$post_exists = sql("SELECT COUNT(id_unique)
							FROM posts
							WHERE id_unique = :_idunique
							".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
						   ", Array(
							   '_idunique' => $id_unique
						   ), 'count');

		if($post_exists != 0) {
			$post_do_exists = true;
			$reading_post = true;

			$post = sql("SELECT *
						 FROM posts
						 WHERE id_unique = :_idunique
						 ".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
						", Array(
							'_idunique' => $id_unique
						), 'fetch');

			$tags_exists = sql("SELECT COUNT(t.name)
								FROM tags AS t
								JOIN tags_linked AS tl
								ON t.id = tl.id_tag
								WHERE tl.id_post = :_idpost
							   ", Array(
								   '_idpost' => (int)$post['id']
							   ), 'count');

			if($tags_exists != 0) {
				$get_tags = sql("SELECT t.name
								 FROM tags AS t
								 JOIN tags_linked AS tl
								 ON t.id = tl.id_tag
								 WHERE tl.id_post = :_idpost
								", Array(
									'_idpost' => (int)$post['id']
								));

				$arr_keywords = [];
				foreach($get_tags AS $keyword) {
					$arr_keywords[] = $keyword['name'];
				}
			}
		}
	}

	$meta_url = $og_url . ($reading_post == false ? '' : '/read:'.$post['id_unique']);
	$meta_title = ($reading_post == false ? $og_title : $post['subject']);
	$meta_description = ($reading_post == true ? (empty($post['content_meta']) ? ($viewing_in_english == false ? 'Klicka eller tryck här för att läsa inlägget.' : 'Click or press here to read the blog post.') : shorten_text($post['content_meta'])) : $og_description);
	$meta_image = (($reading_post == true AND $post['cover_hash'] != null) ? $og_url.'/cover:'.$post['cover_hash'] : $og_image);



	echo '<!DOCTYPE html>';
	echo '<html lang="'.($viewing_in_english == false ? 'sv' : 'en').'">';
		echo '<head>';
			echo '<title>';
				echo $og_title;
				echo ($filename == 'page-read.php' ? ' _ '.($post_do_exists == false ? ($viewing_in_english == false ? 'Okänt inlägg' : 'Unknown post') : $post['subject']) : '');
				echo ($filename == 'page-about.php' ? ' _ '.($viewing_in_english == false ? 'Om' : 'About') : '');
				echo ($filename == 'page-privacy.php' ? ' _ Integritetspolicy' : '');
				echo ($filename == 'page-privacy-en.php' ? ' _ Privacy policy' : '');
				echo ($filename == 'page-search.php' ? ' _ '.($viewing_in_english == false ? 'Sök' : 'Search') : '');
				echo ($filename == 'page-license.php' ? ' _ '.($viewing_in_english == false ? 'Licens' : 'License') : '');
				echo ($filename == 'page-statistics.php' ? ' _ '.($viewing_in_english == false ? 'Statistik' : 'Statistics') : '');
				echo ($filename == 'admin-login.php' ? ' _ '.($viewing_in_english == false ? 'Logga in' : 'Log in') : '');
				echo (($filename == 'admin-write.php' AND $id_unique == null) ? ' _ '.($viewing_in_english == false ? 'Skriv ett nytt inlägg' : 'Compose a new post') : '');
				echo (($filename == 'admin-write.php' AND $id_unique != null) ? ' _ '.$post['subject'].' ('.($viewing_in_english == false ? 'hantera' : 'manage').')' : '');
			echo '</title>';

			echo '<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">';
			echo '<meta name="google" content="notranslate">';
			echo '<meta name="google" content="nopagereadaloud">';
			echo '<meta name="robots" content="index,follow">';

			echo '<meta name="description" content="'.$meta_description.'">';
			echo (($reading_post == false OR empty($arr_keywords)) ? '' : '<meta name="keywords" content="'.implode(',', $arr_keywords).'">');
			echo '<meta name="copyright" content="'.$config_author_name.'">';
			echo '<meta name="owner" content="'.$config_author_name.'">';
			echo '<meta name="author" content="'.$config_author_name.', '.$config_author_email.'">';

			echo '<meta itemprop="name" content="'.$meta_title.'">';
			echo '<meta itemprop="description" content="'.$meta_description.'">';
			echo '<meta itemprop="image" content="'.$meta_image.'">';

			echo '<meta property="og:url" content="'.$meta_url.'">';
			echo '<meta property="og:type" content="'.($reading_post == false ? 'website' : 'article').'">';
			echo '<meta property="og:title" content="'.$meta_title.'">';
			echo '<meta property="og:description" content="'.$meta_description.'">';
			echo '<meta property="og:image" content="'.$meta_image.'">';
			echo '<meta property="og:locale" content="'.($viewing_in_english == false ? 'sv_SE' : 'en_GB').'">';

			if($reading_post == true) {
				echo '<meta property="article:published_time" content="'.date('Y-m-dTH:i:s+0100', $post['timestamp_published']).'">';
				echo ($post['timestamp_edited'] == null ? '' : '<meta property="article:modified_time" content="'.date('Y-m-dTH:i:s+0100', $post['timestamp_edited']).'">');
				echo '<meta name="article:author" content="'.$config_author_name.'">';

				if($tags_exists != 0) {
					foreach($get_tags AS $tag) {
						echo '<meta name="article:tag" content="'.$tag['name'].'">';
					}
				}
			}

			echo '<meta name="twitter:card" content="summary_large_image">';
			echo '<meta name="twitter:title" content="'.$meta_title.'">';
			echo '<meta name="twitter:description" content="'.$meta_description.'">';
			echo '<meta name="twitter:image:src" content="'.$meta_image.'">';

			echo '<link rel="webmention" href="https://webmention.io/'.$config_subdomain . ($config_subdomain == null ? '' : '.') . $config_domain.'/webmention">';
			echo '<link rel="pingback" href="https://webmention.io/'.$config_subdomain . ($config_subdomain == null ? '' : '.') . $config_domain.'/xmlrpc">';
			echo ($reading_post == false ? '' : '<link rel="pingback" href="https://webmention.io/webmention?forward='.$og_url.'/webmention">');
			echo '<link rel="alternate" type="application/rss+xml" title="'.$og_title.' RSS" href="'.$og_url.'/'.($viewing_in_english == false ? '' : 'en/').'feed">';
			echo '<link rel="canonical" href="'.$og_url . ($reading_post == false ? '' : '/read:'.$post['id_unique']).'">';
			echo '<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII=" rel="icon" type="image/x-icon">';

			echo '<link href="https://airikr.me" rel="me">';
			echo '<link href="https://github.com/e-edgren" rel="me">';
			echo '<link href="https://codeberg.org/airikr" rel="me">';
			echo '<link href="https://fosstodon.org/@edgren" rel="me">';


			# echo '<link href="https://tools.aftertheflood.com/sparks/styles/font-faces.css" rel="stylesheet" />';
			echo '<link type="text/css" rel="stylesheet preload" as="style" href="'.($config_development == true ? url('css/theme-'.$config_defaulttheme.'.css?'.time(), true) : url('css/theme-dark.min.css', true)).'" id="theme">';
			echo '<link type="text/css" rel="stylesheet preload" as="style" href="'.($config_development == true ? url('css/desktop.css?'.time(), true) : url('css/desktop.min.css', true)).'">';
		echo '</head>';
		echo '<body>';



			echo '<noscript><div>';
				echo '<link type="text/css" rel="stylesheet preload" as="style" href="'.($config_development == true ? url('css/noscript.css?'.time(), true) : 'https://cdn.airikr.me/blog/noscript.min.css').'">';

				if($viewing_in_english == false) {
					echo 'JavaScript är inaktiverat. Vissa funktioner är gömda eller oanvändbara.';
				} else {
					echo 'JavaScript are inactivated. Some features will be hidden or unusable.';
				}
			echo '</div></noscript>';



			echo '<section id="website">';
				echo '<div class="aside open">'.svgicon('menu-open').'</div>';
				echo '<div class="aside close">'.svgicon('menu-close').'</div>';

				echo '<div class="change-theme">';
					echo '<a href="javascript:void(0)" class="theme'.($config_defaulttheme == 'dark' ? ' hide' : '').'" data-theme="dark">';
						echo svgicon('linkicon-theme-dark');
					echo '</a>';

					echo '<a href="javascript:void(0)" class="theme'.($config_defaulttheme == 'light' ? ' hide' : '').'" data-theme="light">';
						echo svgicon('linkicon-theme-light');
					echo '</a>';
				echo '</div>';



				echo '<aside>';
					echo '<header class="no-select">';
						echo 'Airikr';
					echo '</header>';

					echo '<nav id="main" class="small-text">';
						echo '<a href="'.url('').'"'.((strpos($filename, 'index') !== false AND !isset($_GET['pag'])) ? ' class="active"' : '').'>';
							echo svgicon('linkicon-home');
							echo ($viewing_in_english == false ? 'Hem' : 'Home');
						echo '</a>';

						echo '<a href="'.url('search').'"'.(strpos($filename, 'search') !== false ? ' class="active"' : '').'>';
							echo svgicon('linkicon-search');
							echo ($viewing_in_english == false ? 'Sök' : 'Search');
						echo '</a>';

						echo '<a href="'.url('statistics').'"'.(strpos($filename, 'statistics') !== false ? ' class="active"' : '').'>';
							echo svgicon('linkicon-statistics');
							echo ($viewing_in_english == false ? 'Statistik' : 'Statistics');
						echo '</a>';

						echo '<a href="'.url('random-post'.($reading_post == true ? '/current:'.$post['id_unique'] : null)).'">';
							echo svgicon('linkicon-dice');
							echo ($viewing_in_english == false ? 'Slumpa inlägg' : 'Random post');
						echo '</a>';

						if($config_allowcomments == true) {
							echo '<a href="'.url('comments').'"'.(strpos($filename, 'comments') !== false ? ' class="active"' : '').'>';
								echo svgicon('linkicon-comments');
								echo ($viewing_in_english == false ? 'Kommentarer' : 'Comments');
							echo '</a>';
						}

						echo '<a href="'.url('tags').'"'.(strpos($filename, 'tags') !== false ? ' class="active"' : '').'>';
							echo svgicon('linkicon-tag');
							echo ($viewing_in_english == false ? 'Taggar' : 'Tags');
						echo '</a>';


						echo '<div class="space"></div>';


						echo '<a href="'.url('privacy').'"'.(strpos($filename, 'privacy') !== false ? ' class="active"' : '').'>';
							echo svgicon('linkicon-privacy');
							echo ($viewing_in_english == false ? 'Integritetspolicy' : 'Privacy policy');
						echo '</a>';

						if($filename == 'page-privacy.php' OR $filename == 'page-privacy-en.php') {
							echo '<div class="submenu">';
								echo '<a href="'.url('privacy:collects').'">';
									echo ($viewing_in_english == false ? 'Insamling' : 'Collects');
								echo '</a>';

								echo '<a href="'.url('privacy:uses').'">';
									echo ($viewing_in_english == false ? 'Behandling' : 'Uses');
								echo '</a>';

								echo '<a href="'.url('privacy:duration').'">';
									echo ($viewing_in_english == false ? 'Varaktivhet' : 'Duration');
								echo '</a>';

								echo '<a href="'.url('privacy:access').'">';
									echo ($viewing_in_english == false ? 'Åtkomst' : 'Access');
								echo '</a>';

								echo '<a href="'.url('privacy:rights').'">';
									echo ($viewing_in_english == false ? 'Rättigheter' : 'Rights');
								echo '</a>';

								echo '<a href="'.url('privacy:stored').'">';
									echo ($viewing_in_english == false ? 'Lagring' : 'Stored');
								echo '</a>';
							echo '</div>';
						}

						echo '<a href="'.url('license').'"'.(strpos($filename, 'license') !== false ? ' class="active"' : '').'>';
							echo svgicon('linkicon-copyright');
							echo ($viewing_in_english == false ? 'Licens' : 'License');
						echo '</a>';



						echo '<h4>'.($viewing_in_english == false ? 'Konto' : 'Account').'</h4>';

						if($session == true) {
							$id_unique = (isset($_GET['idp']) ? strip_tags(htmlspecialchars($_GET['idp'])) : null);

							if($user['password'] == null) {
								echo '<a href="'.url('new-post').'"'.(strpos($filename, 'write') !== false ? ' class="active"' : '').'>';
									echo svgicon('linkicon-admin-write');
									echo ($viewing_in_english == false ? 'Författa' : 'Write');
								echo '</a>';

								echo '<a href="'.url('saved-posts').'"'.((strpos($filename, 'index') !== false AND isset($_GET['pag'])) ? ' class="active"' : '').'>';
									echo svgicon('linkicon-admin-saved');
									echo ($viewing_in_english == false ? 'Sparade' : 'Saved');
								echo '</a>';

								echo '<a href="'.url('files').'"'.(strpos($filename, 'files') !== false ? ' class="active"' : '').'>';
									echo svgicon('linkicon-admin-files');
									echo ($viewing_in_english == false ? 'Filer' : 'Files');
								echo '</a>';

								echo '<a href="'.url('settings').'"'.(strpos($filename, 'settings') !== false ? ' class="active"' : '').'>';
									echo svgicon('linkicon-admin-settings');
									echo ($viewing_in_english == false ? 'Inställningar' : 'Settings');
								echo '</a>';
							}

							echo '<a href="'.url('logout').'" class="color-red">';
								echo svgicon('linkicon-admin-logout');
								echo ($viewing_in_english == false ? 'Logga ut' : 'Log out');
							echo '</a>';


						} else {
							echo '<a href="'.url('login').'"'.(strpos($filename, 'login') !== false ? ' class="active"' : '').'>';
								echo svgicon('linkicon-login');
								echo ($viewing_in_english == false ? 'Logga in' : 'Log in');
							echo '</a>';
						}
					echo '</nav>';



					echo '<div class="about">';
						echo '<div class="avatar"></div>';

						if($viewing_in_english == false) {
							echo '<p>Jag heter Erik Edgren och är '.age(1985, 7, 6).' år. Bor på Hammarö i Värmland. Har mild autism och full uppmärksamhetsstörning. '.link_('Läs mer', 'https://airikr.me').'.</p>';
							echo '<p>Bloggen handlar om hur jag ser på saker och hur jag hanterar vardagen.</p>';

						} else {
							echo '<p>My name is Erik Edgren, and I am '.age(1985, 7, 6).' years old. I live on an island called Hammarö in Sweden and have mild autism and full attention deficit disorder. '.link_('Read more', 'https://airikr.me/en').'.</p>';
							echo '<p>On this blog, you can read about my personal views of privacy and other stuff.</p>';
						}
					echo '</div>';



					echo '<div class="other">';
						echo '<div class="icons">';
							echo link_(svgicon('linkicon-rss'), url('rss'));

							echo '<a href="'.url('your-data').'">';
								echo svgicon('your-data');
							echo '</a>';

							echo '<a href="https://codeberg.org/airikr/airikr-blog" target="_blank">';
								echo svgicon('linkicon-git');
							echo '</a>';

							echo '<a href="javascript:void(0)" class="theme'.($config_defaulttheme == 'dark' ? ' hide' : '').'" data-theme="dark">';
								echo svgicon('linkicon-theme-dark');
							echo '</a>';

							echo '<a href="javascript:void(0)" class="theme'.($config_defaulttheme == 'light' ? ' hide' : '').'" data-theme="light">';
								echo svgicon('linkicon-theme-light');
							echo '</a>';
						echo '</div>';
					echo '</div>';
				echo '</aside>';



				echo '<main';
				echo ($viewing_in_english == false ? '' : ' data-language="en"');
				echo ' data-default-theme="'.$config_defaulttheme.'"';
				echo ($theme == 'light' ? ' data-theme="light"' : '');
				echo ($session == true ? ' class="logged-in"' : '');
				echo ' data-localfolder="'.$config_localfolder.'"';
				echo ((isset($not_logged) AND $not_logged == false) ? '' : ' class="spacebelow"');
				echo '>';

					echo '<div class="dim" id="dim"></div>';

					echo '<div id="analytics-question"'.((isset($not_logged) AND $not_logged == false) ? '' : ' style="display: block;"').'>';
						echo '<div class="minimized no-select">';
							echo ($viewing_in_english == false ? 'Dataanalysförfrågan' : 'Analytics request');
						echo '</div>';

						echo '<div class="maximized">';
							if($viewing_in_english == false) {
								echo '<p>Dataanalysen kommer att göra '.$og_title.' bättre. Om du godkänner förfrågan, kommer följande att lagras om dig:</p>';

							} else {
								echo '<p>The analytics will make '.$og_title.' better. If you accept the analytics request, the following data will be stored about you:</p>';
							}

							echo '<ul>';
								echo '<li>'.($viewing_in_english == false ? 'Enhetens IP-adress' : 'The device\'s IP address').'</li>';
								echo '<li>'.($viewing_in_english == false ? 'Webbläsarens användaragent' : 'Browser\'s user agent').'</li>';
								echo '<li>'.($viewing_in_english == false ? 'Vilka sidor du besöker' : 'What pages you visit').'</li>';
								echo '<li>'.($viewing_in_english == false ? 'När du godkände datainsamling' : 'A timestamp when you agreed to the analytics').'</li>';
								echo '<li>'.($viewing_in_english == false ? 'När du besökte en sida' : 'A timestamp for each page you\'ve visited').'</i>';
							echo '</ul>';

							if($viewing_in_english == false) {
								echo '<p><a href="'.url('privacy:uses').'">Läs mer</a> om hur webbsidan behandlar dina uppgifter.</p>';
							} else {
								echo '<p><a href="'.url('privacy:uses').'">Read more</a> on how the website uses your data.</p>';
							}

							echo '<div class="links">';
								echo '<a href="javascript:void(0)" id="allow">'.($viewing_in_english == false ? 'Godkänn' : 'Accept').'</a>';
								echo '<a href="javascript:void(0)" id="hide">'.($viewing_in_english == false ? 'Bestäm senare' : 'Decide later').'</a>';
							echo '</div>';

							echo '<div class="message-analytics"></div>';
						echo '</div>';
					echo '</div>';

?>
