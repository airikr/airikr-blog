### Airikr Blog
This is the source code for my personal blog, Airikr Blog.

### Important!
I am do not work with this project anymore. I will keep it active, though, since I might do a better version of this project some day in the future. **This version of the blog are quite buggy**. Expect one or more issues.

### Admin login
Once you have the blog working, visit the login page and login with admin/admin. Then follow the instructions.

### Features
- Strong privacy policy with near to zero logging.
  - The visitors decide when data will be logged on the server and/or on their device.
  - Full control over the saved data.
- Sort posts after tags.
- Markdown for both posts and comments.
- Quickly change theme.
- Detailed statistics.
- Easily share posts to Telegram or a Mastodon instance.
- [Instant View](https://instantview.telegram.org/) for a blog post.
- As of today, simple search for simple words (will be advanced search in the future).
- **ADMIN:** The login demands a username and TOTP. A password are required before setting up TOTP. After that, the password will be removed from the account.
- **ADMIN:** Save posts for later.
- **ADMIN:** Pin posts to the top.
- **ADMIN:** Upload files and manage them.
- **ADMIN:** Set default name, email address, and URL.