<?php

	require_once 'site-header.php';



	$c_comments = sql("SELECT COUNT(id)
					   FROM comments
					  ", Array(), 'count');


	if($c_comments != 0) {
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY timestamp_published DESC
							", Array());
	}







	echo '<section id="comments">';
		echo '<h1>'.($viewing_in_english == false ? 'Kommentarer' : 'Comments').'</h1>';

		echo '<div class="rss-feed">';
			echo svgicon('linkicon-rss');
			echo link_(($viewing_in_english == false ? 'RSS-flöde för alla kommentarer' : 'RSS feed for all the comments'), url('rss/comments'));
		echo '</div>';


		if($c_comments == 0) {
			echo '<div class="message no-select">';
				echo ($viewing_in_english == false ? 'Kunde inte hitta några kommentarer' : 'Couldn\'t find any comments');
			echo '</div>';


		} else {
			foreach($get_comments AS $comment) {
				$identity = (!empty($comment['comment_identity']) ? $comment['comment_identity'] : strtolower(str_replace(' ', '-', trim($comment['visitor_name']))));

				$post = sql("SELECT id_unique, subject
							 FROM posts
							 WHERE id = :_idpost
							", Array(
								'_idpost' => (int)$comment['id_post']
							), 'fetch');


				echo '<div class="comment">';
					echo '<div class="datetime">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Publicerades' : 'Published').':';
						echo '</div>';

						echo '<div class="value">';
							echo '<a href="'.url('read:'.$post['id_unique'].'#'.$comment['id']).'" title="'.($viewing_in_english == false ? 'Gå till kommentaren' : 'Go to the comment').'">';
								echo date_($comment['timestamp_published'], 'short');
							echo '</a>';
						echo '</div>';
					echo '</div>';


					echo '<div class="name">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Avsändare' : 'Author').':';
						echo '</div>';

						echo '<div class="value">';
							echo '<a href="'.url('comment-author:'.$identity).'" title="'.($viewing_in_english == false ? 'Se alla kommentarer från' : 'See all comments from').' '.trim($comment['visitor_name']).'">';
								echo $comment['visitor_name'];
							echo '</a>';

							echo '<span class="rss">';
								echo link_(svgicon('linkicon-rss'), url('rss/comments/author:'.str_replace(' ', '-', strtolower(trim($comment['visitor_name'])))));
							echo '</span>';
						echo '</div>';
					echo '</div>';


					echo '<div class="post">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Inlägg' : 'Post').':';
						echo '</div>';

						echo '<div class="value">';
							echo '<a href="'.url('read:'.$post['id_unique']).'" title="'.($viewing_in_english == false ? 'Gå till inlägget' : 'Go to the post').'">';
								echo $post['subject'];
							echo '</a>';

							echo '<span class="rss">';
								echo link_(svgicon('linkicon-rss'), url('rss/comments/post-id:'.$post['id_unique']));
							echo '</span>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
