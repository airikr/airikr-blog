<?php

	$config_title = null;
	$config_description = ($viewing_in_english == false ? null : null);
	$config_image = null;
	$config_image_alternative = null;

	$config_development = true;
	$config_localfolder = 'airikr-blog';
	$config_domain = null;
	$config_subdomain = null;
	$config_cdn = null;    # Skip https://

	$config_author_name = null;
	$config_author_email = null;

	$config_db_host = 'localhost';
	$config_db_database = null;
	$config_db_username = 'airikr';
	$config_db_password = null;

	$config_smtp_server = null;
	$config_smtp_port = null;
	$config_smtp_username = null;
	$config_smtp_password = null;
	$config_mailer_email = null;
	$config_mailer_name = $config_title;

	$config_encryption_key = null;
	$config_encryption_iv = null;

	$config_max_filesize = (1048576 * 100);
	$config_mastodoninstance = null;

	$config_reviewcomments = false;    # Does not work yet
	$config_allowcomments = true;
	$config_postsperpage = 5;
	$config_defaulttheme = 'dark';

?>