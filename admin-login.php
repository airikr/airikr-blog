<?php

	if(isset($_POST['button-login'])) {
		require_once 'site-settings.php';

		$field_username = strip_tags(htmlspecialchars($_POST['field-username']));
		$field_password = strip_tags(htmlspecialchars($_POST['field-password']));
		$field_totp = (isset($_POST['field-totp']) ? strip_tags(htmlspecialchars($_POST['field-totp'])) : null);

		$check_user = sql("SELECT COUNT(id)
						   FROM users
  						   WHERE username = :_username
  						  ", Array(
  							  '_username' => trim($field_username)
						  ), 'count');

		if($check_user != 0) {
			$user = sql("SELECT *
						 FROM users
						 WHERE username = :_username
						", Array(
							'_username' => trim($field_username)
						), 'fetch');
		}



		if($user['username'] == 'admin' AND password_verify($field_password, $user['password'])) {
			$_SESSION['loggedin'] = (int)$user['id'];

			sql("UPDATE users
				 SET timestamp_lastlogin = :_lastlogin
				 WHERE id = :_iduser
				", Array(
					'_iduser' => (int)$user['id'],
					'_lastlogin' => time()
				));

			header("Location: ".url(''));
			exit;


		} else {
			$ga = new PHPGangsta_GoogleAuthenticator();
			$result = $ga->verifyCode($user['twofactorcode'], $field_totp, 2);

			if($result) {
				$_SESSION['loggedin'] = (int)$user['id'];

				sql("UPDATE users
					 SET timestamp_lastlogin = :_lastlogin
					 WHERE id = :_iduser
					", Array(
						'_iduser' => (int)$user['id'],
						'_lastlogin' => time()
					));

				header("Location: ".url(''));
				exit;

			} else {
				header("Location: ".url('login'));
				exit;
			}
		}


		header("Location: ".url('login'));
		exit;







	} else {

		require_once 'site-header.php';



		$c_users = sql("SELECT COUNT(id)
						FROM users
					   ", Array(), 'count');

		if($c_users == 0) {
			sql("INSERT INTO users(
					 username,
					 password,
					 is_admin
				 )

				 VALUES(
					 :_username,
					 :_password,
					 :_isadmin
				 )
				", Array(
					'_username' => 'admin',
					'_password' => password_hash('admin', PASSWORD_BCRYPT),
					'_isadmin' => 1
				), 'insert');

		} else {
			$c_admin = sql("SELECT COUNT(is_admin)
							FROM users
							WHERE username = :_username
						   ", Array(
							   '_username' => 'admin'
						   ), 'count');


			if($c_admin != 0) {
				$check_admin = sql("SELECT password
									FROM users
									WHERE username = :_username
								   ", Array(
									   '_username' => 'admin'
								   ), 'fetch');
			}
		}







		echo '<section id="login">';
			echo '<div class="icon">'.svgicon('login').'</div>';

			echo '<div id="message"></div>';
			echo '<form action="javascript:void(0)" method="POST" autocomplete="off" novalidate="novalidate">';

				echo '<div class="fields">';
					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Användarnamn' : 'Username');
						echo '</div>';

						echo '<div class="field">';
							echo '<input type="text" name="field-username" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" tabindex="1" autocapitalize="none" required>';
						echo '</div>';
					echo '</div>';


					if($c_users == 0 OR $c_admin != 0 AND password_verify('admin', $check_admin['password'])) {
						echo '<div class="item">';
							echo '<div class="label">';
								echo ($viewing_in_english == false ? 'Lösenord' : 'Password');
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="password" name="field-password" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" tabindex="2" required>';
							echo '</div>';
						echo '</div>';

					} else {
						echo '<div class="item">';
							echo '<div class="label">';
								echo ($viewing_in_english == false ? 'Engångskod' : 'TOTP');
							echo '</div>';

							echo '<div class="field">';
								echo '<input type="text" name="field-totp" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" maxlength="6" tabindex="2" required>';
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';


				echo '<div class="button">';
					echo '<input type="submit" name="button-login" tabindex="3" value="'.($viewing_in_english == false ? 'Logga in' : 'Log in').'">';

					# if($c_users == 0) {
						echo '<div class="inactive no-select small-text">';
							echo ($viewing_in_english == false ? 'Återställ kontot' : 'Reset the account');
						echo '</div>';

					/* } else {
						echo '<a href="'.url('reset-totp').'" class="small-text">';
							echo ($viewing_in_english == false ? 'Återställ kontot' : 'Reset the account');
						echo '</a>';
					} */
				echo '</div>';

			echo '</form>';
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
