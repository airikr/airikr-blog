<?php

	require_once 'site-header.php';



	$page = (!isset($_GET['pag']) ? null : strip_tags(htmlspecialchars($_GET['pag'])));

	$default_title = '<a href="'.url('privacy').'">';
		$default_title .= 'Privacy policy';
	$default_title .= '</a>';
	$default_title .= svgicon('title_arrow');







	echo '<section id="privacy">';
		if($page == 'collects') {

			echo '<h1>'.$default_title.'What the website collects</h1>';

			echo '<p>'.$og_url_noprotocol.' does not store anything about your visit without your consent. If you agree with the analytics (requires JavaScript to accept), the following data will be collected:</p>';

			echo '<ul>';
				echo '<li>The device\'s IP address.</li>';
				echo '<li>Browser\'s user agent.</li>';
				echo '<li>What pages you visit.</li>';
				echo '<li>A timestamp when you agreed to the analytics.</li>';
				echo '<li>A timestamp for each page you\'ve visited.</li>';
			echo '</ul>';

			echo '<p>Please see "<a href="'.url('privacy:uses').'">Uses</a>" to see how your data will be stored.</p>';
			echo '<p>The server is using Apache as a web server and '.link_('their analytics', 'https://httpd.apache.org/docs/2.4/logs.html').' has been disabled.</p>';

			echo '<h4>Commenting</h4>';
			echo '<p>Before you send a comment, an information popup will appear. Please read the information carefully because it tells you what will be collected and how it will be stored in the database.</p>';
			echo '<p>The information popup also contains another checkbox for the comment\'s password. If you want to be able to edit or delete your comment <a href="'.url('privacy:duration').'">within the given period</a>, you must save the comment\'s password somewhere safe. '.$blogowner_capital.' recommends the password manager service '.link_('Bitwarden', 'https://bitwarden.com').'.</p>';


			echo '<div class="pages one-link right">';
				echo '<a href="'.url('privacy:uses').'" class="right">';
					echo 'Uses';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'uses') {

			echo '<h1>'.$default_title.'How the website uses your data</h1>';

			echo '<p>'.$og_url_noprotocol.' uses your data in different ways. Below, you can see how the data will be handled specifically. You can read where your data will be stored <a href="'.url('privacy:stored').'">here</a>.</p>';


			echo '<h4>The device\'s IP address</h4>';
			echo '<p>The device\'s IP address can be linked to a person and are therefore classed as a personal data. Because of this, the IP address will be stored encrypted with '.link_('AES-256', 'https://en.wikipedia.org/wiki/Advanced_Encryption_Standard').', where the decryption keys are owned by the website and its owner. These keys are only visible to '.$blogowner.', but an IP address will only be decrypted when a crime has been committed (which hopefully will never happen).</p>';
			echo '<p>The IP address will be collected when you send a comment and if you agree to the analytics.</p>';
			echo '<p>The data can only be seen by you if you go to the "<a href="'.url('your-data').'">Your data</a>" page. The IP address will be visible in plaintext only because it is being fetched by PHP (source '.link_('1', 'https://codeberg.org/airikr/airikr-blog/src/branch/main/page-yourdata.php#L72').' & '.link_('2', 'https://codeberg.org/airikr/airikr-blog/src/branch/main/site-functions.php#L655').').</p>';
			echo '<p><a href="'.url('privacy:duration').'">Read more</a> of how long the IP address will be logged.</p>';

			echo '<h4>The browser\'s user agent</h4>';
			echo '<p>A user agent shows what device, browser, and operating system you used when you visited the website, which does not link to a person. The user agent is therefore not being classed as a personal data and will be stored in plaintext in the database. The user agent are useful for '.$blogowner.' to make the website better.</p>';
			echo '<p>The user agent will only be logged if you agree to the analytics.</p>';
			echo '<p>The data can only be seen by you if you go to the "<a href="'.url('your-data').'">Your data</a>" page.</p>';

			echo '<h4>Your name in a comment</h4>';
			echo '<p>Your name '.link_('is classed as personal data', 'https://gdpr.eu/eu-gdpr-personal-data/').', but the name will be stored in plaintext because it is linked to your comment, which is public. You may use a different identifier for your comment, such as a username, but you are not allowed to impersonate someone else. The alternative name must not use any other words or names that can be seen as sensitive.</p>';
			echo '<p>The data will be public for all who reads the comment.</p>';

			echo '<h4>Your URL in a comment</h4>';
			echo '<p>Your URL are not classed as personal data and will therefore be logged in plaintext.</p>';
			echo '<p>The data will be public for all who reads the comment.</p>';

			echo '<h4>The content in a comment</h4>';
			echo '<p>The comment itself is not classed as personal data and will therefore be stored in plaintext. But if a comment contains sensitive information such as phone numbers, home addresses, email addresses, etc., the comment will be deleted by '.$blogowner.'. But there are some exceptions.</p>';
			echo '<p>If the sender includes the name of another sender that has commented the blog post or another blog post on the website, that name will not be classed as personal data. But if the sender writes something like "you can reach [surname] at [email address]", the comment will be deleted.</p>';
			echo '<p>The data will be public for all who reads the comment.</p>';

			echo '<h4>Other data</h4>';
			echo '<p>If you agree that the website collects data about you and about your visit, the pages you visit within '.$og_url_noprotocol.', when you visited them, and when you agreed to the analytics will be logged. This data are not classed as personal data and will therefore be stored in plaintext.</p>';
			echo '<p>The data can only be seen by you if you go to the "<a href="'.url('your-data').'">Your data</a>" page.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:collects').'">';
					echo 'Collects';
				echo '</a>';

				echo '<a href="'.url('privacy:duration').'" class="right">';
					echo 'Duration';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'duration') {

			echo '<h1>'.$default_title.'How long your data will be kept</h1>';

			echo '<p>IP addresses that have been stored with help of the analytics, including everything else that are associated with that IP address regarding analytics, will be permanently deleted 1 month after the started to be logged.</p>';
			echo '<p>IP addresses that are linked to a comment will be deleted from the comment 2 weeks after the comment was published. After this period, the owner of that comment will lose the rights to edit his or her\'s comment. If the comment owner wants to edit his or her\'s comment after this period, he or she must contact the blog owner to let him edit the comment for them.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:uses').'">';
					echo 'Uses';
				echo '</a>';

				echo '<a href="'.url('privacy:access').'" class="right">';
					echo 'Access';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'access') {

			echo '<h1>'.$default_title.'People who have access to the data</h1>';

			echo '<p>Only '.$blogowner.' can reach the server who holds the database for '.$og_url_noprotocol.'. He can see the encrypted string for the IP addresses, and he can also access the decryption keys, but privacy does not equal violation. The IP address is encrypted for a reason.</p>';
			echo '<p>However, <a href="'.url('privacy:uses').'">the data that are public</a>, will be seen by anyone for obvious reasons.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:duration').'">';
					echo 'Duration';
				echo '</a>';

				echo '<a href="'.url('privacy:rights').'" class="right">';
					echo 'Rights';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'rights') {

			echo '<h1>'.$default_title.'What your rights are</h1>';

			echo '<h4 class="first">Automatic data collection</h4>';
			echo '<p>You have the rights to deny the analytics so nothing will be stored about you. And if you have agreed to the analytics, you have the right to request deletion of all data that the website have collected about you.</p>';
			echo '<p>The easiest way to do this, will be if you go to the "<a href="'.url('your-data').'">Your data</a>" page. You can also '.$blogowner_contact.' to request deletion of your data, but it is recommended to do it from the "Your data" page.</p>';
			echo '<p>If you contact the blog owner regarding deletion of your data, he need the IP address of the device you used at the time your visited the website. Alternative, he need what device you used, what browser you used, and what operating system you used at that time, including what date and time you visited a page on '.$og_url_noprotocol.'.</p>';
			echo '<p>With other words, it would be much easier for both you and '.$blogowner.' if you delete your own data from the "<a href="'.url('your-data').'">Your data</a>" page or wait until the data will automatically be deleted <a href="'.url('privacy:duration').'">after the given time</a>.</p>';

			echo '<h4>Self data collection</h4>';
			echo '<p>You have the right to delete any of your own comments by using the password for that comment. If you don\'t have access to the password and have your URL in the comment, you can '.$blogowner_contact.' and let him delete your comment on the following terms:</p>';

			echo '<ul>';
				echo '<li>Create a temporary blog post on your website where you state that you commented a blog post on '.$og_url_noprotocol.' (state what blog post it was) and the link to the comment.</li>';
				echo '<li>Add a text string in the source code that the blog owner can see when visiting your website. He will give you a specific text string that you can have visible on your website or as a comment in the website\'s source code when viewing it from the browser.</li>';
			echo '</ul>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:access').'">';
					echo 'Access';
				echo '</a>';

				echo '<a href="'.url('privacy:stored').'" class="right">';
					echo 'Stored';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'stored') {

			echo '<h1>'.$default_title.'Where your data are stored</h1>';

			echo '<p>The server that holds '.$og_url_noprotocol.' are located in '.$blogowner.'\'s home in Sweden. Only the blog owner can log in to the server.</p>';
			echo '<p>The website uses '.link_('bunny.net', 'https://bunny.net').' as CDN to let files that are not updated that often to be cached, but they or any other third-party companies can read or reach your data.</p>';


			echo '<div class="pages one-link">';
				echo '<a href="'.url('privacy:rights').'">';
					echo 'Rights';
				echo '</a>';
			echo '</div>';



		} else {

			echo '<h1>Privacy policy</h1>';

			echo '<p>This website tells you what and how '.$og_url_noprotocol.' collects about you and your visit. It is important for '.$blogowner.' that you both read and understand the privacy policy.</p>';


			echo '<div class="links">';
				echo '<a href="'.url('privacy:collects').'">';
					echo 'What the website collects';
				echo '</a>';

				echo '<a href="'.url('privacy:uses').'">';
					echo 'How the website uses your data';
				echo '</a>';

				echo '<a href="'.url('privacy:duration').'">';
					echo 'How long your data will be stored';
				echo '</a>';

				echo '<a href="'.url('privacy:access').'">';
					echo 'People who have access to your data';
				echo '</a>';

				echo '<a href="'.url('privacy:rights').'">';
					echo 'What your rights are';
				echo '</a>';

				echo '<a href="'.url('privacy:stored').'">';
					echo 'Where your data are stored';
				echo '</a>';
			echo '</div>';



			echo '<h4>What the website stores on your device</h4>';
			echo '<p>When you change the theme, the data will be stored in the browser\'s '.link_('localStorage', 'https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage').'. This is neccesary to let the chosen theme to be still chosen when you navigate through the website. But if JavaScript are not allowed or activated in your browser, this will not work at all.</p>';
			echo '<p>The data in localStorage will not be deleted by itself, so you must delete it manually. Below, you can see if the website have stored some data locally.</p>';

			echo '<div class="explaination small-text">';
				echo '<h4>What if the difference between localStorage and cookies?</h4>';
				echo '<p>This website doesn\'t use any cookies. Instead it uses localStorage to store your user preferences (light or dark theme). Unlike cookies, localStorage does not send data back and forth to the server with every http request. The data is stored locally and by itself, local storage cannot do anything to or with the data stored on your device.</p>';
			echo '</div>';

			echo '<div class="localstorage">';
				echo '<div class="question"><b>Question:</b> Do the local data <code>theme</code> exists?</div>';
				echo '<div class="answer">';
					echo '<b>Answer:</b> ';

					echo '<span class="js-inactive">Please enable JavaScript to check</span>';
					echo '<span class="checking">Checking...</span>';
					echo '<span class="deleting">Delete...</span>';
					echo '<span class="answer-no">No (<a href="javascript:void(0)" id="check">check again</a>)</span>';
					echo '<span class="answer-yes">Yes (<a href="javascript:void(0)" id="check">check again</a> or <a href="javascript:void(0)" id="delete">delete</a>)</span>';
					echo '<span class="deleted">Deletion successful (<a href="javascript:void(0)" id="check">check again</a>)</div>';
				echo '</div>';
			echo '</div>';



			echo '<h4>Contact the blog owner</h4>';
			echo '<p>You can contact '.$blogowner.' via '.link_($config_domain, $protocol.'://'.$config_domain.'/en').'.</p>';

		}
	echo '</section>';







	require_once 'site-footer.php';

?>
