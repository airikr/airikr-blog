<?php

	require_once 'site-settings.php';



	$editing = false;
	if(isset($_GET['idp'])) {
		$editing = true;
		$id_unique = strip_tags(htmlspecialchars($_GET['idp']));

	} else {
		$id_unique = uniqid();
	}



	$form_subject = strip_tags(htmlspecialchars($_POST['field-subject']));
	$form_content = strip_tags(htmlspecialchars($_POST['field-content']));
	$form_tags = strip_tags(htmlspecialchars($_POST['field-tags']));
	$form_publish = strip_tags(htmlspecialchars($_POST['field-publish']));
	$form_check_1 = (isset($_POST['check-1']) ? 1 : null);
	$form_check_2 = (isset($_POST['check-2']) ? 1 : null);
	$form_check_3 = (isset($_POST['check-3']) ? 1 : null);
	$form_savepost = (isset($_POST['button-save']) ? true : false);

	$tags = explode(',', strip_tags(htmlspecialchars($_POST['field-tags'])));
	$array_content = explode('----', $form_content);
	$content = trim($form_content);
	$content_before = trim($array_content[0]);
	$content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);
	$cover_empty = true;



	if(!empty($_POST['field-cover'])) {
		$cover_empty = false;
		$unsplash_url = strip_tags(htmlspecialchars($_POST['field-cover']));
		$unsplash_owner = strip_tags(htmlspecialchars($_POST['field-cover-owner']));
		$unsplash_owner_url = strip_tags(htmlspecialchars($_POST['field-cover-owner-url']));
		$unsplash_hash = md5($unsplash_url);
		$unsplash_get = file_get_contents($unsplash_url);
		$file_dimensions = 890;
		$destination = 'covers/'.$unsplash_hash.'.jpg';

		if(!file_exists($destination)) {
			file_put_contents($destination, $unsplash_get);
			compress_image($destination, $file_dimensions);
		}

	} else {
		unlink('covers/'.$unsplash_hash.'.jpg');
	}



	if($editing == false) {
		sql("INSERT INTO posts(
				 id_unique,
				 subject,
				 cover_hash,
				 cover_url,
				 cover_owner,
				 cover_owner_url,
				 content_beforebreak,
				 content_afterbreak,
				 tags,
				 is_inenglish,
				 hasbeencorrected,
				 readers,
				 timestamp_saved,
				 timestamp_published,
				 timestamp_edited,
				 is_pinned
			 )

			 VALUES(
				 :_idunique,
				 :_subject,
				 :_coverhash,
				 :_coverurl,
				 :_coverowner,
				 :_coverowner_url,
				 :_content_before,
				 :_content_after,
				 :_tags,
				 :_is_inenglish,
				 :_hasbeencorrected,
				 :_readers,
				 :_saved,
				 :_published,
				 :_edited,
				 :_ispinned
			 )
			", Array(
				'_idunique' => $id_unique,
				'_subject' => (empty($form_subject) ? 'Ett namnlöst inlägg' : $form_subject),
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_tags' => (empty($form_tags) ? null : implode(',', $tags)),
				'_is_inenglish' => $form_check_3,
				'_hasbeencorrected' => $form_check_1,
				'_readers' => null,
				'_saved' => ($form_savepost == false ? null : time()),
				'_published' => (empty($form_publish) ? time() : strtotime($form_publish)),
				'_edited' => null,
				'_ispinned' => $form_check_2
			), 'insert');


	} else {
		sql("UPDATE posts
			 SET id_unique = :_idunique,
				 subject = :_subject,
				 cover_hash = :_coverhash,
				 cover_url = :_coverurl,
				 cover_owner = :_coverowner,
				 cover_owner_url = :_coverowner_url,
				 content_beforebreak = :_content_before,
				 content_afterbreak = :_content_after,
				 tags = :_tags,
				 is_inenglish = :_is_inenglish,
				 hasbeencorrected = :_hasbeencorrected,
				 readers = :_readers,
				 timestamp_saved = :_saved,
				 timestamp_published = :_published,
				 timestamp_edited = :_edited,
				 is_pinned = :_ispinned

			 WHERE id_unique = :_idunique
			", Array(
				'_idunique' => $id_unique,
				'_subject' => (empty($form_subject) ? 'Ett namnlöst inlägg' : $form_subject),
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_tags' => (empty($form_tags) ? null : implode(',', $tags)),
				'_is_inenglish' => $form_check_3,
				'_hasbeencorrected' => $form_check_1,
				'_readers' => null,
				'_saved' => ($form_savepost == false ? null : time()),
				'_published' => (empty($form_publish) ? time() : strtotime($form_publish)),
				'_edited' => ($form_savepost == false ? time() : null),
				'_ispinned' => $form_check_2
			));
	}


	if($form_savepost == false) {
		include 'generate-feed.php'.($viewing_in_english == false ? '' : '?lau=en');
	}



	header("Location: ".url(($editing == false ? ($form_savepost == false ? '' : 'saved-posts') : ($form_savepost == false ? 'read' : 'edit').':'.$id_unique)));
	exit;

?>
