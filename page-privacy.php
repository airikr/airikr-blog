<?php

	require_once 'site-header.php';



	$page = (!isset($_GET['pag']) ? null : strip_tags(htmlspecialchars($_GET['pag'])));

	$default_title = '<a href="'.url('privacy').'">';
		$default_title .= 'Integritetspolicy';
	$default_title .= '</a>';
	$default_title .= svgicon('title_arrow');







	echo '<section id="privacy">';
		if($page == 'collects') {

			echo '<h1>'.$default_title.'Vad webbsidan samlar in</h1>';

			echo '<p>'.$og_url_noprotocol.' lagrar inte någon uppgift om dig utan ditt godkännande. Om du godkänner dataanalysen (kräver JavaScript), kommer följande uppgifter att samlas in:</p>';

			echo '<ul>';
				echo '<li>Enhetens IP-adress.</li>';
				echo '<li>Webbläsarens användaragent.</li>';
				echo '<li>Vilka sidor du besöker.</li>';
				echo '<li>Tidstämpeln om när du godkände lagringen.</li>';
				echo '<li>Tidstämpeln när du besökte en sida.</li>';
			echo '</ul>';

			echo '<p>Se "<a href="'.url('privacy:uses').'">Behandling</a>" för att veta hur dina uppgifter lagras.</p>';
			echo '<p>Servern använder sig av Apache som webbserver och '.link_('deras lagring av data', 'https://httpd.apache.org/docs/2.4/logs.html').' är inaktiverad på servern.</p>';

			echo '<h4>Kommentering</h4>';
			echo '<p>Innan du skickar en kommentar, kommer en informationsruta att dyka upp. Läs den noga, för den berättar vad för uppgifter som lagras i och med din kommentar, och hur de lagras i databasen.</p>';
			echo '<p>Informationsrutan innehåller även en till kryssruta för kommentarens lösenord. Om du vill kunna hantera din kommentar <a href="'.url('privacy:duration').'">inom den angivna perioden</a>, så måste du spara ner lösenordet för din kommentar. '.$blogowner_capital.' rekommenderar att du sparar kommentarernas lösenord på lösenordshanteringstjänsten '.link_('Bitwarden', 'https://bitwarden.com').'.</p>';


			echo '<div class="pages one-link right">';
				echo '<a href="'.url('privacy:uses').'" class="right">';
					echo 'Behandling';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'uses') {

			echo '<h1>'.$default_title.'Hur webbsidan behandlar dina uppgifter</h1>';

			echo '<p>'.$og_url_noprotocol.' behandlar dina uppgifter på olika sätt. Nedan kan du se specifikt hur webbsidan hanterar dom. Du kan läsa var uppgifterna lagras <a href="'.url('privacy:stored').'">här</a>.</p>';


			echo '<h4>Enhetens IP-adress</h4>';
			echo '<p>Enhetens IP-adress kan länkas till en person och är därför klassad som en personuppgift. I och med detta, lagras adressen krypterad med '.link_('AES-256', 'https://sv.wikipedia.org/wiki/Advanced_Encryption_Standard').', där avkrypteringsnycklarna ägs av webbsidan och dess ägare. Nycklarna är endast synliga för '.$blogowner.', men en IP-adress kommer endast att avkrypteras när ett brott har skett (vilket förhoppningsvis aldrig kommer att ske).</p>';
			echo '<p>IP-adressen lagras i samband med när du kommenterar något inlägg, samt när du godkänner dataanalysen.</p>';
			echo '<p>Uppgiften kan endast ses av dig på "<a href="'.url('your-data').'">Din data</a>"-sidan. Du kan se enhetens IP-adress där i klartext endast på grund av att webbsidan hämtar den via PHP (källa '.link_('1', 'https://codeberg.org/airikr/airikr-blog/src/branch/main/page-yourdata.php#L72').' & '.link_('2', 'https://codeberg.org/airikr/airikr-blog/src/branch/main/site-functions.php#L655').').</p>';
			echo '<p><a href="'.url('privacy:duration').'">Läs mer</a> om hur länge IP-adressen sparas.</p>';

			echo '<h4>Webbläsarens användaragent</h4>';
			echo '<p>En användaragent visar vad för enhet, webbläsare och operativsystem du använde när du besökte webbsidan, vilket inte kan länkas till en person. Den anses därför inte som en personuppgift och kommer att lagras i klartext. Denna information är nödvändig för '.$blogowner.' för och göra webbsidan bättre.</p>';
			echo '<p>Användaragenten lagras endast när du godkänner dataanalysen.</p>';
			echo '<p>Uppgiften kan endast ses av dig på "<a href="'.url('your-data').'">Din data</a>"-sidan.</p>';

			echo '<h4>Ditt namn i en kommentar</h4>';
			echo '<p>Ditt namn '.link_('klassas som en personuppgift', 'https://www.imy.se/verksamhet/dataskydd/det-har-galler-enligt-gdpr/introduktion-till-gdpr/personuppgifter/').', men namnet sparas i klartext av följande anledning: uppgiften visas för alla som besöker webbsidan. Om du inte vill använda ditt riktiga namn, så kan du använda till exempel ditt smeknamn, men aldrig någon annans namn. Det alternativa namnet får inte heller innehålla något som kan anses vara stötande hos andra.</p>';
			echo '<p>Uppgiften visas för alla som läser kommentaren.</p>';

			echo '<h4>Din webbadress i en kommentar</h4>';
			echo '<p>Din webbadress är inte klassad som en personuppgift, så den lagrar i klartext.</p>';
			echo '<p>Uppgiften visas för alla som läser kommentaren.</p>';

			echo '<h4>Innehållet för en kommentar</h4>';
			echo '<p>Själva kommentaren i sig är inte klassad som en personuppgift och lagras därför i klartext. Men om en kommentar innehåller någons annans telefonnummer, e-postadress med personens namn (till exempel förnamn.efternamn@domän.se), hemadress, och/eller annat som '.link_('IMY listar som personuppgift', 'https://www.imy.se/verksamhet/dataskydd/det-har-galler-enligt-gdpr/introduktion-till-gdpr/personuppgifter/').' (även '.link_('känsliga personuppgifter', 'https://www.imy.se/verksamhet/dataskydd/det-har-galler-enligt-gdpr/introduktion-till-gdpr/personuppgifter/kansliga-personuppgifter/').'), kommer kommentaren att döljas från allmänheten av '.$blogowner.'. Det finns dock vissa undantag.</p>';
			echo '<p>Om en avsändare anger namnet på en person som redan har kommenterat samma inlägg (eller ett annat inlägg) i form av en förtydligande om att avsändaren menar denna person, så kommer '.$blogowner.' inte vidta några åtgärder. Men om avsändaren skriver till exempel "du kan nå [förnamn] på [telefonnummer]", så kommer kommentaren att döljas.</p>';
			echo '<p>Om det finns tillräckligt med data för att kunna kontakta den person som omnämns i kommentaren, så kommer '.$blogowner.' att kontakta personen och fråga om han eller hon är medveten om händelsen och om han eller hon har gett avsändaren ett godkännande.</p>';
			echo '<p>Man måste alltid fråga personen i fråga om lov först, innan man lämnar ut information om honom eller henne!</p>';
			echo '<p>Uppgiften visas för alla som läser kommentaren.</p>';

			echo '<h4>Andra uppgifter</h4>';
			echo '<p>Om du godkänner att '.$og_url_noprotocol.' får lagra uppgifter om dig, kommer bland annat de sidor du besöker, när du godkände frågan om lagring och när du besökte sidan att lagras. Alla dessa uppgifter är inte klassade som personuppgifter och på grund av det, kommer de att lagras i klartext.</p>';
			echo '<p>Uppgifterna kan endast ses av dig på "<a href="'.url('your-data').'">Din data</a>"-sidan.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:collects').'">';
					echo 'Insamling';
				echo '</a>';

				echo '<a href="'.url('privacy:duration').'" class="right">';
					echo 'Varaktivhet';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'duration') {

			echo '<h1>'.$default_title.'Hur länge dina uppgifter ligger lagrade</h1>';

			echo '<p>IP-adresser som har lagrats i och med dataanalysen, tillsammans med allt annat som har lagrats ihop med den IP-adressen, kommer att tas bort från databasen 1 månad efter att datan lagrades.</p>';
			echo '<p>IP-adresser som är länkade till kommentarer, kommer att tas bort 2 veckor efter att kommentaren publicerades. Efter detta, kommer avsändaren inte kunna ändra kommentaren. Han eller hon måste då '.$blogowner_contact.' om en redigering önskas.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:uses').'">';
					echo 'Behandling';
				echo '</a>';

				echo '<a href="'.url('privacy:access').'" class="right">';
					echo 'Åtkomst';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'access') {

			echo '<h1>'.$default_title.'Vilka personer som har tillgång till dina uppgifter</h1>';

			echo '<p>Endast '.$blogowner.' kan logga in på servern som håller databasen för '.$og_url_noprotocol.'. Han kan se de krypterade strängarna för IP-adresserna och han har även tillgång till avkrypteringsnycklarna, men integritet är inte lika med överträdelse. IP-adresserna är krypterade av en anledning.</p>';
			echo '<p>Den <a href="'.url('privacy:uses').'">data som är offentliga</a>, kan ses av alla som besöker webbsidan.</p>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:duration').'">';
					echo 'Varaktivhet';
				echo '</a>';

				echo '<a href="'.url('privacy:rights').'" class="right">';
					echo 'Rättigheter';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'rights') {

			echo '<h1>'.$default_title.'Vad du har för rättigheter</h1>';

			echo '<h4 class="first">Automatisk datainsamling</h4>';
			echo '<p>Du har rätten till att neka datainsamling så att ingenting samlas in om dig. Om du accepterar dataanlyseringen, så har du rätten till att begära borttagning av all data som har samlats in om dig.</p>';
			echo '<p>Det smidigaste sättet att göra detta, är att gå till "<a href="'.url('your-data').'">Din data</a>"-sidan. Du kan även kontakta '.$blogowner.' för att begära borttagning av data, men det är rekommenderat att gå till "Din data"-sidan istället.</p>';
			echo '<p>Om du kontaktar '.$blogowner.' gällande borttagning av data, kommer han att behöva veta vad för IP-adress enheten hade när du besökte webbsidan, alternativt vilken enhet, webbläsare och operativsystem du använde när du besökte webbsidan, inklusive när du besökte någon sida på '.$og_url_noprotocol.'.</p>';
			echo '<p>Med andra ord är det enklare för både dig och bloggägaren om du tog bort din data själv eller att du väntar på att all din data raderas automatiskt efter <a href="'.url('privacy:duration').'">den angivna perioden</a>.</p>';

			echo '<h4>Självman datainsamling</h4>';
			echo '<p>Du har rätten till att ta bort de kommentarer som du har skickat genom att ange kommentarens lösenord. Om du inte har tillgång till lösenordet och har angett din webbadress i kommentaren, kan du '.$blogowner_contact.' och låta honom ta bort din kommentar på följande villkor:</p>';

			echo '<ul>';
				echo '<li>Skapa ett tillfälligt blogginlägg på din webbsida där du skriver att du kommenterade ett inlägg på '.$og_url_noprotocol.' (ange även vilket inlägg du kommenterade) och ange direktlänken till kommentaren.</li>';
				echo '<li>Lägg till en textsträng i källkoden för din webbsida som bloggägaren kan se när han besöker din webbsida. Han kommer att ge dig en specifik textsträng some du har synligt på webbsidan eller som en kommentar i källkoden som kan ses i webbläsarens källkodsvisare.</li>';
			echo '</ul>';


			echo '<div class="pages">';
				echo '<a href="'.url('privacy:access').'">';
					echo 'Åtkomst';
				echo '</a>';

				echo '<a href="'.url('privacy:stored').'" class="right">';
					echo 'Lagring';
				echo '</a>';
			echo '</div>';



		} elseif($page == 'stored') {

			echo '<h1>'.$default_title.'Var dina uppgifter är lagrade</h1>';

			echo '<p>Servern som '.$og_url_noprotocol.' ligger på är lokaliserad hemma hos '.$blogowner.' i Sverige.</p>';
			echo '<p>Webbsidan använder sig av '.link_('bunny.net', 'https://bunny.net').' som CDN för att låta filer som inte uppdateras ofta att vara cachade, men varken dom eller andra tredjepartsföretag kan nå eller använda din data.</p>';


			echo '<div class="pages one-link">';
				echo '<a href="'.url('privacy:rights').'">';
					echo 'Rättigheter';
				echo '</a>';
			echo '</div>';



		} else {

			echo '<h1>Integritetspolicy</h1>';

			echo '<p>Den här sidan berättar vad och hur '.$og_url_noprotocol.' samlar in om dig och ditt besök. Det är viktigt för '.$blogowner.' att du läser igenom allt och att du även förstår integritetspolicyn.</p>';


			echo '<div class="links">';
				echo '<a href="'.url('privacy:collects').'">';
					echo 'Vad webbsidan samlar in';
				echo '</a>';

				echo '<a href="'.url('privacy:uses').'">';
					echo 'Hur webbsidan behandlar dina uppgifter';
				echo '</a>';

				echo '<a href="'.url('privacy:duration').'">';
					echo 'Hur länge dina uppgifter ligger lagrade';
				echo '</a>';

				echo '<a href="'.url('privacy:access').'">';
					echo 'Vilka som har tillgång till dina uppgifter';
				echo '</a>';

				echo '<a href="'.url('privacy:rights').'">';
					echo 'Vad du har för rättigheter';
				echo '</a>';

				echo '<a href="'.url('privacy:stored').'">';
					echo 'Var dina uppgifter är lagrade';
				echo '</a>';
			echo '</div>';



			echo '<h4>Vad webbsidan lagrar på din enhet</h4>';
			echo '<p>När du ändrar temat på bloggen, kommer ditt val att lagras med hjälp av '.link_('localStorage', 'https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage').' i webbläsaren. Den här metoden är den enklaste för att låta ditt val vara kvar när du navigerar runt på webbsidan. Dock kräver detta JavaScript, och om du inte tillåter JavaScript i din webbläsare, så kommer webbsidan inte kunna spara ditt val.</p>';

			echo '<div class="explaination small-text">';
				echo '<h4>Vad är skillnaden mellan localStorage och kakor?</h4>';
				echo '<p>Webbsidan använder inga kakor. Istället använder den localStorage för att lagra dina inställningar (lujst eller mörkt tema). Till skillnad från kakor skickar inte localStorage någon data fram och tillbaka till servern vid varje HTTP-anrop. Datan är lagrad lokalt och kan inte hämta eller använda den data som finns lagrad på din enhet.</p>';
			echo '</div>';

			echo '<p>Datan i webbläsarens lokala utrymmet tas inte bort av sig själv, så du måste ta bort den manuellt. Nedan kan du se om webbsidan har sparat data i webbläsarens lokala utrymme, eller inte.</p>';

			echo '<div class="localstorage">';
				echo '<div class="question"><b>Fråga:</b> Finns den lokala datan <code>theme</code>?</div>';
				echo '<div class="answer">';
					echo '<b>Svar:</b> ';

					echo '<span class="js-inactive">Aktivera javaScript för att kolla</span>';
					echo '<span class="checking">Kollar...</span>';
					echo '<span class="deleting">Tar bort...</span>';
					echo '<span class="answer-no">Nej (<a href="javascript:void(0)" id="check">kolla igen</a>)</span>';
					echo '<span class="answer-yes">Ja (<a href="javascript:void(0)" id="check">kolla igen</a> eller <a href="javascript:void(0)" id="delete">ta bort</a>)</span>';
					echo '<span class="deleted">Borttagningen lyckades (<a href="javascript:void(0)" id="check">kolla igen</a>)</div>';
				echo '</div>';
			echo '</div>';



			echo '<h4>Kontakta bloggägaren</h4>';
			echo '<p>Du kan kontakta '.$blogowner.' via '.link_($config_domain, $protocol.'://'.$config_domain).'.</p>';

		}
	echo '</section>';







	require_once 'site-footer.php';

?>
