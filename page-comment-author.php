<?php

	require_once 'site-header.php';



	$is_unique = false;

	if(isvalid_md5(strip_tags(htmlspecialchars($_GET['ide'])))) {
		$is_unique = true;
		$identity = strip_tags(htmlspecialchars($_GET['ide']));

		$author = sql("SELECT visitor_name
					   FROM comments
					   WHERE comment_identity = :_identity
					   LIMIT 1
					  ", Array(
						  '_identity' => $identity
					  ), 'fetch');

		$get_comments = sql("SELECT *,
									c.id AS id_c,
									p.id AS id_p

							 FROM comments AS c
							 JOIN posts AS p
							 ON c.id_post = p.id
							 WHERE c.comment_identity = :_identity
							 AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY c.timestamp_published DESC
							", Array(
								'_identity' => $identity
							));

		$c_comments = sql("SELECT COUNT(visitor_name)
						   FROM comments AS c
						   JOIN posts AS p
						   ON c.id_post = p.id
						   WHERE c.comment_identity = :_identity
						   AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
						  ", Array(
							  '_identity' => $identity
						  ), 'count');

		$p_mostcommented = sql("SELECT p.id_unique, p.subject, COUNT(visitor_name) AS c_comments
								FROM posts AS p
								JOIN comments AS c
								ON p.id = c.id_post
								WHERE c.comment_identity = :_identity
								AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
								GROUP BY p.id, p.subject
								ORDER BY c_comments DESC, p.subject ASC
								LIMIT 1
		 					   ", Array(
		 						   '_identity' => $identity
		 					   ), 'fetch');


	} else {
		$name = strip_tags(htmlspecialchars(ucwords(str_replace('-', ' ', $_GET['ide']))));
		$author = sql("SELECT visitor_name
					   FROM comments
					   WHERE visitor_name LIKE :_name
					   LIMIT 1
					  ", Array(
						  '_name' => '%'.$name.'%'
					  ), 'fetch');

		$get_comments = sql("SELECT *,
									c.id AS id_c,
									p.id AS id_p

							 FROM comments AS c
							 JOIN posts AS p
							 ON c.id_post = p.id
							 WHERE c.visitor_name LIKE :_name
							 AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							 ORDER BY c.timestamp_published DESC
							", Array(
								'_name' => '%'.$name.'%'
							));

		$c_comments = sql("SELECT COUNT(visitor_name)
						   FROM comments AS c
						   JOIN posts AS p
						   ON c.id_post = p.id
						   WHERE c.visitor_name LIKE :_name
						   AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
						  ", Array(
							  '_name' => '%'.$name.'%'
						  ), 'count');

		$p_mostcommented = sql("SELECT p.id_unique, p.subject, COUNT(visitor_name) AS c_comments
								FROM posts AS p
								JOIN comments AS c
								ON p.id = c.id_post
								WHERE c.visitor_name LIKE :_name
								AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
								GROUP BY p.id, p.subject
								ORDER BY c_comments DESC, p.subject ASC
								LIMIT 1
		 					   ", Array(
		 						   '_name' => '%'.$name.'%'
		 					   ), 'fetch');
	}







	echo '<section id="comment-author">';
		echo '<h1>'.($viewing_in_english == false ? 'Kommentarer från' : 'Comments from').' '.$author['visitor_name'].'</h1>';

		if($c_comments == 0) {
			echo '<div class="message">';
				echo ($viewing_in_english == false ? 'Kunde inte hitta några kommentarer' : 'Couldn\'t find any comments');
			echo '</div>';


		} else {
			if($is_unique == false) {
				echo '<div class="not-unique small-text">';
					if($viewing_in_english == false) {
						echo '<p>De kommentarer som är listade nedan, kan vara från fler än en person, då det är väldigt enkelt för andra att säga sig vara en viss person här. En lösning på detta är på planeringsbordet.</p>';

					} else {
						echo '<p>The listed comments below can be from more than 1 person. This is due to the fact that it is very easy to impersonate another person. A solution on this are on the planning board.</p>';
					}
				echo '</div>';
			}


			echo '<div>';
				echo '<div class="list">';
					foreach($get_comments AS $comment) {
						$url = (empty($comment['visitor_url']) ? null : parse_url($comment['visitor_url']));
						$identity = (!empty($comment['comment_identity']) ? $comment['comment_identity'] : strtolower(str_replace(' ', '-', trim($comment['visitor_name']))));

						$post = sql("SELECT *
									 FROM posts
									 WHERE id = :_idpost
									", Array(
										'_idpost' => (int)$comment['id_p']
									), 'fetch');



						echo '<div class="item">';

							echo '<div class="post">';
								echo '<a href="'.url('read:'.$post['id_unique']).'">';
									echo $post['subject'];
								echo '</a>';
							echo '</div>';

							echo '<div class="top">';
								echo '<div class="id">';
									echo '<a href="'.url('read:'.$post['id_unique'].'#'.(int)$comment['id_c']).'">';
										echo '#'.(int)$comment['id_c'];
									echo '</a>';
								echo '</div>';

								echo '<div class="information" id="'.(int)$comment['id'].'">';
									echo '<div>';
										echo '<div class="name">';
											echo '<a href="'.url('comment-author:'.$identity).'" title="Se alla kommentarer från '.trim($comment['visitor_name']).'">';
												echo trim($comment['visitor_name']);
											echo '</a>';
										echo '</div>';

										if($url != null) {
											echo '<div class="url">';
												echo link_($url['host'] . (isset($url['path']) ? '<span class="path">'.$url['path'].'</span>' : ''), $comment['visitor_url']);
											echo '</div>';
										}
									echo '</div>';

									echo '<div>';
										echo '<div class="published small-text">';
											echo date_($comment['timestamp_published'], 'full');
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</div>';


							echo '<div class="comment">';
								echo preg_replace('/(?<!\S)#([0-9a-zA-Z]+)/', '<a href="javascript:void(0)" class="link-id" data-idcomment="$1">#$1</a>', $Parsedown->text(emoji_convert($comment['visitor_comment'])));
							echo '</div>';

						echo '</div>';
					}
				echo '</div>';



				echo '<div class="statistics small-text">';
					echo '<h2>'.($viewing_in_english == false ? 'Statistik' : 'Statistics').'</h2>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Antalet kommentarer' : 'Number of comments');
						echo '</div>';

						echo '<div class="value">';
							echo format_number($c_comments, 0);
						echo '</div>';
					echo '</div>';

					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Mest kommenterade inlägget' : 'Most commented post');
						echo '</div>';

						echo '<div class="value">';
							echo '<a href="'.url('read:'.$p_mostcommented['id_unique']).'">';
								echo $p_mostcommented['subject'];
							echo '</a>';
							echo ' ('.$p_mostcommented['c_comments'].')';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}
	echo '</section>';







	require_once 'site-footer.php';

?>
