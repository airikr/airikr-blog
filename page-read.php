<?php

	if(isset($_POST['button-sendtoot'])) {

		require_once 'site-settings.php';

		$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
		$post = sql("SELECT *, id AS id_post, COUNT(id_unique) AS p_exists
					 FROM posts
					 WHERE id_unique = :_idunique
					 GROUP BY id
					", Array(
						'_idunique' => $id_unique
					), 'fetch');

		$subject = '"'.$post['subject'].'"';
		$url = $og_url.'/read:'.$id_unique;

		header("Location: ".$_POST['field-instance']."/share?text=".urlencode($subject).'%0A%0A'.$url.'%0A%0A'.implode('%20', $arr_tags));
		exit;



	} else {

		require_once 'site-header.php';



		$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
		$managing_comment = (isset($_GET['mge']) ? true : false);
		$do_exists = 0;
		$error = (isset($_GET['err']) ? strip_tags(htmlspecialchars($_GET['err'])) : null);

		$post_exists = sql("SELECT COUNT(id_unique)
							FROM posts
							WHERE id_unique = :_idunique
							".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
						   ", Array(
							   '_idunique' => $id_unique
						   ), 'count');

		$edit_exists = sql("SELECT COUNT(id_post)
							FROM posts
							WHERE id_post = :_idpost
						   ", Array(
							   '_idpost' => (int)$post['id']
						   ), 'count');

		if($post_exists != 0 AND $edit_exists == 0) {
			$post = sql("SELECT *
						 FROM posts
						 WHERE id_unique = :_idunique
						 ".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
						", Array(
							'_idunique' => $id_unique
						), 'fetch');

		} elseif($post_exists != 0 AND $edit_exists != 0) {
			$post = sql("SELECT *
						 FROM posts
						 WHERE id_post = :_idpost
						 ".($session == true ? "" : "AND FROM_UNIXTIME(timestamp_published) < NOW()")."
						 ORDER BY timestamp_edited DESC
						", Array(
							'_idpost' => (int)$post['id']
						), 'fetch');
		}



		if(!isset($_GET['shr'])) {

			/*
			if($post_exists != 0) {
				$c_comments = sql("SELECT COUNT(id_post)
								   FROM comments
								   WHERE id_post = :_idpost
								  ", Array(
									  '_idpost' => (int)$post['id']
								  ), 'count');


				if($c_comments != 0) {
					if($managing_comment == true) {
						$comment_id = strip_tags(htmlspecialchars($_GET['mge']));

						$do_exists = sql("SELECT COUNT(id)
										  FROM comments
										  WHERE id = :_idcomment
										  AND id_post = :_idpost
										 ", Array(
											 '_idcomment' => (int)$comment_id,
											 '_idpost' => (int)$post['id']
										 ), 'count');

						if($do_exists != 0) {
							$manage = sql("SELECT *
										   FROM comments
										   WHERE id = :_idcomment
										   AND id_post = :_idpost
										  ", Array(
											  '_idcomment' => (int)$comment_id,
											  '_idpost' => (int)$post['id']
										  ), 'fetch');

							$no_ipaddress = (empty($manage['visitor_ipaddress']) ? true : false);


						} else {
							header("Location: ".url('read:'.$id_unique));
							exit;
						}
					}
				}
			}
			*/



			if($post_exists != 0 AND $session == false AND $post['timestamp_saved'] != null OR
			   $post_exists == 0 OR
			   $post['is_inenglish'] == 0 AND $viewing_in_english == true OR
			   $post['is_inenglish'] == 1 AND $viewing_in_english == false) {
				echo '<div class="message color-red">';
					echo ($viewing_in_english == false ? 'Kunde inte hitta inlägget' : 'Couldn\'t find the blog post');
				echo '</div>';


			} else {

				/* if($post['timestamp_saved'] == null) {
					sql("UPDATE posts
						 SET readers = :_readers
						 WHERE id = :_idpost
						", Array(
							'_idpost' => (int)$post['id'],
							'_readers' => $post['readers'] + 1
						));
				} */


				$subject = $post['subject'];
				$cover_url = $post['cover_url'];
				$cover_hash = $post['cover_hash'];
				$cover_attribution = $post['cover_owner'];
				$content_beforebreak = $post['content_beforebreak'];
				$content_afterbreak = $post['content_afterbreak'];
				$hasbeencorrected = $post['hasbeencorrected'];
				$published = $post['timestamp_published'];
				$edited = $post['timestamp_edited'];
				$reading_time = round((str_word_count(strip_tags($content_beforebreak . $content_afterbreak)) / 200));

				$get_tags = sql("SELECT t.name
								 FROM tags AS t
								 JOIN tags_linked AS tl
								 ON t.id = tl.id_tag
								 WHERE tl.id_post = :_idpost
								 ORDER BY t.name ASC
								", Array(
									'_idpost' => (int)$post['id']
								));


				$c_previous = sql("SELECT COUNT(id)
								   FROM posts
								   WHERE id = (
									   SELECT MIN(id)
									   FROM posts
									   WHERE id > :_idpost
									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									   AND timestamp_saved IS NULL
									   AND FROM_UNIXTIME(timestamp_published) < NOW()
								   )
								   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
								   LIMIT 1
								  ", Array(
									  '_idpost' => (int)$post['id']
								  ), 'count');

				if($c_previous != 0) {
					$post_previous = sql("SELECT id_unique, subject, cover_hash
										  FROM posts
										  WHERE id = (
	   									   SELECT MIN(id)
	   									   FROM posts
	   									   WHERE id > :_idpost
	   									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										   AND timestamp_saved IS NULL
	   									   AND FROM_UNIXTIME(timestamp_published) < NOW()
	   								   )
		   								  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										  LIMIT 1
										 ", Array(
											 '_idpost' => (int)$post['id']
										 ), 'fetch');
				}

				$c_next = sql("SELECT COUNT(id)
							   FROM posts
							   WHERE id = (
								   SELECT MAX(id)
								   FROM posts
								   WHERE id < :_idpost
								   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
								   AND timestamp_saved IS NULL
								   AND FROM_UNIXTIME(timestamp_published) < NOW()
							   )
							   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
							   LIMIT 1
							  ", Array(
								  '_idpost' => (int)$post['id']
							  ), 'count');

				if($c_next != 0) {
	 				$post_next = sql("SELECT id_unique, subject, cover_hash
	 								  FROM posts
	 								  WHERE id = (
	   									  SELECT MAX(id)
	   									  FROM posts
	   									  WHERE id < :_idpost
	   									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
   										  AND timestamp_saved IS NULL
	   									  AND FROM_UNIXTIME(timestamp_published) < NOW()
	   								  )
									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
	 								  LIMIT 1
	 								 ", Array(
	 									 '_idpost' => (int)$post['id']
	 								 ), 'fetch');
				}


				$get_similartags = sql("SELECT t.name
										FROM tags AS t
										JOIN tags_linked AS tl
										ON t.id = tl.id_tag
										WHERE tl.id_post = :_idpost
										ORDER BY t.name ASC
									   ", Array(
										   '_idpost' => (int)$post['id']
									   ));

				$arr_tags = [];
				foreach($get_similartags AS $similartag) {
					$arr_tags[] = "p.id_unique != :_idunique AND t.name = '".$similartag['name']."' AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL AND p.timestamp_saved IS NULL AND FROM_UNIXTIME(timestamp_published) < NOW()";
				}


				/*if(!empty($arr_tags)) {
					$c_similarposts = sql("SELECT COUNT(DISTINCT p.id)
										   FROM posts AS p
										   JOIN tags_linked AS tl
										   ON p.id = tl.id_post
										   JOIN tags AS t
										   ON tl.id_tag = t.id
										   WHERE ".implode($arr_tags, ' OR ')."
										  ", Array(
											  '_idunique' => $id_unique
										  ), 'count');

					if($c_similarposts != 0) {
						$get_similarposts = sql("SELECT DISTINCT p.id_unique, p.subject, p.cover_hash
												 FROM posts AS p
												 JOIN tags_linked AS tl
												 ON p.id = tl.id_post
												 JOIN tags AS t
												 ON tl.id_tag = t.id
												 WHERE ".implode($arr_tags, ' OR ')."
												 ORDER BY RAND()
												 LIMIT 10
												", Array(
													'_idunique' => $id_unique
												));
					}
				}*/


				$c_comments = sql("SELECT COUNT(id_post)
								   FROM comments
								   WHERE id_post = :_idpost
								  ", Array(
									  '_idpost' => (int)$post['id']
								  ), 'count');

				$c_webmention = sql("SELECT COUNT(id_post)
									 FROM webmention
									 WHERE id_post = :_idpost
									 ", Array(
										 '_idpost' => $id_unique
									 ), 'count');

				$c_edits = sql("SELECT *
								FROM public_edits
								WHERE id_post = :_idpost
							   ", Array(
								   '_idpost' => (int)$post['id']
							   ), 'count');


				if($c_edits != 0) {
					$get_edits = sql("SELECT *
									  FROM public_edits
									  WHERE id_post = :_idpost
									 ", Array(
										 '_idpost' => (int)$post['id']
									 ));
				}


				if($c_webmention != 0) {
					$c_likes = sql("SELECT COUNT(id_post)
									FROM webmention
									WHERE id_post = :_idpost
									AND type = 'like'
								   ", Array(
									   '_idpost' => $id_unique
								   ), 'count');

					$c_reposts = sql("SELECT COUNT(id_post)
									  FROM webmention
									  WHERE id_post = :_idpost
									  AND type = 'repost'
									 ", Array(
										 '_idpost' => $id_unique
									 ), 'count');

  					$c_replies = sql("SELECT COUNT(id_post)
  									  FROM webmention
  									  WHERE id_post = :_idpost
  									  AND type = 'reply'
  									 ", Array(
  										 '_idpost' => $id_unique
  									 ), 'count');


					if($c_likes != 0) {
						$get_likes = sql("SELECT *
										  FROM webmention
										  WHERE id_post = :_idpost
										  AND type = 'like'
										 ", Array(
											 '_idpost' => $id_unique
										 ));
					}

					if($c_reposts != 0) {
						$get_reposts = sql("SELECT *
											FROM webmention
											WHERE id_post = :_idpost
											AND type = 'repost'
										   ", Array(
											   '_idpost' => $id_unique
										   ));
					}

					if($c_replies != 0) {
						$get_replies = sql("SELECT *
											FROM webmention
											WHERE id_post = :_idpost
											AND type = 'reply'
											ORDER BY webmention_received ASC
										   ", Array(
											   '_idpost' => $id_unique
										   ));
					}
				}



				echo '<section id="read" data-idpost="'.(int)$post['id'].'">';

					echo '<div class="popup"><div><div>';
						echo '<div class="content">';
							if($viewing_in_english == false) {

								echo '<p>Om du väljer att fortsätta, kommer webbsidan att lagra följande:</p>';

								echo '<ul>';
									echo '<li>Ditt namn i klartext.</li>';
									echo '<li class="url">Din webbadress i klartext.</li>';
									echo '<li>Din kommentar i klartext.</li>';
									echo '<li>Enhetens IP-adress ('.getip().') krypterad med AES-256 med webbsidans egna krypteringsnyckel.</li>';
								echo '</ul>';

								echo '<p>Informationen är lagrade fram tills du själv tar bort kommentaren eller ändrar någon uppgift. Använd det lösenord som webbsidan har gett dig för att ändra eller ta bort din kommentar.</p>';
								echo '<p>'.link_('Läs mer', url('privacy')).'.</p>';


							} else {

								echo '<p>If you choose to continue, the website will store the following:</p>';

								echo '<ul>';
									echo '<li>Your name in plain text.</li>';
									echo '<li class="url">Your URL in plain text.</li>';
									echo '<li>Your comment in plain text.</li>';
									echo '<li>The device\'s IP address ('.getip().') encrypted with AES-256 with the website\'s own encryption key.</li>';
								echo '</ul>';

								echo '<p>The information will be stored until you delete the comment or change any data. Use the password that the website provided to you to change or delete your comment.</p>';
								echo '<p>'.link_('Read more', url('privacy')).'.</p>';

							}
						echo '</div>';



						echo '<footer>';
							echo '<div class="options">';
								echo '<div class="checkboxes small-text">';
									echo '<div class="message-popup"></div>';

									echo '<div class="pretty p-default p-round p-thick">';
										echo '<input type="checkbox" name="check-1" id="check-1">';
										echo '<div class="state p-primary-o">';
											echo '<label for="check-1">';
												echo ($viewing_in_english == false ? 'Jag godkänner att webbsidan lagrar mina uppgifter' : 'I agree that the website stores my data');
											echo '</label>';
										echo '</div>';
									echo '</div>';

									echo '<div class="pretty p-default p-round p-thick">';
										echo '<input type="checkbox" name="check-2" id="check-2">';
										echo '<div class="state p-primary-o">';
											echo '<label for="check-2">';
												echo ($viewing_in_english == false ? 'Jag har sparat kommentarens lösenord' : 'I have saved the password for my comment');
											echo '</label>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</div>';


							echo '<div class="button">';
								echo '<input type="submit" name="button-send" value="'.($viewing_in_english == false ? 'Skicka min kommentar' : 'Send my comment').'">';

								echo '<a href="javascript:void(0)" id="close" class="small-text">';
									echo ($viewing_in_english == false ? 'Stäng' : 'Close');
								echo '</a>';
							echo '</div>';
						echo '</footer>';
					echo '</div></div></div>';



					echo '<article class="h-entry item read">';
						echo '<header>';
							echo '<h2 class="p-name">';
								echo '<a href="'.url('read:'.$id_unique).'">';
									echo $subject;
								echo '</a>';
							echo '</h2>';


							echo '<div class="info small-text">';
								echo '<div class="dt-published datetime">';
									if($post['timestamp_saved'] != null) {
										echo ($viewing_in_english == false ? 'Sparades den' : 'Saved on').' ';
										echo '<time datetime="'.date('Y-m-d\TH:i:s', $post['timestamp_saved']).'+01:00">';
											echo date_($post['timestamp_saved'], 'full');
										echo '</time>';

									} else {
										echo '<time datetime="'.date('Y-m-d\TH:i:s', $post['timestamp_published']).'+01:00">';
											echo date_($post['timestamp_published'], 'full');
										echo '</time>';
									}
								echo '</div>';

								/*
								echo '<div class="delimiter">_</div>';

								echo '<div class="reading-time'.($tags == null ? ' notags read' : '').'">';
									echo ($reading_time == 0 ? 'Under en minuts lästid' : 'Cirka '.$reading_time.' '.($reading_time == 1 ? 'minut' : 'minuter').'s lästid');
								echo '</div>';
								*/


								if($post['timestamp_saved'] == null) {
									echo '<div class="delimiter comments">_</div>';

									echo '<div class="comments">';
										echo '<a href="javascript:void(0)" id="go-to-comments">';
											echo number_format((int)$c_comments, 0, ',', ' ').' ';

											if($viewing_in_english == false) {
												echo ((int)$c_comments == 1 ? 'kommentar' : 'kommentarer');
											} else {
												echo ((int)$c_comments == 1 ? 'comment' : 'comments');
											}
										echo '</a>';
									echo '</div>';
								}


								if($session == true AND $user['is_admin'] == true) {
									echo '<div class="delimiter">_</div>';

									echo '<div class="admin">';
										echo '<a href="'.url('edit:'.$post['id_unique']).'">';
											echo ($viewing_in_english == false ? 'Hantera' : 'Manage');
										echo '</a>';
									echo '</div>';
								}
							echo '</div>';


							echo '<div class="tags small-text">';
								foreach($get_tags AS $tag) {
									echo '<a href="'.url('tag:'.$tag['name']).'">#'.$tag['name'].'</a>';
								}
							echo '</div>';


							if($edited != null) {
								echo '<div class="edited small-text">';
									echo ($edit_exists == 0 ? '' : '<a href="'.url('diff:'.$id_unique).'">');
										echo ($viewing_in_english == false ? 'Redigerades den' : 'Edited').' <span class="dt-updated">'.date_($edited, 'full').'</span>';
									echo ($edit_exists == 0 ? '' : '</a>');
								echo '</div>';
							}
						echo '</header>';


						if($cover_hash != null) {
							echo '<figure'.($cover_attribution == null ? '' : ' id="with-attribution"').'><img id="cover" src="'.url('cover:'.$cover_hash, true).'" loading="lazy" alt="'.($viewing_in_english == false ? 'Omslagsbild' : 'Cover').'" class="u-photo"></figure>';

							if($cover_attribution != null) {
								echo '<div class="attribution small-text">';
									echo link_(($viewing_in_english == false ? 'Foto av' : 'Photo by').' '.$post['cover_owner'], $post['cover_owner_url']);
								echo '</div>';
							}
						}


						echo '<main'.($cover_hash == null ? ' class="nocover"' : '').'>';
							if($hasbeencorrected == false) {
								echo '<div class="has-not-been-corrected small-text">';
									echo ($viewing_in_english == false ? 'Inlägget har inte korrigerats än. Stavfel, särskrivning och annat kan förekomma.' : 'The blog post hasn\'t been corrected yet. Spelling errors, missing sources, and other stuff can occur.');
								echo '</div>';
							}

							echo '<div class="p-summary'.(!empty($content_afterbreak) ? '' : ' normal').'">'.$Parsedown->text(emoji_convert($content_beforebreak)).'</div>';
							echo '<div class="e-content">'.$Parsedown->text(emoji_convert(checkaftertoots($content_afterbreak))).'</div>';
						echo '</main>';


						echo '<div class="suggestions">';
							echo '<div class="make-an-edit small-text">';
								echo ($viewing_in_english == false ? 'Har du till exempel hittat stavfel?' : 'Have you for an example found a spelling error?').' ';
								echo '<a href="'.url('read:'.$id_unique.'/edit').'">';
									echo ($viewing_in_english == false ? 'Gör en rättelse' : 'Make a correction');
								echo '</a>.';
							echo '</div>';

							if($c_edits != 0) {
								echo '<div class="edits small-text">';
									echo '<div>'.($viewing_in_english == false ? 'Förslag' : 'Suggestions').':</div>';
									foreach($get_edits AS $edit) {
										echo '<div><a href="'.url('diff:'.$id_unique.'/'.$edit['timestamp_sent']).'">';
											echo date_($edit['timestamp_sent'], 'short');
										echo '</a></div>';
									}
								echo '</div>';
							}
						echo '</div>';
					echo '</article>';















					if($post['timestamp_saved'] == null) {
						echo '<div class="share">';
							echo '<div class="icon telegram">';
								echo link_(svgicon('telegram'), 'https://t.me/share/url?url='.$og_url.'/read:'.$id_unique);
							echo '</div>';

							echo '<div class="icon mastodon">';
								echo link_(svgicon('mastodon'), url('read:'.$id_unique.'/share:mastodon'));
							echo '</div>';
						echo '</div>';



						echo '<div class="paging">';
							echo '<div class="previous">';
								echo '<h3 class="no-select">'.($viewing_in_english == false ? 'Föregående' : 'Previous').'</h3>';

								if($c_previous == 0) {
									echo '<div class="nothing no-select">';
										echo '<div class="cover"></div>';
									echo '</div>';

								} else {
									echo '<a href="'.url('read:'.$post_previous['id_unique']).'" id="previous">';
										echo (empty($post_previous['cover_hash']) ? '<div class="no-cover"></div>' : '<figure><img id="cover" src="'.url('cover:'.$post_previous['cover_hash'], true).'" loading="lazy" alt="'.($viewing_in_english == false ? 'Omslagsbild' : 'Cover').'"></figure>');
										echo '<div>'.$post_previous['subject'].'</div>';
									echo '</a>';
								}
							echo '</div>';


							echo '<div class="next">';
								echo '<h3 class="no-select">'.($viewing_in_english == false ? 'Nästa' : 'Next').'</h3>';

								if($c_next == 0) {
									echo '<div class="nothing no-select">';
										echo '<div class="cover"></div>';
									echo '</div>';

								} else {
									echo '<a href="'.url('read:'.$post_next['id_unique']).'" id="next">';
										echo (empty($post_next['cover_hash']) ? '<div class="no-cover"></div>' : '<figure><img id="cover" src="'.url('cover:'.$post_next['cover_hash'], true).'" loading="lazy" alt="'.($viewing_in_english == false ? 'Omslagsbild' : 'Cover').'"></figure>');
										echo '<div>'.$post_next['subject'].'</div>';
									echo '</a>';
								}
							echo '</div>';
						echo '</div>';



						/*echo '<div class="similar">';
							if(empty($arr_tags) OR $c_similarposts == 0) {
								echo '<div class="message">';
									echo ($viewing_in_english == false ? 'Kunde inte hitta några liknande inlägg' : 'Couldn\'t find any similar posts');
								echo '</div>';

							} else {
								echo '<h4 class="first">'.($viewing_in_english == false ? 'Inlägg med samma taggar som denna' : 'Posts with the same tags as this one').'</h4>';

								echo '<div>';
									foreach($get_similarposts AS $similar) {
										echo '<a href="'.url('read:'.$similar['id_unique']).'">';
											echo (empty($similar['cover_hash']) ? '<div class="no-cover"></div>' : '<figure><img id="cover" src="'.url('cover:'.$similar['cover_hash'], true).'" loading="lazy" alt="'.($viewing_in_english == false ? 'Omslagsbild' : 'Cover').'"></figure>');
											echo '<div class="small-text">'.$similar['subject'].'</div>';
										echo '</a>';
									}

									* for($i = 1; $i <= (10 - $c_similarposts); $i++) {
										echo '<div class="nothing no-select">';
											echo '<div class="no-cover"></div>';
											echo '<div class="dummy-text">...</div>';
										echo '</div>';
									} *
								echo '</div>';
							}
						echo '</div>';*/



						if($config_allowcomments == true) {
							echo '<div class="write-a-comment" id="new-comment">';
								echo '<h4>'.($viewing_in_english == false ? 'Kommentera inlägget' : 'Comment the post').'</h4>';

								echo '<div class="no-ipaddress small-text">';
									if($viewing_in_english == false) {
										echo '<p>Det har gått 2 veckor sen kommentaren blev publicerad och kan därför inte redigeras (<a href="'.url('privacy:duration').'">läs mer</a>). Men du kan fortfarande ta bort den, om du har lösenordet.</p>';

									} else {
										echo '<p>The comment was published over 2 weeks ago and can therefore not be edited (<a href="'.url('privacy:duration').'">read more</a>). But you can still delete the comment, if you have the password.</p>';
									}
								echo '</div>';


								echo '<div id="message"></div>';
								echo '<form action="javascript:void(0)" method="POST" data-idpost="'.$id_unique.'"'.($managing_comment == false ? '' : ' data-idcomment="'.$comment_id.'"').'>';

									echo '<input type="hidden" name="hidden-idcomment">';
									echo '<input type="hidden" name="hidden-idanswer">';


									echo '<div class="item">';
										echo '<div class="label">';
											echo ($viewing_in_english == false ? 'Ditt namn' : 'Your name');
										echo '</div>';

										echo '<div class="field">';
											echo '<input type="text" name="field-name" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'"';
											echo ($managing_comment == false ? ($session == false ? '' : ' value="'.$user['info_name'].'"') : (($do_exists == 0 OR $c_comments == 0) ? '' : ' value="..."'));
											echo ' tabindex="1" required>';
										echo '</div>';
									echo '</div>';


									echo '<div class="item">';
										echo '<div class="label">';
											echo ($viewing_in_english == false ? 'Din webbadress' : 'Your URL');
										echo '</div>';

										echo '<div class="field">';
											echo '<input type="url" name="field-url" placeholder="'.($viewing_in_english == false ? 'Valfritt (exempel: '.$og_url.')' : 'Optional (example: '.$og_url.')').'"';
											echo ($managing_comment == false ? ($session == false ? '' : ' value="'.$user['info_url'].'"') : (($do_exists == 0 OR $c_comments == 0) ? '' : ' value="'.$manage['visitor_url'].'"'));
											echo ' tabindex="2">';
										echo '</div>';
									echo '</div>';


									echo '<div class="item your-comment">';
										echo '<div class="label">';
											echo ($viewing_in_english == false ? 'Din kommentar (minst 10 tecken, max 1 000 tecken)' : 'Your comment (min. 10 chars, max. 1000 chars.)');
										echo '</div>';

										echo '<div class="field">';
											echo '<textarea name="field-comment" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" maxlength="1000" tabindex="3" required>';
											echo ($managing_comment == false ? '' : (($do_exists == 0 OR $c_comments == 0) ? '' : $manage['visitor_comment']));
											echo '</textarea>';

											echo '<div class="hint small-text">';
												echo svgicon('markdown');

												if($viewing_in_english == false) {
													echo 'Du kan använda Markdown i din kommentar.';
												} else {
													echo 'You can use Markdown in your comment.';
												}
											echo '</div>';
										echo '</div>';
									echo '</div>';


									if($session == false OR $session == true AND $managing_comment == false) {
										echo '<div class="item your-password">';
											echo '<div class="label">';
												echo ($viewing_in_english == false ? 'Kommentarens lösenord' : 'The password for this comment');
											echo '</div>';

											echo '<div class="field">';
												echo '<input type="text" name="field-password-readonly" id="pw-readonly" value="'.$password[0].'" readonly>';
												echo '<input type="password" name="field-password" id="pw" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'" tabindex="4" required>';
											echo '</div>';
										echo '</div>';
									}



									echo '<div class="checkboxes">';
										echo '<div class="pretty p-default p-round p-thick">';
											echo '<input type="checkbox" name="check-1" id="check-1">';

											echo '<div class="state p-danger-o">';
												echo '<label for="check-1">';
													echo ($viewing_in_english == false ? 'Ta bort kommentaren' : 'Delete the comment');
												echo '</label>';
											echo '</div>';
										echo '</div>';
									echo '</div>';



									echo '<div class="button">';
										$send_comment = ($viewing_in_english == false ? 'Skicka min kommentar' : 'Send my comment');
										$save_comment = ($viewing_in_english == false ? 'Spara' : 'Save');

										echo '<input type="submit" name="button-';
										echo ($managing_comment == false ? 'showpopup' : (($do_exists == 0 OR $c_comments == 0) ? 'send' : 'save'));
										echo '" value="';
										echo ($managing_comment == false ? $send_comment : (($do_exists == 0 OR $c_comments == 0) ? $send_comment : $save_comment));
										echo '" tabindex="'.($managing_comment == false ? '5' : (($do_exists == 0 OR $c_comments == 0) ? '5' : '6')).'"';
										echo ' disabled';
										echo '>';

		 								echo '<a href="javascript:void(0)" id="cancel" class="small-text">';
		 									echo ($viewing_in_english == false ? 'Avbryt' : 'Cancel');
		 								echo '</a>';
									echo '</div>';

								echo '</form>';
							echo '</div>';



							echo '<div class="list-comments">';
								echo '<div class="message no-select loading">';
									echo svgicon('loading');
									echo ($viewing_in_english == false ? 'Läser in kommentarerna - var god vänta' : 'Loading the comments - please wait');
								echo '</div>';

								echo '<div class="comments"></div>';
							echo '</div>';
						}







						echo '<div class="webmention">';
							echo '<h4>Webmention</h4>';

							echo '<div class="desc small-text">';
								if($viewing_in_english == false) {
									echo '<p>Du kan länka det här inlägget till något socialt nätverk och när du får någon form av respons, visas det här.</p>';

								} else {
									echo '<p>You can share this post to any popular social network. When you get any sort of response, it will show here.</p>';
								}
							echo '</div>';


							if($c_webmention == 0) {
								echo '<div class="message no-select">';
									echo svgicon('no-webmentions');
									echo ($viewing_in_english == false ? 'Kunde inte hitta några webmentions' : 'Couldn\'t find any webmentions');
								echo '</div>';


							} else {
								echo '<div class="likes">';
									echo svgicon('webmention-like');

									if($c_likes == 0) {
										echo '<div><div class="empty"></div></div>';
									} else {
										foreach($get_likes AS $like) {
											echo '<div>'.link_('<img src="'.url('images/webmention-avatars/'.$like['author_avatar_locally'], true).'">', $like['url']).'</div>';
										}
									}
								echo '</div>';

								echo '<div class="reposts">';
									echo svgicon('webmention-repost');

									if($c_reposts == 0) {
										echo '<div><div class="empty"></div></div>';
									} else {
										foreach($get_reposts AS $repost) {
											echo '<div>'.link_('<img src="'.url('images/webmention-avatars/'.$repost['author_avatar_locally'], true).'">', $repost['url']).'</div>';
										}
									}
								echo '</div>';


								if($c_replies != 0) {
									echo '<div class="replies">';
										foreach($get_replies AS $reply) {
											echo '<div>';
												echo '<div class="info">';
													echo '<div class="avatar" style="background-image: url('.url('images/webmention-avatars/'.$reply['author_avatar_locally'], true).');"></div>';

													echo '<div>';
														echo '<div class="name">'.link_($reply['author_name'], $reply['author_url']).'</div>';
														echo '<div class="received small-text">'.link_(date_($reply['webmention_received'], 'full'), $reply['url']).'</div>';
													echo '</div>';
												echo '</div>';

												echo '<div class="content">';
													echo $reply['content_html'];
												echo '</div>';
											echo '</div>';
										}
									echo '</div>';
								}
							}
						echo '</div>';
					}
				echo '</section>';


			}





		} else {

			echo '<section id="share-to-mastodon">';
				echo '<h2>';
					if($viewing_in_english == false) {
						echo 'Ange din Mastodon-instans';
					} else {
						echo 'Enter your Mastodon instance';
					}
				echo '</h2>';

				if($viewing_in_english == false) {
					echo '<p>För att du ska kunna dela "<a href="'.url('read:'.$id_unique).'">'.$post['subject'].'</a>" till en instans på Mastodon, så måste du ange vilken instans du vill dela länken till.</p>';
				} else {
					echo '<p>In order to share the blog post "<a href="'.url('read:'.$id_unique).'">'.$post['subject'].'</a>" to an instance on Mastodon, you need to enter the Mastodon instance you want to share the post to.</p>';
				}


				echo '<form action="'.url('read:'.$id_unique.'/share-to-mastodon').'" method="POST">';
					echo '<div class="item">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Webbadress' : 'URL');
						echo '</div>';

						echo '<div class="field">';
							echo '<input type="url" name="field-instance" placeholder="'.($viewing_in_english == false ? 'Exempel' : 'Example').': fosstodon.org" autofocus>';
						echo '</div>';
					echo '</div>';

					echo '<div class="button">';
						echo '<input type="submit" name="button-sendtoot" value="'.($viewing_in_english == false ? 'Fortsätt' : 'Continue').'">';
					echo '</div>';
				echo '</form>';
			echo '</section>';

		}







		require_once 'site-footer.php';

	}

?>
