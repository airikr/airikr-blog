<?php

	require_once (isset($_GET['js']) ? '../' : '').'site-settings.php';

	$page = (isset($_GET['pag']) ? strip_tags(htmlspecialchars($_GET['pag'])) : false);
	$paging = (isset($_GET['pgn']) ? strip_tags(htmlspecialchars($_GET['pgn'])) : false);
	$tag = (isset($_GET['tag']) ? true : false);
	$tag_get = ($tag == true ? strip_tags(htmlspecialchars($_GET['tag'])) : null);
	$search = (isset($_GET['str']) ? true : false);
	$search_get = ($search == true ? strip_tags(htmlspecialchars($_GET['str'])) : null);
	$search_get = str_replace('-', ' ', $search_get);
	$offset = (($paging == null OR $paging == 0) ? 0 : ($paging - 1)) * $config_postsperpage;



	if($tag == true) {
		$c_totalposts = sql("SELECT COUNT(t.name)
							 FROM posts p
							 INNER JOIN tags_linked tl
							 ON tl.id_post = p.id
							 INNER JOIN tags t
							 ON tl.id_tag = t.id
							 WHERE t.name = :_name
							 AND p.timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							", Array(
								'_name' => str_replace('-', ' ', $tag_get)
							), 'count');

		$posts_exists = sql("SELECT COUNT(t.name)
							 FROM posts p
							 INNER JOIN tags_linked tl
							 ON tl.id_post = p.id
							 INNER JOIN tags t
							 ON tl.id_tag = t.id
							 WHERE t.name = :_name
							 AND p.timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							", Array(
								'_name' => str_replace('-', ' ', $tag_get)
							), 'count');


	} elseif($search == true) {
		$c_totalposts = sql("SELECT COUNT(id)
							 FROM posts
							 WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND subject LIKE :_string
							 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND content_beforebreak LIKE :_string
							 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND content_afterbreak LIKE :_string
							", Array(
								'_string' => '%'.$search_get.'%'
							), 'count');

		$posts_exists = sql("SELECT COUNT(id_unique)
							 FROM posts
							 WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND subject LIKE :_string
							 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND content_beforebreak LIKE :_string
							 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							 AND content_afterbreak LIKE :_string
							", Array(
								'_string' => '%'.$search_get.'%'
							), 'count');


	} else {
		$c_totalposts = sql("SELECT COUNT(id)
							 FROM posts
							 WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							", Array(), 'count');

		$posts_exists = sql("SELECT COUNT(id_unique)
							 FROM posts
							 WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
							", Array(), 'count');
	}


	$total_pages = ceil($c_totalposts / $config_postsperpage);
	$current_page = ($paging == 0 ? 1 : $paging);

	if($tag == true) {
		$link = 'tag:'.$tag_get;
	} elseif($search == true) {
		$link = 'search:'.$search_get;
	} else {
		$link = null;
	}





	echo '<section id="blog">';
		if($tag == true) {
			echo '<h4 id="results">';
				echo svgicon('hashtag');
				echo ($viewing_in_english == false ? 'Visar inlägg med taggen' : 'Showing posts with the tag').' "'.str_replace('-', ' ', $tag_get).'"';
			echo '</h4>';

		} elseif($search == true) {
			echo '<h4 id="results">';
				echo ($viewing_in_english == false ? 'Visar inlägg med sökfrasen' : 'Showing posts with the search phrase').' "'.$search_get.'"';
			echo '</h4>';

		} elseif($page == 'saved') {
			echo '<h1>';
				echo ($viewing_in_english == false ? 'Sparade inlägg' : 'Saved posts');
			echo '</h1>';
		}



		if($posts_exists == 0 OR
		   $paging != false AND $current_page > $total_pages OR
		   $paging != false AND $paging == 0 AND $paging != false) {

			echo '<div class="no-posts">';
				if($page == 'saved') {
					echo svgicon('nothing-saved');
					echo '<p>'.($viewing_in_english == false ? 'Kunde inte hitta några sparade inlägg.' : 'Couldn\'t find any saved posts.').'</p>';

				} elseif($page == 'tag' AND $paging == false) {
					echo svgicon('nothing-tagged');
					echo '<p>'.($viewing_in_english == false ? 'Kunde inte hitta några inlägg med den taggen' : 'Couldn\'t find any posts with that tag.').'</p>';

				} elseif($paging != false AND $current_page > $total_pages) {
					echo svgicon('outside-paging');

					if($viewing_in_english == false) {
						echo '<p>Det finns inga fler sidor. <a href="'.url(($tag == false ? '' : 'tag:'.$tag_get . ($total_pages == 1 ? '' : '/')) . ($total_pages == 1 ? '' : 'page:'.$total_pages)).'">Gå tillbaka till den sista sidan.</a></p>';
					} else {
						echo '<p>There\'s no more pages. <a href="'.url(($tag == false ? '' : 'tag:'.$tag_get . ($total_pages == 1 ? '' : '/')) . ($total_pages == 1 ? '' : 'page:'.$total_pages)).'">Go to the last page.</a></p>';
					}


				} else {
					echo svgicon('no-posts');
					echo '<p>'.($viewing_in_english == false ? 'Kunde inte hitta några inlägg.' : 'Couldn\'t find any posts.').'</p>';
				}
			echo '</div>';



		} else {

			if($posts_exists != 0) {
				if($tag == true) {
					$count_pinned = sql("SELECT COUNT(p.id)
										 FROM posts p
										 INNER JOIN tags_linked tl
										 ON tl.id_post = p.id
										 INNER JOIN tags t
										 ON tl.id_tag = t.id
										 WHERE t.name = :_name
										 AND p.timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
	   									 AND p.is_pinned IS NOT NULL
   										 AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
   										 ".($session == true ? "" : " AND FROM_UNIXTIME(p.timestamp_published) < NOW()")."
										", Array(
											'_name' => str_replace('-', ' ', $tag_get)
									 	), 'count');

					if($count_pinned != 0 AND $search == false) {
						$get_pinned = sql("SELECT *
										   FROM posts p
										   INNER JOIN tags_linked tl
										   ON tl.id_post = p.id
										   INNER JOIN tags t
										   ON tl.id_tag = t.id
										   WHERE t.name = :_name
										   AND p.timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
	 									   AND p.is_pinned IS NOT NULL
	 									   AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
	 									   ".($session == true ? "" : " AND FROM_UNIXTIME(p.timestamp_published) < NOW()")."
										   ORDER BY p.timestamp_published DESC
										  ", Array(
											  '_name' => str_replace('-', ' ', $tag_get)
										  ));
					}

					$get_posts = sql("SELECT *, p.id AS id_post
									  FROM posts p
									  INNER JOIN tags_linked tl
									  ON tl.id_post = p.id
									  INNER JOIN tags t
									  ON tl.id_tag = t.id
									  WHERE t.name = :_name
									  AND p.timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
									  AND p.is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									  ".($session == true ? "" : " AND FROM_UNIXTIME(p.timestamp_published) < NOW()")."
									  ORDER BY p.timestamp_published DESC
   									  LIMIT ".$offset.", ".$config_postsperpage."
									 ", Array(
										 '_name' => str_replace('-', ' ', $tag_get)
									 ));


				} elseif($search == true) {
					$count_pinned = sql("SELECT COUNT(id)
										 FROM posts
										 WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
										 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										 ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
										 AND subject LIKE :_string

										 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
										 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										 ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
										 AND content_beforebreak LIKE :_string

										 OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
										 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										 ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
										 AND content_afterbreak LIKE :_string
										", Array(
											'_string' => '%'.$search_get.'%'
									 	), 'count');

					if($count_pinned != 0 AND $search == false) {
						$get_pinned = sql("SELECT *
										   FROM posts
	 									   WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
	 									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										   ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
	 									   AND subject LIKE :_string

	 									   OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
	 									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										   ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
	 									   AND content_beforebreak LIKE :_string

	 									   OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
	 									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
										   ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
	 									   AND content_afterbreak LIKE :_string

										   ORDER BY timestamp_published DESC
										  ", Array(
											  '_string' => '%'.$search_get.'%'
										  ));
					}

					$get_posts = sql("SELECT *
									  FROM posts
									  WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									  AND subject LIKE :_string

									  OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									  AND content_beforebreak LIKE :_string

									  OR timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									  AND content_afterbreak LIKE :_string

									  ORDER BY timestamp_published DESC
   									  LIMIT ".$offset.", ".$config_postsperpage."
									 ", Array(
										 '_string' => '%'.$search_get.'%'
									 ));


				} else {
					$count_pinned = sql("SELECT COUNT(id)
										 FROM posts
										 WHERE is_pinned IS NOT NULL
   										 AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
   										 ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
										", Array(), 'count');

					if($count_pinned != 0 AND $search == false) {
						$get_pinned = sql("SELECT *
										   FROM posts
										   WHERE is_pinned IS NOT NULL
	 									   AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
	 									   ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
										   ORDER BY timestamp_published DESC
										  ", Array());
					}

					$get_posts = sql("SELECT *, id AS id_post
									  FROM posts
									  WHERE timestamp_saved ".($page == 'saved' ? "IS NOT" : "IS")." NULL
									  AND is_inenglish ".($viewing_in_english == true ? "IS NOT" : "IS")." NULL
									  ".($session == true ? "" : " AND FROM_UNIXTIME(timestamp_published) < NOW()")."
									  ORDER BY timestamp_published DESC
									  LIMIT ".$offset.", ".$config_postsperpage."
									 ", Array());
				}
			}


			if($count_pinned != 0 AND $page != 'saved' AND $search == false) {
				echo '<div class="pinned">';
					echo '<h4>'.($viewing_in_english == false ? 'Fastnålade inlägg' : 'Pinned posts').'</h4>';

					foreach($get_pinned AS $pinned) {
						echo '<div>';
							echo '<div class="published">';
								echo date_($pinned['timestamp_published'], 'short');
							echo '</div>';

							echo '<div class="subject">';
								echo '<a href="'.url('read:'.$pinned['id_unique']).'">';
									echo $pinned['subject'];
								echo '</a>';
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			}


			$count = 0;
			foreach($get_posts AS $post) {
				$is_pinned = ($post['is_pinned'] == 1 ? true : false);
				# $reading_time = round((str_word_count(strip_tags($post['content_beforebreak'] . $post['content_afterbreak'])) / 200));
				$count++;

				$c_comments = sql("SELECT COUNT(id)
								   FROM comments
								   WHERE id_post = :_idpost
								  ", Array(
									  '_idpost' => (int)$post['id_post']
								  ), 'count');

				$get_tags = sql("SELECT t.name
								 FROM tags AS t
								 INNER JOIN tags_linked AS tl
								 ON t.id = tl.id_tag
								 WHERE tl.id_post = :_idpost
								 ORDER BY name ASC
								", Array(
									'_idpost' => (int)$post['id_post']
								));



				/*
				if($post['tags'] != null) {
					foreach(explode(',', $post['tags']) AS $tag) {
						$check_tag = sql("SELECT COUNT(name)
										  FROM tags
										  WHERE name = :_tag
										 ", Array(
											 '_tag' => $tag
										 ), 'count');


						if($check_tag == 0) {
							sql("INSERT INTO tags(
									 name,
									 is_inenglish
								 )

								 VALUES(
									 :_name,
									 :_isinenglish
								 )
								", Array(
									'_name' => $tag,
									'_isinenglish' => ($viewing_in_english == false ? null : 'en')
								), 'insert');
						}


						$tagname = sql("SELECT *
										FROM tags
										WHERE name = :_tag
									   ", Array(
										   '_tag' => $tag
									   ), 'fetch');

						$check_tagid = sql("SELECT COUNT(id_tag)
											FROM tags_linked
											WHERE id_tag = :_idtag
											AND id_post = :_idpost
										   ", Array(
			     							   '_idtag' => (int)$tagname['id'],
			     							   '_idpost' => (int)$post['id']
										   ), 'count');

						if($check_tagid == 0) {
							sql("INSERT INTO tags_linked(
									 id_tag,
									 id_post
								 )

								 VALUES(
									 :_idtag,
									 :_idpost
								 )
								", Array(
									'_idtag' => (int)$tagname['id'],
									'_idpost' => (int)$post['id']
								), 'insert');
						}
					}
				}
				*/



				echo '<article class="item'.($count == 1 ? ' first' : '').'">';
					echo '<header>';
						echo '<h2>';
							echo ($is_pinned == true ? svgicon('pinned') : '');
							echo '<a href="'.url('read:'.$post['id_unique']).'">';
								echo $post['subject'];
							echo '</a>';
						echo '</h2>';


						echo '<div class="info small-text">';
							echo '<div class="datetime">';
								if($post['timestamp_saved'] != null) {
									echo ($viewing_in_english == false ? 'Sparades den' : 'Saved on').' ';
									echo '<time datetime="'.date('Y-m-dTH:i:s', $post['timestamp_saved']).'+01:00">';
										echo date_($post['timestamp_saved'], 'full');
									echo '</time>';

								} else {
									echo '<time datetime="'.date('Y-m-dTH:i:s', $post['timestamp_published']).'+01:00">';
										echo date_($post['timestamp_published'], 'full');
									echo '</time>';
								}
							echo '</div>';

							/*
							echo '<div class="delimiter">_</div>';

							echo '<div class="reading-time'.($tags == null ? ' notags read' : '').'">';
								echo ($reading_time == 0 ? 'Under en minuts lästid' : 'Cirka '.$reading_time.' '.($reading_time == 1 ? 'minut' : 'minuter').'s lästid');
							echo '</div>';
							*/


							if($post['timestamp_saved'] == null) {
								echo '<div class="delimiter">_</div>';

								echo '<div class="comments">';
									echo '<a href="'.url('read:'.$post['id_unique'].'#comments').'">';
										echo number_format((int)$c_comments, 0, ',', ' ').' ';

										if($viewing_in_english == false) {
											echo ((int)$c_comments == 1 ? 'kommentar' : 'kommentarer');
										} else {
											echo ((int)$c_comments == 1 ? 'comment' : 'comments');
										}
									echo '</a>';
								echo '</div>';
							}
						echo '</div>';


						echo '<div class="tags small-text">';
							foreach($get_tags AS $singletag) {
								echo '<a href="'.url('tag:'.$singletag['name']).'">#'.$singletag['name'].'</a>';
							}
						echo '</div>';
					echo '</header>';


					echo ($post['cover_hash'] == null ? '' : '<figure><img id="cover-list" src="'.url('cover:'.$post['cover_hash'], true).'" loading="lazy" alt="'.($viewing_in_english == false ? 'Omslagsbild' : 'Cover').'"></figure>');


					echo '<main'.($post['cover_hash'] == null ? ' class="nocover"' : '').'>';
						if($post['content_afterbreak'] == null) {
							if($search == false) {
								echo $Parsedown->text(emoji_convert($post['content_beforebreak']));
							} else {
								echo $Parsedown->text(highlight_word(emoji_convert(checkaftertoots($post['content_beforebreak'])), $search_get));
							}

						} else {
							echo '<details>';
								if($search == false) {
									echo '<summary>'.$Parsedown->text(emoji_convert($post['content_beforebreak'])).'</summary>';
									echo '<div class="content">'.$Parsedown->text(emoji_convert(checkaftertoots($post['content_afterbreak']))).'</div>';
								} else {
									echo '<summary>'.$Parsedown->text(highlight_word(emoji_convert($post['content_beforebreak']), $search_get)).'</summary>';
									echo '<div class="content">'.$Parsedown->text(highlight_word(emoji_convert(checkaftertoots($post['content_afterbreak'])), $search_get)).'</div>';
								}
							echo '</details>';
						}
					echo '</main>';
				echo '</article>';
			}



			# echo '<div class="paging small-text'.($not_logged == true ? ' spacebelow' : '').'">';
			echo '<div class="paging small-text">';
				echo '<div class="current">';
					echo ($viewing_in_english == false ? 'Sida' : 'Page').' <b>'.number_format($current_page).'</b> '.($viewing_in_english == false ? 'av' : 'of').' '.number_format($total_pages);
				echo '</div>';


				echo '<div class="navigate">';
					if($paging < 2) {
						echo '<div class="inactive no-select">';
							echo svgicon('previous');
							echo ($viewing_in_english == false ? 'Föregående' : 'Previous');
						echo '</div>';

					} else {
						echo '<a href="'.url($link . (($paging - 1) == 1 ? null : ($link == null ? null : '/').'page:'.($paging - 1)), false, false).'">';
							echo svgicon('previous');
							echo ($viewing_in_english == false ? 'Föregående' : 'Previous');
						echo '</a>';
					}


					if($paging >= $total_pages OR $total_pages == 1 OR $current_page == $total_pages) {
						echo '<div class="inactive no-select">';
							echo ($viewing_in_english == false ? 'Nästa' : 'Next');
							echo svgicon('next');
						echo '</div>';

					} else {
						echo '<a href="'.url($link . ($link == null ? null : '/').'page:'.($paging == 0 ? 2 : ($paging + 1)), false, false).'">';
							echo ($viewing_in_english == false ? 'Nästa' : 'Next');
							echo svgicon('next');
						echo '</a>';
					}
				echo '</div>';
			echo '</div>';

		}
	echo '</section>';

?>
