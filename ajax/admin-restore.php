<?php

	require_once '../site-settings.php';

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	$mail = new PHPMailer(true);
	$field_email = strip_tags(htmlspecialchars($_POST['field-email']));


	$user_exists = sql("SELECT COUNT(info_email)
						FROM users
						WHERE info_email = :_email
					   ", Array(
						   '_email' => $field_email
					   ), 'count');

	if($user_exists != 0) {
		$user = sql("SELECT id
					 FROM users
					 WHERE info_email = :_email
					 GROUP BY id
					", Array(
						'_email' => $field_email
					), 'fetch');
	}



	if($user_exists == 0) {
		echo 'error-usernotfound';

	} else {
		sql("UPDATE users
			 SET twofactorcode = null
			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id']
			));


		try {
			$mail->SMTPDebug = 3;
			$mail->isSMTP();
			$mail->Host = 'posteo.de';
			$mail->SMTPAuth = true;
			$mail->Username = 'edgren@posteo.net';
			$mail->Password = 'QH9Qx2Dd5w5FKDiyG3pP4RqwV767K8r3';
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
			$mail->Port = 587;

			$mail->setFrom('no-reply@airikr.me', 'Airikr Blog');
			$mail->addAddress('hi@airikr.me');
			$mail->addReplyTo('info@example.com', 'Information');
			$mail->addCC($field_email);
			$mail->addBCC($field_email);

			$mail->isHTML(true);
			$mail->Subject = 'Here is the subject';
			$mail->Body = 'This is the HTML message body <b>in bold!</b>';
			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();

		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}

?>
