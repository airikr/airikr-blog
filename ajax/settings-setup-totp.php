<?php

	require_once '../site-settings.php';

	$field_secret = strip_tags(htmlspecialchars($_POST['field-secret']));
	$field_totp = strip_tags(htmlspecialchars($_POST['field-totp']));
	$field_username = strip_tags(htmlspecialchars($_POST['field-username']));
	$field_email = strip_tags(htmlspecialchars($_POST['field-email']));

	$ga = new PHPGangsta_GoogleAuthenticator();
	$result = $ga->verifyCode($field_secret, $field_totp, 2);

	$ncrypt = new mukto90\Ncrypt;
	$ncrypt->set_secret_key($password[0]);
	$ncrypt->set_secret_iv($password[0]);
	$ncrypt->set_cipher('AES-256-CBC');



	if(!$result) {
		echo 'error-invalidtotp';

	} else {
		sql("UPDATE users
			 SET password = null,
			 	 username = :_username,
				 info_email = :_email,
			 	 twofactorcode = :_twofactorcode

			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id'],
				'_username' => trim($field_username),
				'_email' => trim($ncrypt->encrypt($field_email)),
				'_twofactorcode' => $field_secret
			));
	}

?>
