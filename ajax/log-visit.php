<?php

	require_once 'site-settings.php';



	sql("INSERT INTO visitors(
			 data_ipaddress,
			 data_useragent,
			 data_page,
			 data_page_get,
			 timestamp_visited
		 )

		 VALUES(
			 :_ipaddress,
			 :_useragent,
			 :_page,
			 :_page_get,
			 :_visited
		 )
		", Array(
			'_ipaddress' => $visitor_ip,
			'_useragent' => $_SERVER['HTTP_USER_AGENT'],
			'_page' => $filename,
			'_page_get' => $filename_get,
			'_visited' => time()
		), 'insert');

?>
