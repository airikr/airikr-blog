<?php

	require_once '../site-settings.php';

	$id_unique = (!isset($_GET['idu']) ? uniqid() : strip_tags(htmlspecialchars($_GET['idu'])));
	$type = strip_tags(htmlspecialchars($_GET['typ']));


	$form_subject = strip_tags(htmlspecialchars($_POST['field-subject']));
	$form_content = strip_tags(htmlspecialchars($_POST['field-content']));
	$form_tags = strip_tags(htmlspecialchars($_POST['field-tags']));
	$form_publish_date = strip_tags(htmlspecialchars($_POST['field-publish-date']));
	$form_publish_time = strip_tags(htmlspecialchars($_POST['field-publish-time']));
	$form_check_1 = (isset($_POST['check-1']) ? 1 : null);
	$form_check_2 = (isset($_POST['check-2']) ? 1 : null);
	$form_check_3 = (isset($_POST['check-3']) ? 1 : null);

	$tags = explode(',', strip_tags(htmlspecialchars($_POST['field-tags'])));
	$array_content = explode('----', $form_content);
	$content = trim($form_content);
	$content_before = trim($array_content[0]);
	$content_meta = strip_tags(htmlspecialchars($_POST['field-content-meta']));
	$content_after = (isset($array_content[1]) ? trim($array_content[1]) : null);
	$cover_empty = true;


	if(!empty($_POST['field-cover'])) {
		$cover_empty = false;
		$unsplash_url = strip_tags(htmlspecialchars($_POST['field-cover']));
		$unsplash_owner = strip_tags(htmlspecialchars($_POST['field-cover-owner']));
		$unsplash_owner_url = strip_tags(htmlspecialchars($_POST['field-cover-owner-url']));
		$unsplash_hash = md5($unsplash_url);
		$file_dimensions = 890;
		$destination = '../covers/'.$unsplash_hash.'.jpg';

		if(!file_exists($destination)) {
			copy($unsplash_url, $destination);
			compress_image($destination, $file_dimensions);
		}
	}


	if($type != 'savenew' OR $type != 'publish') {
		if(isset($_GET['idu'])) {
			$postid = sql("SELECT id
						   FROM posts
						   WHERE id_unique = :_idunique
						  ", Array(
							  '_idunique' => $id_unique
						  ), 'fetch');

			$c_edits = sql("SELECT COUNT(id_post)
							FROM posts
							WHERE id_post = :_idpost
						   ", Array(
							   '_idpost' => (int)$postid['id']
						   ), 'count');


			if($c_edits != 0) {
				$post = sql("SELECT *
							 FROM posts
							 WHERE id_post = :_idpost
							 ORDER BY timestamp_edited DESC
							 LIMIT 1
							", Array(
								'_idpost' => (int)$postid['id']
							), 'fetch');

				$id_post = (int)$post['id_post'];


			} else {
				$post = sql("SELECT *
							 FROM posts
							 WHERE id_unique = :_idunique
							", Array(
								'_idunique' => $id_unique
							), 'fetch');

				$id_post = (int)$post['id'];
			}
		}
	}







	if($type == 'savenew' OR $type == 'publish') {
		sql("INSERT INTO posts(
				 id_unique,
				 subject,
				 cover_hash,
				 cover_url,
				 cover_owner,
				 cover_owner_url,
				 content_beforebreak,
				 content_meta,
				 content_afterbreak,
				 hasbeencorrected,
				 timestamp_saved,
				 timestamp_published,
				 timestamp_edited,
				 is_pinned,
				 is_inenglish
			 )

			 VALUES(
				 :_idunique,
				 :_subject,
				 :_coverhash,
				 :_coverurl,
				 :_coverowner,
				 :_coverowner_url,
				 :_content_before,
				 :_content_meta,
				 :_content_after,
				 :_hasbeencorrected,
				 :_saved,
				 :_published,
				 :_edited,
				 :_ispinned,
				 :_is_inenglish
			 )
			", Array(
				'_idunique' => $id_unique,
				'_subject' => $form_subject,
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_meta' => $content_meta,
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_hasbeencorrected' => $form_check_1,
				'_saved' => ($type == 'savenew' ? time() : null),
				'_published' => (empty($form_publish_date) ? time() : strtotime($form_publish_date.' '.$form_publish_time)),
				'_edited' => null,
				'_ispinned' => $form_check_2,
				'_is_inenglish' => $form_check_3
			), 'insert');

		echo $id_unique;


	} elseif($type == 'edit') {
		sql("UPDATE posts
			 SET subject = :_subject,
				 cover_hash = :_coverhash,
				 cover_url = :_coverurl,
				 cover_owner = :_coverowner,
				 cover_owner_url = :_coverowner_url,
				 content_beforebreak = :_content_before,
				 content_meta = :_content_meta,
				 content_afterbreak = :_content_after,
				 hasbeencorrected = :_hasbeencorrected,
				 timestamp_saved = :_saved,
				 timestamp_published = :_published,
				 timestamp_edited = :_edited,
				 is_pinned = :_ispinned,
				 is_inenglish = :_is_inenglish

			 WHERE id_unique = :_idunique
			", Array(
				'_idunique' => $id_unique,
				'_subject' => $form_subject,
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_meta' => $content_meta,
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_hasbeencorrected' => $form_check_1,
				'_saved' => null,
				'_published' => (empty($form_publish_date) ? time() : strtotime($form_publish_date.' '.$form_publish_time)),
				'_edited' => null,
				'_ispinned' => $form_check_2,
				'_is_inenglish' => $form_check_3
			));


		/*
		sql("INSERT INTO posts(
				 id_post,
				 subject,
				 cover_hash,
				 cover_url,
				 cover_owner,
				 cover_owner_url,
				 content_beforebreak,
				 content_afterbreak,
				 hasbeencorrected,
				 timestamp_saved,
				 timestamp_published,
				 timestamp_edited,
				 is_pinned,
				 is_inenglish
			 )

			 VALUES(
				 :_idpost,
				 :_subject,
				 :_coverhash,
				 :_coverurl,
				 :_coverowner,
				 :_coverowner_url,
				 :_content_before,
				 :_content_after,
				 :_hasbeencorrected,
				 :_saved,
				 :_published,
				 :_edited,
				 :_ispinned,
				 :_is_inenglish
			 )
			", Array(
				'_idpost' => $id_post,
				'_subject' => $form_subject,
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_hasbeencorrected' => $form_check_1,
				'_saved' => ($type == 'savenew' ? time() : null),
				'_published' => (empty($form_publish_date) ? time() : strtotime($form_publish_date.' '.$form_publish_time)),
				'_edited' => time(),
				'_ispinned' => $form_check_2,
				'_is_inenglish' => $form_check_3
			), 'insert');
		*/

		echo $id_unique;


	} elseif($type == 'finish') {
		sql("UPDATE posts
			 SET subject = :_subject,
				 cover_hash = :_coverhash,
				 cover_url = :_coverurl,
				 cover_owner = :_coverowner,
				 cover_owner_url = :_coverowner_url,
				 content_beforebreak = :_content_before,
				 content_meta = :_content_meta,
				 content_afterbreak = :_content_after,
				 hasbeencorrected = :_hasbeencorrected,
				 timestamp_saved = :_saved,
				 timestamp_published = :_published,
				 timestamp_edited = :_edited,
				 is_pinned = :_ispinned,
				 is_inenglish = :_is_inenglish

			 WHERE id_unique = :_idunique
			", Array(
				'_idunique' => $id_unique,
				'_subject' => $form_subject,
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_meta' => $content_meta,
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_hasbeencorrected' => $form_check_1,
				'_saved' => null,
				'_published' => (empty($form_publish_date) ? time() : strtotime($form_publish_date.' '.$form_publish_time)),
				'_edited' => null,
				'_ispinned' => $form_check_2,
				'_is_inenglish' => $form_check_3
			));

		echo $id_unique;


	} elseif($type == 'save') {
		sql("UPDATE posts
			 SET subject = :_subject,
				 cover_hash = :_coverhash,
				 cover_url = :_coverurl,
				 cover_owner = :_coverowner,
				 cover_owner_url = :_coverowner_url,
				 content_beforebreak = :_content_before,
				 content_meta = :_content_meta,
				 content_afterbreak = :_content_after,
				 hasbeencorrected = :_hasbeencorrected,
				 timestamp_saved = :_saved,
				 timestamp_published = :_published,
				 timestamp_edited = :_edited,
				 is_pinned = :_ispinned,
				 is_inenglish = :_is_inenglish

			 WHERE id_post = :_idpost
			 OR id_unique = :_idunique
			", Array(
				'_idpost' => ($post['id_unique'] == null ? $id_post : null),
				'_idunique' => ($post['id_unique'] == null ? null : $post['id_unique']),
				'_subject' => $form_subject,
				'_coverhash' => ($cover_empty == true ? null : $unsplash_hash),
				'_coverurl' => ($cover_empty == true ? null : $unsplash_url),
				'_coverowner' => ($cover_empty == true ? null : $unsplash_owner),
				'_coverowner_url' => ($cover_empty == true ? null : $unsplash_owner_url),
				'_content_before' => (strpos($content, '----') !== false ? $content_before : $content),
				'_content_meta' => $content_meta,
				'_content_after' => (strpos($content, '----') !== false ? (empty($content_after) ? null : $content_after) : null),
				'_hasbeencorrected' => $form_check_1,
				'_saved' => time(),
				'_published' => (empty($form_publish_date) ? time() : strtotime($form_publish_date.' '.$form_publish_time)),
				'_edited' => null,
				'_ispinned' => $form_check_2,
				'_is_inenglish' => $form_check_3
			));
	}






	$postid = sql("SELECT id
				   FROM posts
				   WHERE id_unique = :_idunique
				  ", Array(
					  '_idunique' => $id_unique
				  ), 'fetch');

	sql("DELETE FROM tags_linked
		 WHERE id_post = :_idpost
		", Array(
			'_idpost' => (int)$postid['id']
		));

	$get_tags = explode(',', $form_tags);


	foreach($get_tags AS $tag) {
		$tag_exists = sql("SELECT COUNT(name)
						   FROM tags
						   WHERE name = :_tagname
						  ", Array(
							  '_tagname' => $tag
						  ), 'count');

		if($tag_exists == 0) {
			sql("INSERT INTO tags(
					 name,
					 is_inenglish
				 )

				 VALUES(
					 :_name,
					 :_is_inenglish
				 )
				", Array(
					'_name' => $tag,
					'_is_inenglish' => ($viewing_in_english == false ? null : 'en')
				), 'insert');
		}

		$taginfo = sql("SELECT id
						FROM tags
						WHERE name = :_tagname
					   ", Array(
						   '_tagname' => $tag
					   ), 'fetch');


		sql("INSERT INTO tags_linked(
				 id_tag,
				 id_post
			 )

			 VALUES(
				 :_idtag,
				 :_idpost
			 )
			", Array(
				'_idtag' => (int)$taginfo['id'],
				'_idpost' => (int)$postid['id']
			), 'insert');
	}

?>
