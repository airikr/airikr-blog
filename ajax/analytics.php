<?php

	require_once '../site-settings.php';



	$allow = (!isset($_GET['all']) ? false : true);
	$delete = (!isset($_GET['del']) ? false : true);



	if($allow == true) {
		sql("INSERT INTO visitors(
				 data_ipaddress,
				 data_useragent,
				 data_page,
				 data_page_get,
				 timestamp_allowed,
				 timestamp_visited
			 )

			 VALUES(
				 :_ipaddress,
				 :_useragent,
				 :_page,
				 :_page_get,
				 :_allowed,
				 :_visited
			 )
			", Array(
				'_ipaddress' => $visitor_ip,
				'_useragent' => $_SERVER['HTTP_USER_AGENT'],
				'_page' => $filename,
				'_page_get' => ($filename_get == '-' ? null : $filename_get),
				'_allowed' => time(),
				'_visited' => time()
			), 'insert');

		$check_ip = sql("SELECT COUNT(data_ipaddress)
						 FROM visitors
						 WHERE data_ipaddress = :_ipaddress
						", Array(
							'_ipaddress' => $visitor_ip
						), 'count');


		if($check_ip == 0) {
			echo 'error';
		} else {
			echo 'added';
		}



	} elseif($delete == true) {
		sql("DELETE FROM visitors
			 WHERE data_ipaddress = :_ipaddress
			", Array(
				'_ipaddress' => $visitor_ip
			));

		$check_ip = sql("SELECT COUNT(data_ipaddress)
						 FROM visitors
						 WHERE data_ipaddress = :_ipaddress
						", Array(
							'_ipaddress' => $visitor_ip
						), 'count');


		if($check_ip != 0) {
			echo 'error';
		} else {
			echo 'deleted';
		}
	}

?>
