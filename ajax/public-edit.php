<?php

	require_once '../site-settings.php';



	$post_id = strip_tags(htmlspecialchars($_POST['hidden-idpost']));
	$form_content = strip_tags(htmlspecialchars($_POST['field-content']));

	$post = sql("SELECT *
				 FROM posts
				 WHERE id = :_idpost
				", Array(
					'_idpost' => (int)$post_id
				), 'fetch');



	if($post['content_beforebreak'] . $post['content_afterbreak'] == $form_content) {
		echo 'error-samecontent';


	} else {
		sql("INSERT INTO public_edits(
				 id_post,
				 content,
				 timestamp_sent
			 )

			 VALUES(
				 :_idpost,
				 :_content,
				 :_sent
			 )
			", Array(
				'_idpost' => $post_id,
				'_content' => $form_content,
				'_sent' => time()
			), 'insert');
	}

?>
