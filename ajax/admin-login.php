<?php

	require_once '../site-settings.php';

	$field_username = strip_tags(htmlspecialchars($_POST['field-username']));
	$field_password = (isset($_POST['field-password']) ? strip_tags(htmlspecialchars($_POST['field-password'])) : null);
	$field_totp = (isset($_POST['field-totp']) ? strip_tags(htmlspecialchars($_POST['field-totp'])) : null);


	$user_exists = sql("SELECT COUNT(username)
						FROM users
						WHERE username = :_username
					   ", Array(
						   '_username' => $field_username
					   ), 'count');

	if($user_exists != 0) {
		$user = sql("SELECT id, password, twofactorcode
					 FROM users
					 WHERE username = :_username
					 GROUP BY id
					", Array(
						'_username' => $field_username
					), 'fetch');

		$ga = new PHPGangsta_GoogleAuthenticator();
		$result = $ga->verifyCode($user['twofactorcode'], $field_totp, 2);
	}



	if($user_exists == 0) {
		echo 'error-usernotfound';

	} elseif($field_password != null AND !password_verify($field_password, $user['password'])) {
		echo 'error-invalidpassword';

	} elseif($field_totp != null AND !$result) {
		echo 'error-invalidtotp';

	} else {
		$_SESSION['loggedin'] = (int)$user['id'];

		sql("UPDATE users
			 SET timestamp_lastlogin = :_lastlogin,
			 	 timestamp_lastactive = :_lastactive

			 WHERE id = :_iduser
			", Array(
				'_iduser' => (int)$user['id'],
				'_lastlogin' => time(),
				'_lastactive' => time()
			));
	}

?>
