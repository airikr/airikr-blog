<?php

	require_once '../site-settings.php';

	$post_id = (!isset($_GET['idp']) ? null : strip_tags(htmlspecialchars($_GET['idp'])));
	$comment_id = strip_tags(htmlspecialchars($_GET['idc']));

	$id_unique = strip_tags(htmlspecialchars($_GET['idu']));
	$field_name = (!isset($_POST['field-name']) ? null : strip_tags(htmlspecialchars(trim($_POST['field-name']))));
	$field_url = (!isset($_POST['field-name']) ? null : strip_tags(htmlspecialchars(trim($_POST['field-url']))));
	$field_url = ($field_url == null ? '' : filter_var($field_url, FILTER_SANITIZE_URL));
	$field_comment = (!isset($_POST['field-name']) ? null : strip_tags(htmlspecialchars(trim($_POST['field-comment']))));
	$field_password = strip_tags(htmlspecialchars(trim($_POST['field-password-readonly'])));
	$checkbox_delete_comment = (isset($_POST['check-1']) ? true : false);


	$comment = sql("SELECT id, comment_password
					FROM comments
					WHERE id = :_idcomment
				   ", Array(
					   '_idcomment' => $comment_id
				   ), 'fetch');



	if($session == true AND $user['is_admin'] == 0 AND !password_verify($field_password, $comment['comment_password']) OR
	   $session == false AND !password_verify($field_password, $comment['comment_password'])) {
		echo 'error-invalidpassword';

	} else {
		if($checkbox_delete_comment == true) {
			sql("DELETE FROM comments
				 WHERE id = :_idcomment
				", Array(
					'_idcomment' => (int)$comment['id']
				));


		} else {
			sql("UPDATE comments
				 SET visitor_name = :_name,
					 visitor_url = :_url,
					 visitor_comment = :_comment,
					 timestamp_edited = :_edited

				 WHERE id = :_idcomment
				", Array(
					'_idcomment' => (int)$comment['id'],
					'_name' => $field_name,
					'_url' => $field_url,
					'_comment' => $field_comment,
					'_edited' => time()
				));
		}
	}

?>
