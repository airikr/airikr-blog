<?php

	require_once '../site-settings.php';



	$comment = sql("SELECT *
					FROM comments
					WHERE id = :_idcomment
				   ", Array(
					   '_idcomment' => strip_tags(htmlspecialchars($_GET['idc']))
				   ), 'fetch');


	echo $comment['visitor_name'].'|';
	echo (empty($comment['visitor_email']) ? 'no-email' : 'email').'|';
	echo $comment['visitor_url'].'|';
	echo $comment['visitor_comment'].'|';
	echo (empty($comment['visitor_ipaddress']) ? 'no-ip' : null);

?>
