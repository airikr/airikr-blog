<?php

	require_once '../site-settings.php';



	$post = sql("SELECT id, id_unique
				 FROM posts
				 WHERE id = :_idpost
				", Array(
					'_idpost' => strip_tags(htmlspecialchars($_GET['idp']))
				), 'fetch');

	$id_unique = $post['id_unique'];
	$post_id = (int)$post['id'];
	$comment_id = (!isset($_GET['idc']) ? null : strip_tags(htmlspecialchars($_GET['idc'])));

	$c_comments = sql("SELECT COUNT(id_post)
					   FROM comments
					   WHERE id_post = :_idpost
					  ", Array(
						  '_idpost' => $post_id
					  ), 'count');


	if($c_comments != 0) {
		$get_comments = sql("SELECT *
							 FROM comments
							 WHERE id_post = :_idpost
							 AND id_comment IS NULL
							 ORDER BY timestamp_published ASC
							", Array(
								'_idpost' => $post_id
							));
	}



	if($c_comments == 0) {
		echo '<div class="message no-select" id="comments">';
			echo svgicon('no-comments');
			echo ($viewing_in_english == false ? 'Kunde inte hitta några kommentarer' : 'Couldn\'t find any comments');
		echo '</div>';


	} else {
		echo '<h4 id="comments">';
			echo ($viewing_in_english == false ? 'Kommentarer' : 'Comments');
			echo ($c_comments == 0 ? '' : ' ('.$c_comments.')');

			echo '<a href="javascript:void(0)" id="reload" title="'.($viewing_in_english == false ? 'Uppdatera kommentarsflödet' : 'Reload the comments').'">';
				echo svgicon('reload');
			echo '</a>';

			echo '<div id="reload-inactive">';
				echo svgicon('reload');
			echo '</div>';

			echo '<span class="rss">';
				echo link_(svgicon('linkicon-rss'), url('rss/comments/post-id:'.$post['id_unique']));
			echo '</span>';
		echo '</h4>';



		foreach($get_comments AS $comment) {
			$url = (empty($comment['visitor_url']) ? null : parse_url($comment['visitor_url']));
			$identity = (!empty($comment['comment_identity']) ? $comment['comment_identity'] : strtolower(str_replace(' ', '-', trim($comment['visitor_name']))));

			$c_answers = sql("SELECT COUNT(id_post)
							  FROM comments
							  WHERE id_post = :_idpost
							  AND id_comment = :_idcomment
							 ", Array(
								 '_idpost' => $post_id,
								 '_idcomment' => (int)$comment['id']
							 ), 'count');


			if($c_answers != 0) {
				$get_answers = sql("SELECT *
									FROM comments
									WHERE id_post = :_idpost
									AND id_comment = :_idcomment
									ORDER BY timestamp_published ASC
								   ", Array(
									   '_idpost' => $post_id,
									   '_idcomment' => (int)$comment['id']
								   ));
			}



			echo '<div class="item" data-idcomment="'.(int)$comment['id'].'">';

				echo '<div class="top">';
					echo '<div class="id">';
						echo '<a href="javascript:void(0)" class="link-id" data-idcomment="'.(int)$comment['id'].'">';
							echo '#'.(int)$comment['id'];
						echo '</a>';
					echo '</div>';

					echo '<div class="information">';
						echo '<div>';
							echo '<div class="shortinfo">';
								echo '<div class="name" data-idcomment="'.(int)$comment['id'].'">';
									echo '<a href="'.url('comment-author:'.$identity).'" title="'.($viewing_in_english == false ? 'Se alla kommentarer från' : 'See all comments from').' '.trim($comment['visitor_name']).'">';
										echo trim($comment['visitor_name']);
									echo '</a>';

									echo '<span class="rss">';
										echo link_(svgicon('linkicon-rss'), url('rss/comments/author:'.str_replace(' ', '-', strtolower(trim($comment['visitor_name'])))));
									echo '</span>';
								echo '</div>';
							echo '</div>';

							if($url != null) {
								echo '<div class="url">';
									echo link_($url['host'] . (isset($url['path']) ? '<span class="path">'.$url['path'].'</span>' : ''), $comment['visitor_url']);
								echo '</div>';
							}
						echo '</div>';

						echo '<div>';
							echo '<div class="published small-text">';
								echo date_($comment['timestamp_published'], 'full');
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';


				echo '<div class="comment">';
					echo preg_replace('/(?<!\S)#([0-9a-zA-Z]+)/', '<a href="javascript:void(0)" class="link-id" data-idcomment="$1">#$1</a>', $Parsedown->text(emoji_convert($comment['visitor_comment'])));
				echo '</div>';


				echo '<div class="links small-text">';
					echo '<a href="javascript:void(0)" class="answer" data-idcomment="'.(int)$comment['id'].'" data-idanswer="">';
						echo ($viewing_in_english == false ? 'Svara' : 'Answer');
					echo '</a>';

					echo '<div class="answer-inactive" data-idcomment="'.(int)$comment['id'].'">';
						echo ($viewing_in_english == false ? 'Svara' : 'Answer');
					echo '</div>';


					echo '<a href="javascript:void(0)" class="manage" data-idcomment="'.(int)$comment['id'].'">';
						echo ($viewing_in_english == false ? 'Hantera' : 'Manage');
					echo '</a>';

					echo '<div class="loading" data-idcomment="'.(int)$comment['id'].'">';
						echo ($viewing_in_english == false ? 'Hantera (vänta)' : 'Manage (please wait)');
					echo '</div>';

					echo '<a href="javascript:void(0)" class="managing" data-idcomment="'.(int)$comment['id'].'">';
						echo ($viewing_in_english == false ? 'Hanteras...' : 'Managing...');
					echo '</a>';


					echo link_(($viewing_in_english == false ? 'Prenumerera' : 'Subscribe'), url('rss/comment-id:'.(int)$comment['id']));
				echo '</div>';

			echo '</div>';







			if($c_answers != 0) {
				foreach($get_answers AS $answer) {
					$url_a = (empty($answer['visitor_url']) ? null : parse_url($answer['visitor_url']));
					$identity_a = (!empty($answer['comment_identity']) ? $answer['comment_identity'] : strtolower(str_replace(' ', '-', trim($answer['visitor_name']))));

					$answersto = sql("SELECT visitor_name
									  FROM comments
									  WHERE id = :_idcomment
									 ", Array(
										 '_idcomment' => ($answer['id_answer'] == null ? (int)$answer['id_comment'] : (int)$answer['id_answer'])
									 ), 'fetch');



					echo '<div class="item is-an-answer" data-idcomment="'.(int)$answer['id'].'">';

						echo '<div class="top">';
							echo '<div class="id">';
								echo '<a href="javascript:void(0)" class="link-id" data-idcomment="'.(int)$answer['id'].'">';
									echo '#'.(int)$answer['id'];
								echo '</a>';
							echo '</div>';

							echo '<div class="information">';
								echo '<div>';
									echo '<div class="shortinfo">';
										echo '<div class="name" data-idcomment="'.(int)$answer['id'].'">';
											echo '<a href="'.url('comment-author:'.$identity).'" title="'.($viewing_in_english == false ? 'Se alla kommentarer från' : 'See all comments from').' '.trim($answer['visitor_name']).'">';
												echo trim($answer['visitor_name']);
											echo '</a>';

											echo '<span class="rss">';
												echo link_(svgicon('linkicon-rss'), url('rss/comments/author:'.str_replace(' ', '-', strtolower(trim($answer['visitor_name'])))));
											echo '</span>';
										echo '</div>';
									echo '</div>';

									if($url_a != null) {
										echo '<div class="url">';
											echo link_($url_a['host'] . (isset($url_a['path']) ? '<span class="path">'.$url_a['path'].'</span>' : ''), $answer['visitor_url']);
										echo '</div>';
									}
								echo '</div>';

								echo '<div>';
									echo '<div class="published small-text">';
										echo date_($answer['timestamp_published'], 'full');
									echo '</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';


						echo '<div class="comment">';
							echo '<div class="answering-to small-text">';
								echo svgicon('answering-to');
								echo ($viewing_in_english == false ? 'Svar till' : 'An answer to').' <a href="javascript:void(0)" class="link-id" data-idcomment="'.($answer['id_answer'] == null ? (int)$answer['id_comment'] : (int)$answer['id_answer']).'">'.$answersto['visitor_name'].'</a>:';
							echo '</div>';

							echo preg_replace('/(?<!\S)#([0-9a-zA-Z]+)/', '<a href="javascript:void(0)" class="link-id" data-idcomment="$1">#$1</a>', $Parsedown->text(emoji_convert($answer['visitor_comment'])));
						echo '</div>';


						echo '<div class="links small-text">';
							echo '<a href="javascript:void(0)" class="answer" data-idcomment="'.(int)$comment['id'].'" data-idanswer="'.(int)$answer['id'].'">';
								echo ($viewing_in_english == false ? 'Svara' : 'Answer');
							echo '</a>';

							echo '<div class="answer-inactive" data-idcomment="'.(int)$answer['id'].'">';
								echo ($viewing_in_english == false ? 'Svara' : 'Answer');
							echo '</div>';


							echo '<a href="javascript:void(0)" class="manage" data-idcomment="'.(int)$answer['id'].'">';
								echo ($viewing_in_english == false ? 'Hantera' : 'Manage');
							echo '</a>';

							echo '<div class="loading" data-idcomment="'.(int)$answer['id'].'">';
								echo ($viewing_in_english == false ? 'Hantera (vänta)' : 'Manage (please wait)');
							echo '</div>';

							echo '<a href="javascript:void(0)" class="managing" data-idcomment="'.(int)$answer['id'].'">';
								echo ($viewing_in_english == false ? 'Hanteras...' : 'Managing...');
							echo '</a>';
						echo '</div>';

					echo '</div>';
				}
			}
		}
	}

?>
