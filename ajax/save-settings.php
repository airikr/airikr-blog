<?php

	require_once '../site-settings.php';

	$field_name = strip_tags(htmlspecialchars($_POST['field-name']));
	$field_email = strip_tags(htmlspecialchars($_POST['field-email']));
	$field_url = strip_tags(htmlspecialchars($_POST['field-url']));

	$field_email = filter_var($field_email, FILTER_SANITIZE_EMAIL);
	$field_url = filter_var($field_url, FILTER_SANITIZE_URL);



	sql("UPDATE users
		 SET info_name = :_name,
			 info_email = :_email,
			 info_url = :_url

		 WHERE id = :_iduser
		", Array(
			'_iduser' => (int)$user['id'],
			'_name' => trim($field_name),
			'_email' => trim($field_email),
			'_url' => trim($field_url)
		));

?>
