<?php

	require_once '../site-settings.php';

	$file_id = strip_tags(htmlspecialchars($_GET['idf']));
	$field_name = strip_tags(htmlspecialchars($_POST['field-name']));
	$field_alt = strip_tags(htmlspecialchars($_POST['field-alt']));

	$file = sql("SELECT *
				 FROM files
				 WHERE id = :_idfile
				", Array(
					'_idfile' => $file_id
				), 'fetch');

	$filename_md5 = MD5($field_name.'.'.$file['file_extension']);

	$dir_videos = '../videos/';
	$dir_uploaded_small = '../images/uploaded/small/';
	$dir_uploaded_large = '../images/uploaded/large/';


	if($file['file_extension'] == 'mp4') {
		$destination = $dir_videos . $filename_md5.'.mp4';

	} else {
		$destination = $dir_uploaded_small . $filename_md5.'.'.$file['file_extension'];
		$destination_large = $dir_uploaded_large . $filename_md5.'.'.$file['file_extension'];
	}



	if($file['file_name_md5'] != $filename_md5 AND file_exists($destination)) {
		echo 'error-file-exists';

	} else {
		rename($dir_uploaded_small . $file['file_name_md5'].'.'.$file['file_extension'], $destination);
		rename($dir_uploaded_large . $file['file_name_md5'].'.'.$file['file_extension'], $destination_large);

		sql("UPDATE files
			 SET file_name = :_filename,
				 file_name_md5 = :_filename_md5,
				 field_alt = :_alt

			 WHERE id = :_idfile
			", Array(
				'_idfile' => (int)$file_id,
				'_filename' => $field_name,
				'_filename_md5' => $filename_md5,
				'_alt' => $field_alt
			));
	}

?>
