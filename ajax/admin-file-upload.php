<?php

	require_once '../site-settings.php';

	$file_temp = $_FILES['file']['tmp_name'];
	$file_size = $_FILES['file']['size'];
	$file_error = $_FILES['file']['error'];
	$file_info = pathinfo($_FILES['file']['name']);
	$file_dimensions_large = 2000;
	$file_dimensions_small = 800;
	$file_extension = strtolower($file_info['extension']);

	$field_name = strip_tags(htmlspecialchars($_POST['field-name']));
	$field_alt = strip_tags(htmlspecialchars($_POST['field-alt']));
	$check_1 = (isset($_POST['check-1']) ? true : false);

	$filename_md5 = MD5($field_name.'.'.$file_extension);

	$dir_videos = '../videos/';
	$dir_uploaded = '../images/uploaded/';
	$dir_uploaded_small = '../images/uploaded/small/';
	$dir_uploaded_large = '../images/uploaded/large/';


	create_folder(rtrim($dir_videos, '/'));
	create_folder(rtrim($dir_uploaded, '/'));
	create_folder(rtrim($dir_uploaded_small, '/'));
	create_folder(rtrim($dir_uploaded_large, '/'));


	if($file_extension == 'mp4') {
		$destination = $dir_videos . $filename_md5.'.mp4';

	} else {
		list($width, $height) = getimagesize($_FILES['file']['tmp_name']);

		$file_extension = ($file_extension == 'gif' ? 'gif' : 'jpg');
		$destination_small = $dir_uploaded_small . $filename_md5.'.'.$file_extension;
		$destination_large = $dir_uploaded_large . $filename_md5.'.'.$file_extension;
	}


	$file = sql("SELECT file_name_md5, COUNT(file_name_md5) AS do_exists
				 FROM files
				 WHERE file_name_md5 = :_filename_md5
				 GROUP BY id
				", Array(
					'_filename_md5' => $filename_md5
				), 'fetch');



	if($file_error == 1) {
		echo 'error-upload-1';

	} elseif($file_size > $config_max_filesize) {
		echo 'error-upload-2';

	} elseif(file_exists($dir_uploaded_small . $filename_md5.'.'.$file_extension)) {
		echo 'error-file-exists';


	} else {
		if(move_uploaded_file($file_temp, $destination_small)) {
			if($file_extension != 'gif') {
				copy($destination_small, $destination_large);
				manage_image($destination_small, $height, $width, ($check_1 == true ? 2000 : 800));
				manage_image($destination_large, $height, $width, ($check_1 == true ? 2000 : 2000));
			}

			sql("INSERT INTO files(
					 file_name,
					 file_name_md5,
					 file_extension,
					 field_alt,
					 timestamp_uploaded
				 )

				 VALUES(
					 :_filename,
					 :_filename_md5,
					 :_extension,
					 :_alt,
					 :_uploaded
				 )
				", Array(
					'_filename' => $field_name,
					'_filename_md5' => $filename_md5,
					'_extension' => $file_extension,
					'_alt' => $field_alt,
					'_uploaded' => time()
				), 'insert');

			echo $filename_md5.'.'.$file_extension;


		} else {
			echo 'error-filenotuploaded';
		}
	}

?>
