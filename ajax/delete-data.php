<?php

	require_once '../site-settings.php';



	sql("UPDATE comments
		 SET visitor_ipaddress = ''
		 WHERE timestamp_published < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 WEEK));

		 DELETE FROM visitors
	 	 WHERE timestamp_visited < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 MONTH));
		", Array());

?>
