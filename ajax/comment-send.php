<?php

	require_once '../site-settings.php';

	$id_unique = strip_tags(htmlspecialchars($_GET['idu']));

	$hidden_idcomment = strip_tags(htmlspecialchars(trim($_POST['hidden-idcomment'])));
	$hidden_idanswer = strip_tags(htmlspecialchars(trim($_POST['hidden-idanswer'])));

	$field_name = strip_tags(htmlspecialchars(trim($_POST['field-name'])));
	$field_url = strip_tags(htmlspecialchars(trim($_POST['field-url'])));
	$field_url = filter_var($field_url, FILTER_SANITIZE_URL);
	$field_comment = strip_tags(htmlspecialchars(trim($_POST['field-comment'])));
	$field_password = strip_tags(htmlspecialchars(trim($_POST['field-password-readonly'])));

	$identity = (!empty($field_url) ? MD5($field_name . $field_url) : null);

	$get_blacklistedurls = sql("SELECT data_word
								FROM blacklist
							   ", Array());

	$arr_urls = [];
	foreach($get_blacklistedurls AS $url) {
		$arr_urls[] = $url['data_word'];
	}



	if(stripos_array($field_url, $arr_urls) !== false) {
		echo 'error-urlblacklisted';


	} else {
		$post = sql("SELECT id, id_unique
					 FROM posts
					 WHERE id_unique = :_idunique
					", Array(
						'_idunique' => $id_unique
					), 'fetch');

		sql("INSERT INTO comments(
				 id_post,
				 id_comment,
				 id_answer,
				 visitor_name,
				 visitor_url,
				 visitor_comment,
				 visitor_ipaddress,
				 comment_password,
				 comment_identity,
				 timestamp_published
			 )
			 VALUES(
				 :_idpost,
				 :_idcomment,
				 :_idanswer,
				 :_name,
				 :_url,
				 :_comment,
				 :_ipaddress,
				 :_password,
				 :_identity,
				 :_published
			 )
			", Array(
				'_idpost' => (int)$post['id'],
				'_idcomment' => (empty($hidden_idcomment) ? null : $hidden_idcomment),
				'_idanswer' => (empty($hidden_idanswer) ? null : $hidden_idanswer),
				'_name' => $field_name,
				'_url' => (empty($field_url) ? null : $field_url),
				'_comment' => $field_comment,
				'_ipaddress'  => $visitor_ip,
				'_password' => password_hash($field_password, PASSWORD_BCRYPT),
				'_identity' => $identity,
				'_published' => time()
			), 'insert');
	}

?>
