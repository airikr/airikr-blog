<?php

	if(isset($_GET['del'])) {

		require_once 'site-settings.php';

		$idfile = strip_tags(htmlspecialchars($_GET['idf']));
		$file = sql("SELECT *
					 FROM files
					 WHERE id = :_idfile
					", Array(
						'_idfile' => $idfile
					), 'fetch');


		sql("DELETE FROM files
			 WHERE id = :_idfile
			", Array(
				'_idfile' => $file['id']
			));

		unlink('images/uploaded/small/'.$file['file_name_md5'].'.'.$file['file_extension']);
		unlink('images/uploaded/large/'.$file['file_name_md5'].'.'.$file['file_extension']);


		header("Location: ".url('files'));
		exit;



	} else {

		require_once 'site-header.php';



		$editing = false;

		if(isset($_GET['idf'])) {
			$editing = true;
			$file_id = strip_tags(htmlspecialchars($_GET['idf']));

			$file = sql("SELECT *
						 FROM files
						 WHERE id = :_idfile
						", Array(
							'_idfile' => $file_id
						), 'fetch');


		} else {
			$dir = 'images/uploaded/small/';
			$get_files = sql("SELECT *
							  FROM files
							  ORDER BY timestamp_uploaded DESC
							 ", Array());
		}





		echo '<section id="files">';
			echo '<h1>';
				if($editing == false) {
					echo ($viewing_in_english == false ? 'Ladda upp en bild' : 'Upload an image');

				} else {
					echo ($viewing_in_english == false ? 'Ändra en bild' : 'Edit a file');
				}
			echo '</h1>';


			if($editing == false) {
				if($viewing_in_english == false) {
					echo '<ul>';
						echo '<li>Den maximala filstorleken är '.calculate_filesize($config_max_filesize).'.</li>';
						echo '<li>Stödjer filformaten JPG, JPEG, GIF, WEBP, PNG och MP4.</li>';
						echo '<li>Om filtypen är JPG eller JPEG, kommer bilden att optimeras och all metadata raderas. Filtyperna PNG och WEBP kommer att konverteras till JPG.</li>';
					echo '</ul>';

				} else {
					echo '<ul>';
						echo '<li>Maximum file size is '.calculate_filesize($config_max_filesize).'.</li>';
						echo '<li>JPG, JPEG, GIF, WEBP, PNG, and MP4 are supported.</li>';
						echo '<li>If the file type are JPG or JPEG, the image will be optimized and all metadata will be removed. The file types PNG and WEBP will be converted to JPG.</li>';
					echo '</ul>';
				}
			}



			echo '<div id="message"></div>';
			echo '<form action="javascript:void(0)" method="POST" enctype="multipart/form-data" autocomplete="off">';

				if($editing == false) {
					echo '<div class="item choose-file">';
						echo '<div class="field">';
							# echo '<input type="file" name="file" id="file" tabindex="1" accept=".jpg,.jpeg,.gif,.webp,.png" required>';
							echo '<label class="custom-file-upload">';
								echo '<input type="file" name="file" id="file" tabindex="1" accept=".jpg,.jpeg,.gif,.webp,.png,.mp4" required>';

								echo svgicon('file');
								echo '<span id="file-selected">';
									echo ($viewing_in_english == false ? 'Välj fil' : 'Choose a file');
								echo '</span>';
							echo '</label>';
						echo '</div>';
					echo '</div>';


				} else {
					list($width, $height) = getimagesize('images/uploaded/small/'.$file['file_name_md5'].'.'.$file['file_extension']);
					echo '<div class="image" style="background-image: url('.url('images/uploaded/small/'.$file['file_name_md5'].'.'.$file['file_extension']).'); height: '.$height.'px;"></div>';
					echo '<input type="hidden" name="hidden-idfile" value="'.(int)$file['id'].'">';
				}


				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Namn' : 'Name');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-name" placeholder="'.($viewing_in_english == false ? 'Krävs' : 'Required').'"';
						echo ($editing == false ? '' : ' value="'.$file['file_name'].'"');
						echo ' tabindex="2">';

						echo '<div class="extension">';
							echo ($editing == false ? '' : $file['file_extension']);
						echo '</div>';
					echo '</div>';
				echo '</div>';

				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Alternativ text' : 'Alternative text');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-alt" placeholder="'.($viewing_in_english == false ? 'Krävs för blinda personer' : 'Required for visually impaired').'"';
						echo ($editing == false ? '' : ' value="'.$file['field_alt'].'"');
						echo ' maxlength="200" tabindex="3">';
					echo '</div>';
				echo '</div>';



				if($editing == false) {
					echo '<div class="checkboxes">';
						echo '<div class="pretty p-default p-round p-thick">';
							echo '<input type="checkbox" name="check-1" id="check-1">';
							echo '<div class="state p-primary-o">';
								echo '<label for="check-1">';
									echo ($viewing_in_english == false ? 'Förminska inte bilden' : 'Do not shrink the picture');
								echo '</label>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				}



				echo '<div class="button">';
					echo '<input type="submit" name="button-';
					echo ($editing == false ? 'upload' : 'save');
					echo '" value="';
					echo ($editing == false ? ($viewing_in_english == false ? 'Ladda upp' : 'Upload') : ($viewing_in_english == false ? 'Spara' : 'Save'));
					echo '" tabindex="4">';

					if($editing == true) {
						echo '<div class="links">';
							echo '<a href="'.url('files').'">';
								echo ($viewing_in_english == false ? 'Avbryt' : 'Cancel');
							echo '</a>';
						echo '</div>';
					}
				echo '</div>';

			echo '</form>';



			if($editing == false) {
				echo '<div class="list-files">';
					foreach($get_files AS $file) {
						echo '<div>';
							echo '<div class="side-by-side">';
								echo '<div class="delete small-text">';
									echo '<a href="'.url('files/delete:'.$file['id']).'" onClick="return confirm(\'Du är på väg att ta bort filen '.$file['file_name'].'. Du kommer inte ångra dig om du väljer att fortsätta.\')" class="color-red">';
										echo ($viewing_in_english == false ? 'Ta bort' : 'Delete');
									echo '</a>';
								echo '</div>';

								echo '<div class="edit small-text">';
									echo '<a href="'.url('files/edit:'.$file['id']).'">';
										echo ($viewing_in_english == false ? 'Ändra' : 'Edit');
									echo '</a>';
								echo '</div>';

								echo '<div class="copy small-text">';
									echo '<a href="javascript:void(0)" id="copy" data-clipboard-text="!['.$file['field_alt'].']('.url('image:'.$file['file_name_md5'].'.'.$file['file_extension']).')">';
										echo ($viewing_in_english == false ? 'Kopiera' : 'Copy');
									echo '</a>';
								echo '</div>';

								echo '<div class="open small-text">';
									echo link_(($viewing_in_english == false ? 'Öppna' : 'Open'), url('image:'.$file['file_name_md5'].'.'.$file['file_extension'], true));
								echo '</div>';


								echo '<div class="thumbnail">';
									echo '<div style="background-image: url('.url('image:'.$file['file_name_md5'].'.'.$file['file_extension'], true).');"></div>';
								echo '</div>';

								echo '<div class="filename">';
									echo $file['file_name'];
								echo '</div>';
							echo '</div>';

							echo '<div class="filesize">';
								echo calculate_filesize(filesize($dir . $file['file_name_md5'].'.'.$file['file_extension']));
							echo '</div>';
						echo '</div>';
					}
				echo '</div>';
			}
		echo '</section>';







		require_once 'site-footer.php';

	}

?>
