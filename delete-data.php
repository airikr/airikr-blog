<?php

	$viewing_in_english = false;

	require_once 'site-functions.php';
	require_once 'site-config.php';

	try {
		$sql = new PDO('mysql:host='.$config_db_host.';dbname='.$config_db_database, $config_db_username, $config_db_password);
		$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	catch(PDOException $e) {
		echo $e;
		exit;
	}



	sql("UPDATE comments
		 SET visitor_ipaddress = ''
		 WHERE timestamp_published < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 2 WEEK));

		 DELETE FROM visitors
	 	 WHERE timestamp_visited < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 MONTH));
		", Array());

?>
