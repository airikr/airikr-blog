<?php

				echo '</main>';
			echo '</section>';

		echo '</body>';



		echo '<script type="text/javascript" rel="preload" as="script" src="'.(empty($config_cdn) ? url('js/jquery.min.js', true) : 'https://'.$config_cdn.'/jquery.min.js').'"></script>';
		echo '<script async defer type="text/javascript" rel="preload" as="script" src="'.($config_development == true ? url('js/main.js?'.time(), true) : (empty($config_cdn) ? url('js/main.min.js', true) : 'https://'.$config_cdn.'/blog/main.min.js')).'"></script>';
		echo '<script async defer type="text/javascript" rel="preload" as="script" src="'.(empty($config_cdn) ? url('js/validator.min.js', true) : 'https://'.$config_cdn.'/validator.min.js').'"></script>';
		echo '<script async type="text/javascript" rel="preload" as="script" src="'.(empty($config_cdn) ? url('js/clipboard.min.js', true) : 'https://'.$config_cdn.'/clipboard.min.js').'"></script>';
		echo '<script async type="text/javascript" rel="preload" as="script" src="'.(empty($config_cdn) ? url('js/hammer.min.js', true) : 'https://'.$config_cdn.'/hammer.min.js').'"></script>';
		echo '<script async type="text/javascript" rel="preload" as="script" src="'.(empty($config_cdn) ? url('js/jquery-ui.min.js', true) : 'https://'.$config_cdn.'/blog/jquery-ui.min.js').'"></script>';
		# echo '<script async href="https://raw.githubusercontent.com/jhchen/fast-diff/master/diff.js"></script>';
	echo '</html>';

?>
