<?php

	include_once('site-settings.php');
	header('Cache-Control: max-age=604800');

	$value = $_GET['str'];
	$size = min(max(intval(32), 20), 500);

	$icon = new \Jdenticon\Identicon(array(
		'value' => $value,
		'size' => $size,
		'style' => [
			'backgroundColor' => 'transparent',
			'colorLightness' => [.4, .8],
			'grayscaleLightness' => [.3, .9],
			'colorSaturation' => .7,
			'grayscaleSaturation' => .4,
			'padding' => 0
		]
	));

	$icon->displayImage('svg');

?>
