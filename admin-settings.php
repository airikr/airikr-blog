<?php

	require_once 'site-header.php';







	echo '<section id="settings">';
		echo '<h2>'.($viewing_in_english == false ? 'Inställningar' : 'Settings').'</h2>';



		echo '<div id="message"></div>';
		echo '<form action="javascript:void(0)" method="POST" id="comment">';

			echo '<div>';
				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Ditt namn' : 'Your name');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="text" name="field-name" placeholder="'.($viewing_in_english == false ? 'Valfritt' : 'Optional').'" value="'.$user['info_name'].'" tabindex="1">';
					echo '</div>';
				echo '</div>';


				echo '<div class="item">';
					echo '<div class="label">';
						echo ($viewing_in_english == false ? 'Din webbadress' : 'Your URL');
					echo '</div>';

					echo '<div class="field">';
						echo '<input type="url" name="field-url" placeholder="'.($viewing_in_english == false ? 'Valfritt (exempel: https://blog.airikr.me)' : 'Optional (example: https://blog.airikr.me/en)').'" value="'.$user['info_url'].'" tabindex="3">';
					echo '</div>';
				echo '</div>';
			echo '</div>';



			echo '<div class="button">';
				echo '<input type="submit" name="button-save" value="'.($viewing_in_english == false ? 'Spara' : 'Save').'" tabindex="4">';
			echo '</div>';

		echo '</form>';
	echo '</section>';







	require_once 'site-footer.php';

?>
