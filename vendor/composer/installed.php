<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'astrotomic/php-twemoji' => 
    array (
      'pretty_version' => '0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6cedadaf3bfb2ea2133168cb9eed952623e91ec',
    ),
    'bissolli/php-css-js-minifier' => 
    array (
      'pretty_version' => 'v1.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e87834fccfea023b0cb057f517fca3e8dd177c9d',
    ),
    'chrisullyott/php-filesize' => 
    array (
      'pretty_version' => 'v4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef6f7ce0967dc033cdce703761dfa745bcee4046',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'erusev/parsedown-extra' => 
    array (
      'pretty_version' => '0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '91ac3ff98f0cea243bdccc688df43810f044dcef',
    ),
    'hackzilla/password-generator' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53c13507b5b9875a07618f1f2a7db95e5b8c0638',
    ),
    'mukto90/ncrypt' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'c66236716492796fcc7c28e07a5e7752daeb9225',
    ),
    'phpgangsta/googleauthenticator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '505c2af8337b559b33557f37cda38e5f843f3768',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.5.0',
      'version' => '6.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5b5c43e50b7fba655f793ad27303cd74c57363c',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'tecnickcom/tc-lib-barcode' => 
    array (
      'pretty_version' => '1.17.6',
      'version' => '1.17.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8a7183e2be6379e522dc825a3b7a3af5394a9e7',
    ),
    'tecnickcom/tc-lib-color' => 
    array (
      'pretty_version' => '1.14.6',
      'version' => '1.14.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '83cdb57fd900901c6aa2af8cfd67202518fb69b2',
    ),
    'yetanotherape/diff-match-patch' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e93beb95bea67d2a9f2a662434ca7fbf90b0b172',
    ),
  ),
);
