<?php

	require_once 'site-header.php';



	$useragent = (!isset($_GET['use']) ? null : strip_tags(htmlspecialchars($_GET['use'])));
	$check_ip = sql("SELECT COUNT(data_ipaddress)
					 FROM visitors
					 WHERE data_ipaddress = :_ipaddress
					", Array(
						'_ipaddress' => $visitor_ip
					), 'count');

	if($check_ip != 0) {
		if($useragent == null) {
			$get_data = sql("SELECT *
							 FROM visitors
							 WHERE data_ipaddress = :_ipaddress
							 ORDER BY timestamp_visited DESC
							", Array(
								'_ipaddress' => $visitor_ip
							));

			$get_useragents = sql("SELECT data_useragent
								   FROM visitors
								   WHERE data_ipaddress = :_ipaddress
								   GROUP BY data_useragent
								   ORDER BY data_useragent ASC
								  ", Array(
									  '_ipaddress' => $visitor_ip
								  ));


		} else {
			$get_data = sql("SELECT *
							 FROM visitors
							 WHERE data_ipaddress = :_ipaddress
							 AND data_useragent = :_useragent
							 ORDER BY timestamp_visited DESC
							", Array(
								'_ipaddress' => $visitor_ip,
								'_useragent' => $useragent
							));
		}
	}







	echo '<section id="your-data">';
		echo '<h1>'.($viewing_in_english == false ? 'Din data' : 'Your data').'</h1>';


		echo '<div class="message"'.($check_ip == 0 ? ' style="display: block;"' : '').'>';
			echo ($viewing_in_english == false ? 'Inget har loggats om ditt besök' : 'Nothing has been logged about your visit');
		echo '</div>';



		echo '<div class="content"'.($check_ip == 0 ? '' : ' style="display: block;"').'>';
			echo '<div class="delete-all-data">';
				echo '<a href="javascript:void(0)" id="delete">';
					echo ($viewing_in_english == false ? 'Ta bort all data' : 'Delete all your data');
				echo '</a>';
			echo '</div>';

			echo '<div class="ip-address">';
				echo getip();
			echo '</div>';

			echo '<div class="useragent">';
				echo '<ul>';
					foreach($get_useragents AS $ua) {
						echo '<li>'.$ua['data_useragent'].'</li>';
					}
				echo '</ul>';
			echo '</div>';



			foreach($get_data AS $data) {
				echo '<div class="item">';
					echo '<div class="page">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Sida' : 'Page').':';
						echo '</div>';

						echo '<div class="value">';
							echo page_humanreadable($data['data_page'], ($data['data_page_get'] == null ? '' : $data['data_page_get']));
						echo '</div>';
					echo '</div>';

					echo '<div class="visited">';
						echo '<div class="label">';
							echo ($viewing_in_english == false ? 'Besöktes' : 'Visited').':';
						echo '</div>';

						echo '<div class="value">';
							echo date('Y-m-d, H:i:s', $data['timestamp_visited']);
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}
		echo '</div>';
	echo '</section>';







	require_once 'site-footer.php';

?>
