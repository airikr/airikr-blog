<?php

	session_start();

	require_once 'vendor/autoload.php';
	require_once 'site-functions.php';
	require_once 'parsedown-imgfigure.php';

	$Parsedown = new Parsedown();
	$Parsedown = new ParsedownExtra();
	$Parsedown = new FigureExtParsedown();

	use Aheenam\EstimatedReadingTime\EstimatedReadingTime;
	use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
	use Jaybizzle\CrawlerDetect\CrawlerDetect;
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	$generate_id = new ComputerPasswordGenerator();
	$generate_id->setUppercase()->setLowercase()->setNumbers()->setSymbols(false)->setLength(8);
	$id = $generate_id->generatePasswords(1);

	$generate_passwd = new ComputerPasswordGenerator();
	$generate_passwd->setUppercase()->setLowercase()->setNumbers()->setSymbols(false)->setLength(20);
	$password = $generate_passwd->generatePasswords(1);



	/* send_email(
		'hi@airikr.me',
		'Återställning av lösenord för kommentar',
		'<h4>Återställning av lösenord för kommentar</h4><p>Någon (förmodligen du) har begärt att återställa lösenordet för en av dina kommentarer på '.$config_title.'. Om du inte har begärt detta, ignorera då detta e-postmeddelande.</p><p>asd</p>',
		'no-html'
	); */



	$protocol = (stripos($_SERVER['SERVER_PROTOCOL'], 'http') !== false ? 'https' : 'http');
	$host = $_SERVER['HTTP_HOST'];
	$dir = basename(__DIR__);
	$is_locally = ((strpos($host, '192.168') !== false OR $host == 'localhost') ? true : false);
	$session = (isset($_SESSION['loggedin']) ? true : false);
	$session_id = ($session == false ? null : $_SESSION['loggedin']);
	$session_config = (isset($_SESSION['config']) ? true : false);
	$viewing_in_english = (isset($_GET['lau']) ? true : false);

	$filename_path = explode('/', $_SERVER['PHP_SELF']);
	$filename = $filename_path[count($filename_path) - 1];
	$filename_query = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : null;
	$filename_get = !empty($filename_query) ? $filename_query : '-';

	require_once 'site-config.php';

	$og_title = $config_title;
	$og_url = $protocol.'://'.($config_subdomain == null ? '' : $config_subdomain.'.') . $config_domain . ($viewing_in_english == false ? '' : '/en');
	$og_url_noprotocol = ($config_subdomain == null ? '' : $config_subdomain.'.') . $config_domain;
	$og_image = $config_image;
	$og_image_alternative = $config_image_alternative;
	$og_description = $config_description;

	$blogowner = link_(($viewing_in_english == false ? 'bloggägaren' : 'the blog owner'), $protocol.'://'.$config_domain . ($viewing_in_english == false ? null : '/en'));
	$blogowner_capital = link_(($viewing_in_english == false ? 'Bloggägaren' : 'The blog owner'), $protocol.'://'.$config_domain . ($viewing_in_english == false ? null : '/en'));
	$blogowner_contact = link_(($viewing_in_english == false ? 'kontakta bloggägaren' : 'contact the blog owner'), $protocol.'://'.$config_domain . ($viewing_in_english == false ? null : '/en'));



	if(strpos($filename, 'admin-post') === false AND
	   strpos($filename, 'file-') === false AND
	   strpos($filename, 'admin-login') === false AND
	   strpos($filename, 'manage') === false AND
	   strpos($filename, 'settings') === false AND
	   strpos($filename, 'list') === false AND
	   strpos($filename, 'json') === false) {

		create_folder('covers');
		create_folder('images');

	}



	try {
		$sql = new PDO('mysql:host='.$config_db_host.';dbname='.$config_db_database, $config_db_username, $config_db_password);
		$sql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	catch(PDOException $e) {
		echo $e;
		exit;
	}



	if($session == true AND $filename != 'admin-login.php') {
		$user_exists = sql("SELECT COUNT(id)
							FROM users
							WHERE id = :_iduser
						   ", Array(
							   '_iduser' => (int)$session_id
						   ), 'count');


		if($user_exists != 0) {
			$user = sql("SELECT *
						 FROM users
						 WHERE id = :_iduser
						", Array(
							'_iduser' => (int)$session_id
						), 'fetch');

			sql("UPDATE users
				 SET timestamp_lastactive = :_lastactive
				", Array(
					'_lastactive' => time()
				));


			if($user['password'] != null AND $filename != 'admin-setup-totp.php' AND $filename != 'settings-setup-totp.php') {
				header("Location: ".url('settings:setup-totp'));
				exit;
			}


		} else {
			if($filename != 'admin-logout.php') {
				header("Location: ".url('logout'));
				exit;
			}
		}
	}



	if($filename == 'admin-write.php' AND $session == false OR
	   $filename == 'admin-publish.php' AND $session == false OR
	   $filename == 'admin-files.php' AND $session == false OR
	   $filename_get == 'pag=saved' AND $session == false OR
	   $filename == 'admin-login.php' AND $session == true OR
	   $filename == 'admin-setup-totp.php' AND $session == true AND $user['password'] == null) {
		header("Location: ".url('login'));
		exit;
	}



	$ncrypt = new mukto90\Ncrypt;
	$ncrypt->set_secret_key($config_encryption_key);
	$ncrypt->set_secret_iv($config_encryption_iv);
	$ncrypt->set_cipher('AES-256-CBC');

	$visitor_ip = $ncrypt->encrypt(getip());



	/* if(strpos($filename, 'admin-post') === false AND
	   strpos($filename, 'file-') === false AND
	   strpos($filename, 'admin-login') === false AND
	   strpos($filename, 'manage') === false AND
	   strpos($filename, 'settings') === false AND
	   strpos($filename, 'avatar') === false AND
	   strpos($filename, 'list') === false) {

		require_once 'ajax/log-visit.php';

	} */



	if($session == false) {
		$check_analytics = sql("SELECT COUNT(data_ipaddress)
								FROM visitors
								WHERE data_ipaddress = :_ipaddress
							   ", Array(
								   '_ipaddress' => $visitor_ip
							   ), 'count');

		$not_logged = false;
		if($check_analytics == 0) {
			$not_logged = true;

		} else {
			sql("INSERT INTO visitors(
					 data_ipaddress,
					 data_useragent,
					 data_page,
					 data_page_get,
					 timestamp_visited
				 )

				 VALUES(
					 :_ipaddress,
					 :_useragent,
					 :_page,
					 :_page_get,
					 :_visited
				 )
				", Array(
					'_ipaddress' => $visitor_ip,
					'_useragent' => $_SERVER['HTTP_USER_AGENT'],
					'_page' => $filename,
					'_page_get' => ($filename_get == '-' ? null : $filename_get),
					'_visited' => time()
				), 'insert');
		}
	}

?>
